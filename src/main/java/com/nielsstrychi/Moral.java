package com.nielsstrychi;

/**
 * Visualise the entity his moral.
 *
 * @author Nielsstrychi
 */
public enum Moral {
    _100_85("▉"),
    _84_70("▇"),
    _69_55("▆"),
    _54_40("▅"),
    _39_25("▃"),
    _24_10("▂"),
    _9_0("▁");

    private String graph;

    Moral(String graph) {
        this.graph = graph;
    }

    public String getGraph() {
        return graph;
    }
}
