package com.nielsstrychi;

import java.util.Random;

public enum Tiers {
    I("Augusta","Germanica","Adiutrix","Adiutrix Pia Fidelis","Italica","Macriana Liberatrix","Minervia",
            "Minervia Pia Fidelis Domitiana","Flavia Minervia Domitiana", "Parthica"),
    II("Augusta","Adiutrix","Sabina","Italica","Italica Pia Fidelis","Parthica","Traiana Fortis"),
    III("Gallica","Augusta","Cyrenaica","Italica","Parthica"),
    IV("Macedonica","Flavia Felix","Scythica","Parthica"),
    V("Alaudae","Macedonica","Macedonica Pia Fidelis","Vernacula"),
    VI("Ferrata","Victrix","Hispaniensis","Ferrata Fidelis Constans"),
    VII("Paterna","Claudia Pia Fidelis","Claudia","Gemina","Galbiana"),
    VIII("Augusta","Gallica","Gallica Mutinensis"),
    IX("Hispana");

    private String[] names;

    Tiers(String... varargs) {
        this.names = varargs;
    }

    public String[] getAllNames() {
        return this.names;
    }

    public String getRandomName() {
        return this.names[new Random().nextInt(this.names.length)];
    }
}
