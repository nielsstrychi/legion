package com.nielsstrychi.Listeners;

import com.nielsstrychi.Entities.Soldiers.EntityType;
import com.nielsstrychi.Entities.Soldiers.SoldierSkeletonWither;
import com.nielsstrychi.Handlers.DataBatchHandler;
import com.nielsstrychi.LegionPlugin;
import com.nielsstrychi.Providers.EntityProvider;
import net.minecraft.server.v1_13_R2.Items;
import org.bukkit.Chunk;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.craftbukkit.v1_13_R2.inventory.CraftItemStack;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockCanBuildEvent;
import org.bukkit.event.block.BlockPhysicsEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.world.ChunkLoadEvent;
import org.bukkit.event.world.ChunkUnloadEvent;
import org.bukkit.event.world.WorldSaveEvent;
import org.bukkit.inventory.ItemStack;

import java.util.HashSet;
import java.util.Set;

/**
 * Listens to al block related events.
 * And propagate to handlers or performs logic to handle the event.
 *
 * @author Nielsstrychi
 */
public class BlockListener implements Listener {

    /**
     * Retrieves a block place event and handles the logic of it.
     * @param event hold the Player who placed the block and the placed Block and more.
     */
    @EventHandler
    public void onBlockPlace(final BlockPlaceEvent event) {
        this.createWitherSkeletonSoldier(event);
    }

    // creates a wither skeleton TODO may be split up in the further.
    private void createWitherSkeletonSoldier(final BlockPlaceEvent event) {
        Block block = event.getBlock();
        if ((block.getType() == Material.CARVED_PUMPKIN)) {
            Block under1 = block.getRelative(BlockFace.DOWN);
            Block under2 = under1.getRelative(BlockFace.DOWN);
            if (under1.getType() == Material.COAL_BLOCK && under2.getType() == Material.COAL_BLOCK) {
                block.setType(Material.AIR);
                under1.setType(Material.AIR);
                under2.setType(Material.AIR);
                EntityProvider.provide().spawnNewSoldier(
                        EntityType.WITHER_SKELETON,
                        new Location(block.getWorld(),
                                block.getLocation().getX()+0.5,
                                under2.getLocation().getY(),
                                block.getLocation().getZ()+0.5),
                        event.getPlayer());
            }
        }
    }
}
