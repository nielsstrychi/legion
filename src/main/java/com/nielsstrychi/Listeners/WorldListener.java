package com.nielsstrychi.Listeners;

import com.nielsstrychi.Handlers.DataBatchHandler;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.world.WorldSaveEvent;

/**
 * Handles logic when some world related actions are triggered.
 *
 * @author Nielsstrychi
 */
public class WorldListener implements Listener {

    /**
     * Handlers on world save and wil execute all remaining SQL batches to the database.
     * The world state and persistence state will then be Synchronised.
     * @param event that hold all the worlds that wil be saved.
     */
    @EventHandler
    public void onWorldSave(final WorldSaveEvent event) {
        if (event.getWorld().getName().equals("world")) { //Only trigger once for the main world (Hardcoded)
            DataBatchHandler.handle().executeBatches();
        }
    }
}
