package com.nielsstrychi.Listeners;

import com.nielsstrychi.LegionPlugin;
import com.nielsstrychi.Providers.EntityProvider;
import org.bukkit.Chunk;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.world.ChunkLoadEvent;
import org.bukkit.event.world.ChunkUnloadEvent;

import java.util.HashSet;
import java.util.Set;

/**
 * Listens to all chunk related events and will handle with there proper actions on certain events.
 *
 * @author Nielsstrychi
 */
public class ChunkListener implements Listener {

    /**
     * Will cache a list of chunks that already are loaded and entities are loaded on.
     */
    private final Set<Chunk> LOADED_CHUCKS = new HashSet<>();

    /**
     * Will load entities on chunk loads from the database so they are casted to Soldier or Companion objects.
     * @param event hold the loaded chunks.
     */
    @EventHandler
    public void onChunkLoad(final ChunkLoadEvent event) {
        if (LegionPlugin.getInstance().isEnabled()) {
            if ((event.getChunk() != null) && !event.isNewChunk() && !LOADED_CHUCKS.contains(event.getChunk())) {
                EntityProvider.provide().loadEntities(event.getChunk());
            }
            LOADED_CHUCKS.add(event.getChunk());
        }
    }

    /**
     * When a chunk is unloaded this chunk wil be removed from the cache so next time the chuck wil be reloaded.
     * The entities wil be casted back to the persistence Solder/Companion state.
     * @param event that holds the unloaded chunk.
     */
    @EventHandler
    public void onChunkUnload(final ChunkUnloadEvent event) {
        if (LegionPlugin.getInstance().isEnabled()) {
            LOADED_CHUCKS.remove(event.getChunk());
        }
    }
}
