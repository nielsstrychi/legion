package com.nielsstrychi.Listeners;

import com.nielsstrychi.Entities.Holograms.Hologram;
import com.nielsstrychi.Entities.Soldiers.Soldier;
import com.nielsstrychi.Handlers.TargetHandler;
import org.bukkit.craftbukkit.v1_13_R2.entity.CraftEntity;
import org.bukkit.craftbukkit.v1_13_R2.entity.CraftLivingEntity;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityTargetLivingEntityEvent;

/**
 * Listens to events that are targeted to Soldiers.
 * We propagate the event to there respectably handlers to handle the extra logic.
 *
 * @author Nielsstrychi
 */
public class SoldierListener implements Listener {

    /**
     * Listens when a Soldier targets an LivingEntity.
     * @param event that hold the attacker Soldier, the target LivingEntity and the reason for attacking.
     */
    public void onTarget(EntityTargetLivingEntityEvent event) {
        if (((CraftLivingEntity) event.getEntity()).getHandle() instanceof Soldier) {
            TargetHandler.handle().start((Soldier) ((CraftLivingEntity) event.getEntity()).getHandle(), event.getTarget());
        }
    }

    @EventHandler //TODO holgram listernet
    public void onDamage(EntityDamageEvent event) {
        if (((CraftEntity) event.getEntity()).getHandle() instanceof Hologram) {
            event.setCancelled(true);
        }
    }
}
