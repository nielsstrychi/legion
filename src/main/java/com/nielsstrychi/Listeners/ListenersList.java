package com.nielsstrychi.Listeners;

import com.nielsstrychi.LegionPlugin;
import org.bukkit.event.Listener;
import java.util.stream.Stream;

/**
 * List of Listeners so that they can be registered dynamically and easy.
 * To add a listener just add a enum value with there class implementing the listener interface.
 *
 * @author Nielsstrychi
 */
public enum ListenersList {
    /**
     * Listeners to register dynamically.
     */
    BLOCK(new BlockListener()),
    WORLD(new WorldListener()),
    CHUNK(new ChunkListener()),
    PLAYER(new PlayerListener()),
    SOLDIER(new SoldierListener()),
    DIVISION(new DivisionListener());

    /**
     * Listener class that implements the Listener interface.
     */
    private Listener listener;

    /**
     * Constructor to create a registry object.
     * @param listener that will be set.
     */
    private ListenersList(Listener listener) {
        this.listener = listener;
    }

    /**
     * Getter to get the listener object.
     * @return current listener object.
     */
    public Listener getListener() {
        return listener;
    }

    /**
     * Stream to register all listernet values present in the list.
     * This method is called only once on the init method when the server is started.
     * @param plugin the LegionPlugin we retrieve the Server and PluginManager from and the listener wil be bind to.
     */
    public static void register(LegionPlugin plugin) {
        Stream.of(values())
                .map(ListenersList::getListener)
                .forEach(listener -> plugin
                        .getServer()
                        .getPluginManager()
                        .registerEvents(listener, plugin));
    }
}
