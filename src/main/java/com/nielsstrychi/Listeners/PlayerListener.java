package com.nielsstrychi.Listeners;

import com.nielsstrychi.Handlers.ClickHandler;
import com.nielsstrychi.Handlers.SelectionHandler;
import com.nielsstrychi.Providers.ContextProvider;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.EquipmentSlot;


/**
 * Listens to incoming Player events and wil handle logic for each correct situation.
 */
public class PlayerListener implements Listener {
    private final SelectionHandler selectionHandler = ContextProvider.SELECTION_HANDLER;

    /**
     * Handles a block click event this wil perform a walk to action on his selection if one is present.
     * @param event that hold the interaction with the clicked block.
     */
    @EventHandler
    public void onPlayerClickBlock(PlayerInteractEvent event) {
        if (event.getAction() == Action.RIGHT_CLICK_BLOCK) {
            if (!isSneaking(event)) {
                if (selectionHandler.getPlayerSelector().containsKey(event.getPlayer().getPlayerListName()) && selectionHandler.getPlayerSelector().get(event.getPlayer().getPlayerListName()).getSoldier() != null) {
                    selectionHandler.getPlayerSelector().get(event.getPlayer().getPlayerListName()).moveSoldier(event.getClickedBlock().getLocation());
                }
            }
        }
    }


    /**
     * A direct close interaction on a entity.
     * Will be handled further by the selectionHandler through the selection flow process.
     * @param event holds the entity where the player has clicked on directly.
     */
    @EventHandler
    public void onPlayerRightClickEntity(PlayerInteractEntityEvent event) {
        if (singleClick(event)) {
            selectionHandler.handleSelectionFlow(event);
        }
    }

    /**
     * Handles a far away click first to see if they have selected something.
     * This can be an entity to select, attack, or move to a far location.
     * @param event that hold the direction the player clicked in.
     */
    @EventHandler
    public void onPlayerInteract(PlayerInteractEvent event) {
        if (singleClick(event)) {
            ClickHandler.handleRightFarClick(event); // Pass event to click controller
        }
    }

    /**
     * Private helper method to check if the player is sneaking.
     * @param event that holds the player that may be sneaking.
     * @return true if the player is sneaking else false.
     */
    private boolean isSneaking(PlayerInteractEvent event) {
        return event.getPlayer().isSneaking();
    }

    /**
     * On every click event will be called twice.
     * This for each hand the player has.
     * Here check if the click only with the main hand so only the event logic is trigger once for the mainhand click.
     * @param event the player click event that holds all data.
     * @return true if they click only with the mainhand else false.
     */
    private boolean singleClick(PlayerInteractEntityEvent event) {
        return EquipmentSlot.HAND.equals(event.getHand());
    }

    /**
     * On every click event will be called twice.
     * This for each hand the player has.
     * Here check if the click only with the main hand so only the event logic is trigger once for the mainhand click.
     * @param event the player click event that holds all data.
     * @return true if they click only with the mainhand else false.
     */
    private boolean singleClick(PlayerInteractEvent event) {
        return EquipmentSlot.HAND.equals(event.getHand());
    }

}
