package com.nielsstrychi.Exceptions;

public class EntitySpawnException extends RuntimeException {

    public EntitySpawnException() {
        super();
    }

    public EntitySpawnException(String message) {
        super(message);
    }

    public EntitySpawnException(String message, Throwable cause) {
        super(message, cause);
    }

    public EntitySpawnException(Throwable cause) {
        super(cause);
    }

    protected EntitySpawnException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
