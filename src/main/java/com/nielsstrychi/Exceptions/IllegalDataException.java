package com.nielsstrychi.Exceptions;

public class IllegalDataException extends IllegalArgumentException {
    public IllegalDataException(String s) {
        super(s);
    }
}
