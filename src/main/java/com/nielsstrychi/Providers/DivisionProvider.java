package com.nielsstrychi.Providers;

import com.nielsstrychi.Data.Division;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Stream;

public class DivisionProvider {
    private static DivisionProvider INSTANCE;

    Map<UUID, List<Division>> divisions = new HashMap<>();

    private DivisionProvider() { }

    public static DivisionProvider provide() {
        return INSTANCE == null ? INSTANCE = new DivisionProvider() : INSTANCE;
    }

    public void registerDivisions() {
        Stream.of(Bukkit.getOfflinePlayers()).map(OfflinePlayer::getUniqueId)
                .forEach(uuid -> {
                    divisions.put(uuid, ContextProvider.DIVISION_REPOSITORY.findAllPlayerDivisions(uuid));
                });
        divisions.values().stream().forEach(l -> {l.forEach(p -> p.getDivisionList().forEach(m -> m.getAsBukkitEntity().getLocation().getX()));});
    } //TODO fetch differnly without provider

    public List<Division> getPlayerDivisions(UUID uuid) {
        return divisions.get(uuid);
    }

    public List<Division> getPlayerDivisions(Player player) {
        return divisions.get(player.getUniqueId());
    }

}
