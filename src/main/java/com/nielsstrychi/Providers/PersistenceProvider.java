package com.nielsstrychi.Providers;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 * PersistenceProvider provides the usage of the EntityManager in simplistic way. It opens and closes the transaction.
 * Allows try-with-resources to commit and close the database transactions with the implementation of AutoCloseable
 *
 * @author Niels Strychi
 */
public class PersistenceProvider implements AutoCloseable{

    private final EntityManagerFactory factory;
    private EntityManager manager;
    private static PersistenceProvider INSTANCE;

    /**
     * Singleton constructor.
     * Reassigns the ContextClassLoader so JPA wil look in the right path after persistence.xml file.
     */
    private PersistenceProvider() {
        Thread.currentThread().setContextClassLoader(getClass().getClassLoader());
        this.factory = Persistence.createEntityManagerFactory("persistence-unit");
        INSTANCE = this;
    }

    /**
     * Initialize the singleton, so the PersistenceUnit constructor is called in the provide() method,
     * and the PersistenceUnit singleton can start a database connection before its needed.
     */
    @Deprecated
    public static void initialize() {
        provide();
    }

    /**
     * getInstance method, use in try-with-resources PersistenceProvider implements AutoCloseable.
     * @return PersistenceUnit singleton
     */
    public static PersistenceProvider provide() {
        return INSTANCE == null ? INSTANCE = new PersistenceProvider() : INSTANCE;
    }

    /**
     * Creates EntityManager and starts a Transaction.
     * @return EntityManager to perform future operations on.
     */
    public EntityManager beginSession() {
        this.manager = this.factory.createEntityManager();
        this.manager.getTransaction().begin();
        return this.manager;
    }

    /**
     * Commits transaction and closes EntityManager.
     * AutoCloseable in try-with-resources.
     * @throws Exception's that may occur during closing of the EntityManager.
     */
    @Override
    public void close() throws Exception {
        if (manager==null) return;
        if (manager.getTransaction().isActive())
        this.manager.getTransaction().commit();
        this.manager.close();
    }

    /**
     * Closes EntityManagerFactory used to be closed on the en of the Application
     */
    public void onSeverShutdown() {
        try {
            this.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        factory.close();
    }
}
