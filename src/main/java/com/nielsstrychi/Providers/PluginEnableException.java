package com.nielsstrychi.Providers;

public class PluginEnableException extends RuntimeException {
    public PluginEnableException() {
        super();
    }

    public PluginEnableException(String message) {
        super(message);
    }

    public PluginEnableException(String message, Throwable cause) {
        super(message, cause);
    }

    public PluginEnableException(Throwable cause) {
        super(cause);
    }

    protected PluginEnableException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
