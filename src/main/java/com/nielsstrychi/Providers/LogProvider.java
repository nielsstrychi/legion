package com.nielsstrychi.Providers;

public class LogProvider {

    private static LogProvider INSTANCE;

    private LogProvider() { }

    public static LogProvider provide() {
        return INSTANCE == null ? INSTANCE = new LogProvider() : INSTANCE;
    }
}
