package com.nielsstrychi.Providers;

import com.nielsstrychi.Selectors.Selector;

import java.util.HashMap;
import java.util.Map;

public class SelectionProvider {
    private static SelectionProvider INSTANCE;
    private Map<String, Selector> playerSelector = new HashMap<>();

    private SelectionProvider() { }

    public static SelectionProvider provide() {
        return INSTANCE == null ? INSTANCE = new SelectionProvider() : INSTANCE;
    }

    public Map<String, Selector> getPlayerSelector() {
        return playerSelector;
    }
}
