package com.nielsstrychi.Providers;

import com.nielsstrychi.LegionPlugin;
import com.nielsstrychi.Packets.GlowColor;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;

import java.util.Objects;
import java.util.stream.Stream;

public class TeamsProvider {
    public static Scoreboard scoreboard;

    private static TeamsProvider INSTANCE;

    private TeamsProvider() { }

    public static TeamsProvider provide() {
        return INSTANCE == null ? INSTANCE = new TeamsProvider() : INSTANCE;
    }

    public void register() throws PluginEnableException {
        try {
        Stream.of(GlowColor.values()).forEach(team -> LegionPlugin.getInstance()
                .getServer()
                .getScoreboardManager()
                // We use the main scoreboard and register teams
                .getMainScoreboard()
                .registerNewTeam(team.name())
                // We set the colors for the teams
                .setColor(team.getColor()));
        } catch (IllegalArgumentException e) {
            this.unregister();
            String msg = "\u001B[33m\n\nYou should not remove the 'legion-plugin-server-state' file.\n" +
                    "This file signs the LegionPlugin when the server was not closed properly previously.\n" +
                    "The file gets removed dynamically if you close the server with the '/stop' command and all events are successful unregistered.\n" +
                    "It prevents a data leak and events getting registered twice.\n" +
                    "We are now unregistering all events that where still registered from the previous server uptime.\n" +
                    "The LegionPlugin is now disabled and you need to restart the server if you wish to enable this plugin again.\n\n\u001B[0m";
            System.out.println(msg);
            throw new PluginEnableException(msg, e);
        }
    }

    public void unregister() {
        Stream.of(GlowColor.values()).map(team ->
                LegionPlugin.getInstance()
                        .getServer()
                        .getScoreboardManager()
                        .getMainScoreboard()
                        .getTeam(team.name()))
                .filter(Objects::nonNull)
                .forEach(Team::unregister);
    }
}
