package com.nielsstrychi.Providers;

import com.nielsstrychi.Commands.Executors.ExecutorHandler;
import com.nielsstrychi.Handlers.*;
import com.nielsstrychi.LegionPlugin;
import com.nielsstrychi.Listeners.ListenersList;
import com.nielsstrychi.Repositories.AffiliateRepository;
import com.nielsstrychi.Repositories.EntityRepository;
import com.nielsstrychi.Repositories.DivisionRepository;
import com.nielsstrychi.Repositories.PatrolRepository;
import org.bukkit.event.Listener;

import java.io.*;
import java.util.Set;

public class ContextProvider {

    /** Listeners **/
    public static Set<Listener> LISTENERS;

    /** Handlers **/
    public static ClickHandler CLICK_HANDLER;
    public static CommandHandler COMMAND_HANDLER;
    public static SelectionHandler SELECTION_HANDLER = new SelectionHandler();

    /** Repositories **/
    public static final EntityRepository ENTITY_REPOSITORY = new EntityRepository();
    public static final PatrolRepository PATROL_REPOSITORY = new PatrolRepository();
    public static final DivisionRepository DIVISION_REPOSITORY =new DivisionRepository();
    public static final AffiliateRepository AFFILIATE_REPOSITORY = new AffiliateRepository();

    /** Providers **/
    public static LogProvider LOG_PROVIDER;
    public static TeamsProvider TEAM_PROVIDER;
    public static EntityProvider ENTITY_PROVIDER;
    public static PersistenceProvider PERSISTENCE_PROVIDER;
    public static PacketProtocolProvider PACKET_PROTOCOL_PROVIDER;

    private static ContextProvider INSTANCE;

    private static Boolean BAD_SHUTDOWN;

    private ContextProvider() { }

    public static ContextProvider provide() {
        return INSTANCE == null ? INSTANCE = new ContextProvider() : INSTANCE;
    }

    /**
     * Called first when server starts in the onLoad
     */
    public static void start() {
        if (isServerCloseInterrupted()) {
            restoreStart();
        }
        PERSISTENCE_PROVIDER = PersistenceProvider.provide();
        ENTITY_PROVIDER = EntityProvider.provide();
        ENTITY_PROVIDER.register();
    }

    public static void init(LegionPlugin plugin) throws PluginEnableException {
        ListenersList.register(plugin);
        TEAM_PROVIDER = TeamsProvider.provide();
        if (isServerCloseInterrupted()) {
            restoreInit();
        }
        ENTITY_PROVIDER.loadEntities();
        TEAM_PROVIDER.register();
        ExecutorHandler.registerCommands(LegionPlugin.getInstance());
        AffiliateHandler.handle().cache();
    }

    public static void close() {
        ENTITY_PROVIDER.unregister();
        TEAM_PROVIDER.unregister();
        DataBatchHandler.handle().stop();
        PERSISTENCE_PROVIDER.onSeverShutdown();
        serverCloseSuccessFull();
    }

    /**
     * Gets called when server went suddenly down and gets restarted on startup fase.
     */
    private static void restoreStart() {

    }

    /**
     * Gets called when server went suddenly down and gets restarted on initialization fase.
     */
    private static void restoreInit() {

        TEAM_PROVIDER.unregister();
    }

    private static void serverCloseSuccessFull() {
        File file = new File("legion-plugin-server-state");
        if (file.exists()) {
            file.delete();
        }
        BAD_SHUTDOWN = false;
    }

    private static boolean isServerCloseFilePresent() {
        try {
            return !new File("legion-plugin-server-state").createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return true;
    }

    private static boolean isServerCloseInterrupted() {
        return BAD_SHUTDOWN == null
                ? BAD_SHUTDOWN = isServerCloseFilePresent()
                : BAD_SHUTDOWN;
    }
}
