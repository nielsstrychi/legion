package com.nielsstrychi.Providers;

import java.lang.reflect.InvocationTargetException;
import java.util.*;
import java.util.function.Function;

import com.mojang.datafixers.types.Type;
import com.nielsstrychi.Data.SoldierData;
import com.nielsstrychi.Entities.Soldiers.EntityType;
import com.nielsstrychi.Entities.Soldiers.Soldier;
import com.nielsstrychi.Entities.Soldiers.SoldierSkeletonWither;
import com.nielsstrychi.Exceptions.EntitySpawnException;
import com.nielsstrychi.Repositories.Repository;
import com.nielsstrychi.Util.Converter;
import net.minecraft.server.v1_13_R2.*;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_13_R2.CraftWorld;
import org.bukkit.craftbukkit.v1_13_R2.entity.CraftEntity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;

import static com.nielsstrychi.Entities.Soldiers.EntityType.values;

public class EntityProvider {

    private final static Repository repository = ContextProvider.ENTITY_REPOSITORY;

    private static EntityProvider INSTANCE;

    private final Map<UUID, Soldier> soldierWorldMap = new HashMap<>();
    private List<SoldierData> loadedData;

    private EntityProvider() { }

    public static EntityProvider provide() {
        return INSTANCE == null ? INSTANCE = new EntityProvider() : INSTANCE;
    }

    /**
     * Register our entities.
     */
    public void register() {
        for (EntityType entity : values()) {
            registerEntities(entity, SoldierSkeletonWither::new);
        }
    }

    public void unregister() {
        //saveEntities();//TODO leave this uncommeted until programs works without saving onclose also
        unregisterEntities();
    }

    private void saveEntities() {
        List<SoldierData> soldiersToBeSaved = new ArrayList<>();
        for (org.bukkit.World world : Bukkit.getWorlds()) {
            for (org.bukkit.entity.LivingEntity entity : world.getLivingEntities()) {
                if (Converter.convert(entity) instanceof Soldier) {
                    soldiersToBeSaved.add(((Soldier)Converter.convert(entity)).getData());
                }
            }
        }
        ContextProvider.ENTITY_REPOSITORY.update(soldiersToBeSaved); //TODO test full method
    }

    public void loadEntities() {
        loadedData = repository.findAll();
        respawnAsSoldier(getEntitiesWithData());
    }

    public void loadEntities(org.bukkit.Chunk chunk) {
        respawnAsSoldier(getEntitiesWithData(chunk));
    }

    private Map<SoldierData, org.bukkit.entity.LivingEntity> getEntitiesWithData(org.bukkit.Chunk chunk) {
        Map<SoldierData, org.bukkit.entity.LivingEntity> dataEntityMap = new HashMap<>();
        List<SoldierData> dataSet = new ArrayList<>(loadedData);
        for (org.bukkit.entity.Entity entity : chunk.getEntities()) {
            if (entity instanceof LivingEntity) {
                LivingEntity livingEntity = (LivingEntity) entity;
                for (SoldierData data : dataSet) {
                    if (entity.getUniqueId().equals(data.getUniqueId())) {
                        dataEntityMap.put(data, livingEntity);
                        loadedData.remove(data);
                    }
                }
            }
        }
        return dataEntityMap;
    }

    private Map<SoldierData, org.bukkit.entity.LivingEntity> getEntitiesWithData() {
        Map<SoldierData, org.bukkit.entity.LivingEntity> dataEntityMap = new HashMap<>();
        List<SoldierData> dataSet = new ArrayList<>(loadedData);
        for (org.bukkit.World world : Bukkit.getWorlds()) {
            for (org.bukkit.entity.LivingEntity entity : world.getLivingEntities()) {
                for (SoldierData data : dataSet) {
                    if (entity.getUniqueId().equals(data.getUniqueId())) {
                        dataEntityMap.put(data, entity);
                        loadedData.remove(data);
                    }
                }
            }
        }
        return dataEntityMap;
    }


    public SoldierSkeletonWither spawnNewSoldier(final EntityType type, final Location location, Player player) throws EntitySpawnException {
        try {
            World world = ((CraftWorld) location.getWorld()).getHandle();
            SoldierSkeletonWither spawn = (SoldierSkeletonWither) type.getCustomClass()
                    .getConstructor(World.class, Player.class)
                    .newInstance(world, player);
            Location loc = location;
            spawn.setLocation(loc);
            soldierWorldMap.put(spawn.getUniqueID(), spawn);
            world.addEntity(spawn);
            return spawn;
        } catch (InstantiationException | IllegalAccessException |
                InvocationTargetException | NoSuchMethodException e) {
            throw new EntitySpawnException("Could not spawn entity cause: " + e.toString(), e);
        }
    }

    public void respawnAsSoldier(final SoldierData data, final org.bukkit.entity.LivingEntity entity) throws EntitySpawnException {
        try {
            World world = ((CraftWorld) entity.getWorld()).getHandle();
            SoldierSkeletonWither spawn = (SoldierSkeletonWither) data.getEntityType()
                    .getCustomClass()
                    .getConstructor(SoldierData.class, org.bukkit.entity.LivingEntity.class)
                    .newInstance(data,entity);
//TODO save back data if no exception allow spawn and delete other entity
            world.removeEntity(((CraftEntity) entity).getHandle());
            soldierWorldMap.put(spawn.getUniqueID(), spawn);
            world.addEntity(spawn);//TODO place this piece of code and above line in Soldier have it have responsibility
        } catch (InstantiationException | IllegalAccessException |
                InvocationTargetException | NoSuchMethodException e) {
            e.printStackTrace();
       //     throw new EntityCastException("Could not cast entity cause: " + e.toString(), e);
        }
    }

    private void respawnAsSoldier(Map<SoldierData, org.bukkit.entity.LivingEntity> dataEntityMap) {
        dataEntityMap.forEach(((d,e) -> respawnAsSoldier(d,e)));
    }

    /**
     * Unregister our entities to prevent memory leaks. Call on disable.
     */
    private static void unregisterEntities() {
        for (EntityType entity : values()) {
            MinecraftKey minecraftKey = MinecraftKey.a(entity.getName());

            // Unsure if this works fully, but we should be fine...
            Map<Object, Type<?>> typeMap = (Map<Object, Type<?>>) DataConverterRegistry.a().getSchema(15190).findChoiceType(DataConverterTypes.n).types();
            typeMap.remove(minecraftKey);
            System.out.println("[SMP] Unregistered Entity with name \""+ entity.getName() +"\" and key \""+ minecraftKey.toString() +"\"");
        }
    }

    private static void registerEntities(EntityType type, Function<? super World, ? extends Entity> worldFunction) {
        MinecraftKey minecraftKey = MinecraftKey.a(type.getName());

        Map<Object, Type<?>> typeMap = (Map<Object, Type<?>>) DataConverterRegistry.a().getSchema(15190).findChoiceType(DataConverterTypes.n).types();
        typeMap.put(minecraftKey.toString(), typeMap.get(MinecraftKey.a(type.getName()).toString()));

        EntityTypes.a(type.getName(), EntityTypes.a.a(type.getCustomClass(), worldFunction));

        System.out.println("[SMP] Registered Entity with name \""+ type.getName() +"\" and key \""+ minecraftKey.toString() +"\"");
    }

    public Soldier getSoldierEntity(UUID id) {
        return soldierWorldMap.get(id);
    }
}
