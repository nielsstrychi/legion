package com.nielsstrychi;

/**
 * Combat type of entity this wil be defined on runtime by the soldier self.
 */
public enum Combat {
    MELEE,
    RANGE;
}
