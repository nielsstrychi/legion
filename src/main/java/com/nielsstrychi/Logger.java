package com.nielsstrychi;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.ConsoleCommandSender;

public class Logger {

    private final LegionPlugin plugin;

    public Logger(LegionPlugin instance) {
        plugin = instance;
    }

    public void log(String string) {
        plugin.getLogger().info(string);
    }

    ConsoleCommandSender console = Bukkit.getServer().getConsoleSender();

    public void print(String text){
        console.sendMessage(ChatColor.AQUA+text);
        //It will print in sserver's console.
    }
}
