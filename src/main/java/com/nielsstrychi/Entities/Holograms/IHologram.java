package com.nielsstrychi.Entities.Holograms;

import com.nielsstrychi.Util.DivisionPosition;
import net.minecraft.server.v1_13_R2.ItemStack;

public interface IHologram {

    void setEquipment(ItemStack head, ItemStack body, ItemStack legs, ItemStack feet, ItemStack mainHand, ItemStack offHand);

    float spawnAngle();

    DivisionPosition spawnPosition();
}
