package com.nielsstrychi.Entities.Holograms;

import com.nielsstrychi.Util.DivisionPosition;
import net.minecraft.server.v1_13_R2.*;
import org.bukkit.entity.LivingEntity;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class Hologram extends EntitySkeletonWither implements IHologram {

     public Hologram(World world) {
             super(world);
             super.setSize(0.7F, 2.4F);
             super.fireProof = false;
             this.setNoAI(true);
             LivingEntity bukkitEntity = ((LivingEntity) this.getBukkitEntity());
             bukkitEntity.setGravity(false);
             bukkitEntity.setSilent(true);
             bukkitEntity.setInvulnerable(true);
             bukkitEntity.setCanPickupItems(false);
             bukkitEntity.setCollidable(false);
             bukkitEntity.setGlowing(true);
             bukkitEntity.addPotionEffect(new PotionEffect(PotionEffectType.INVISIBILITY, 10000, 1, true));
         }


    @Override
    protected void n() {
    }

    @Override
    protected void a(DifficultyDamageScaler difficultydamagescaler) {
    }

    @Override
    protected void initAttributes() {
        super.initAttributes();
    }

    @Override
    public void die(DamageSource damagesource) {

     }

    @Override
    public void setEquipment(ItemStack head, ItemStack body, ItemStack legs, ItemStack feet, ItemStack mainHand, ItemStack offHand) {

    }

    @Override
    public float spawnAngle() {
        return 0;
    }

    @Override
    public DivisionPosition spawnPosition() {
        return null;
    }
}

