package com.nielsstrychi.Entities.Soldiers;

import com.nielsstrychi.Data.Division;
import com.nielsstrychi.Handlers.AffiliateHandler;
import net.minecraft.server.v1_13_R2.EntityLiving;
import static org.bukkit.event.entity.EntityTargetEvent.TargetReason;

import java.util.Arrays;
import java.util.List;

/**
 * The behavior of a soldier when he wil attack an other entity.
 */
public enum Behavior {

    /**
     * Aggressive behavior will attack any non friendly entities (excluding animals & villagers...).
     */
    AGGRESSIVE(),

    /**
     * Passive behavior will only attack non friendly entities when they are commanded to.
     */
    PASSIVE(TargetReason.CLOSEST_ENTITY,
            TargetReason.CLOSEST_ENTITY,
            TargetReason.CLOSEST_PLAYER,
            TargetReason.TARGET_ATTACKED_NEARBY_ENTITY,
            TargetReason.TARGET_ATTACKED_OWNER,
            TargetReason.TARGET_ATTACKED_ENTITY,
            TargetReason.COLLISION,
            TargetReason.DEFEND_VILLAGE),


    /**
     * Assertive behavior will only attack non friendly entities when they have dealt damage to them,
     * or they are commanded to.
     */
    ASSERTIVE(
            TargetReason.CLOSEST_ENTITY,
            TargetReason.CLOSEST_PLAYER),

    /**
     * Same as aggressive behavior but will exclude strange players.
     */
    AGGRESSIVE_EXCLUDING_STRANGERS(
            TargetReason.CLOSEST_PLAYER),

    /**
     * Same as assertive behavior but will be aggressive to strange players.
     */
    ASSERTIVE_EXCLUDING_STRANGERS(
            TargetReason.CLOSEST_ENTITY);

    private final List<TargetReason> reasons;

    /**
     * Behavior constructor that takes the excluding reasons when not to attack.
     * @param reasons excluding factors to not attack an entity.
     */
    Behavior(final TargetReason... reasons) {
        this.reasons = Arrays.asList(reasons);
    }

    /**
     * This method will check if the soldier should atk the target or not corresponding to the reason and his behavior.
     * The relation between entity will be checked and if they are friendly the attack wil be canceled.
     * Unless friendly targeting is set to off by the player with is not the default setting.
     * @param soldier
     * @param target
     * @param reason
     * @return
     */
    public boolean atk(final Soldier soldier, final EntityLiving target, final TargetReason reason) {
        return soldier.hasDivision()
                ? (((Division) soldier.getPresentDivision()).isAttackingTargetDivision() && !AffiliateHandler.handle().friendlies(soldier, target)) //TODo test
                : !(this.reasons.stream().anyMatch(r -> r.equals(reason)) || AffiliateHandler.handle().friendlies(soldier, target));
    }
}
