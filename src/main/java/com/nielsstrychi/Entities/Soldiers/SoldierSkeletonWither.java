package com.nielsstrychi.Entities.Soldiers;

import com.nielsstrychi.Data.PatrolPoint;
import com.nielsstrychi.Data.SoldierData;
import com.nielsstrychi.Handlers.PathHandler;
import com.nielsstrychi.Handlers.PathfinderGoalHurtByTargetNonFriendlyAttackableTarget;
import com.nielsstrychi.Handlers.PathfinderGoalNearestNonFriendlyAttackableTarget;
import com.nielsstrychi.Handlers.TargetHandler;
import com.nielsstrychi.Strategy;
import com.nielsstrychi.Util.*;
import net.minecraft.server.v1_13_R2.*;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_13_R2.CraftWorld;
import org.bukkit.craftbukkit.v1_13_R2.entity.CraftEntity;
import org.bukkit.craftbukkit.v1_13_R2.entity.CraftLivingEntity;
import org.bukkit.craftbukkit.v1_13_R2.entity.CraftWitherSkeleton;
import org.bukkit.craftbukkit.v1_13_R2.event.CraftEventFactory;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityShootBowEvent;

import java.util.List;
import java.util.Optional;
import static org.bukkit.event.entity.EntityTargetEvent.TargetReason;

/**
 * A implementation of Soldier that extends the EntitySkeletonWither.
 *
 * @author Nielsstrychi
 */
public class SoldierSkeletonWither extends EntitySkeletonWither implements Soldier<SoldierSkeletonWither> {

    private static final EntityType ENTITY_TYPE = EntityType.WITHER_SKELETON;

    /**
     * The data that is stored in the database of this entity.
     * Only extra added data we require for logic is saved.
     * vanilla NBT is not saved and remains on the entity.
     */
    private final SoldierData data;

    /**
     * Represents the LivingEntity that the Soldier is currently following.
     * Is empty when there is no master to follow.
     */
    private Optional<LivingEntity> master;

    /**
     * Represents the LivingEntity that the Soldier is currently attacking.
     * Is empty when there is no target to follow.
     */
    private Optional<LivingEntity> target; //TODO replace goal target? reflection?

    /**
     * Represents the last reason why this Soldier is currently attacking a target.
     */
    private TargetReason targetReason = TargetReason.FORGOT_TARGET;

    /**
     * Represents if the Soldier has given up and flees the battle field.
     */
    private boolean flee;

    /**
     * Represents the Soldier his moral score.
     * If it reaches below zero there is chance the Soldier wil flee.
     */
    private int moral = 100;

    /**
     * Represents the Soldier his location goal to reach.
     * Is a far location the vanilla path finding cant reach.
     * When a value is present the PathHandler takes over until a point is reached,
     * that the vanilla path finding can take over.
     */
    private Optional<Location> finalLocation;

    /**
     * Constructor used by entity registry.
     * Do not use to spawn entities.
     * @param world where the entity wil be represented.
     */
    public SoldierSkeletonWither(@NotNull final World world) {
        super(world);
        this.data = null;
    }

    /**
     * Constructor used to spawn a Soldier.
     * Use EntityProvider.provide().spawnNewSoldier(EntityType, Location, Player);
     * To spawn a entity instead of this constructor.
     * @param world where the entity wil be represented.
     * @param owner that holds the rights to command this Soldier.
     */
    public SoldierSkeletonWither(@NotNull final World world, @NotNull final Player owner) {
        super(world);
        super.setSize(0.7F, 2.4F);
        super.fireProof = false;
        this.getAsBukkitEntity().setRemoveWhenFarAway(false);
        this.data = new SoldierData(this, owner);
    }

    /**
     * Constructor used to clone an existing vanilla entity back to a Soldier.
     * Bind data from previous entity to cast it to a new entity type of soldier.
     * @param data collected from the database
     * @param entity to be casted to a soldier
     */
    public SoldierSkeletonWither(@NotNull final SoldierData data, @NotNull final LivingEntity entity) {
        super(((CraftWorld) entity.getWorld()).getHandle());
        if (entity instanceof CraftWitherSkeleton) {
            this.data = data;
            data.setEntity(this);
            clone(entity);
        } else throw new IllegalArgumentException();
    }

    /**
     * See Soldier interface.
     */
    @Override
    public SoldierData getData() {
        return this.data;
    }

    /**
     * See Soldier interface.
     */
    @Override
    public EntityType getEntityType() {
        return ENTITY_TYPE;
    }

    /**
     * See Soldier interface.
     */
    @Override
    public SoldierSkeletonWither getAsOriginalEntity() {
        return this;
    }

    /**
     * See Soldier interface.
     */
    @Override
    public CraftEntity getBukkitEntity() {
        return super.getBukkitEntity();
    }

    /**
     * See Soldier interface.
     */
    @Override
    public void moveTo(final Location targetLocation, final float speed) {
        stopAttackEntity();
        this.finalLocation = Optional.ofNullable(targetLocation);
        PathHandler.handle().handlePathFinding(this);
    }

    /**
     * See Soldier interface.
     */
    @Override
    public Optional<Location> getFinalLocation() {
        return this.finalLocation;
    }

    /**
     * See Soldier interface.
     */
    @Override
    public void setFinalLocation(@Nullable final Location location) {
        this.finalLocation = Optional.ofNullable(location);
    }

    /**
     * See Soldier interface.
     */
    @Override
    public Location getCurrentLocation() {
        return this.getAsBukkitEntity().getLocation();
    }

    /**
     * See Soldier interface.
     */
    @Override
    public void setPath(Location location) {
        PathEntity path = this.getNavigation().a(location.getX(), location.getY(), location.getBlockZ());
        this.getNavigation().a(path, 1.75f); //Speed second parm
    }

    /**
     * See Soldier interface.
     */
    @Override
    public void setNewPatrolPoint(PatrolPoint point) {
    //TODO
    }

    /**
     * See Soldier interface.
     */
    @Override
    public void removePatrolPoint(int index) {
    //TODO
    }

    /**
     * See Soldier interface.
     */
    @Override
    public PatrolPoint getPatrolPoint(int index) {
        return null; //TODO
    }

    /**
     * See Soldier interface.
     */
    @Override
    public List getAllPatrolPoints() {
        return null; //TODO
    }

    /**
     * See Soldier interface.
     */
    @Override
    public boolean isFollowing() {
        return master.isPresent();
    }

    /**
     * See Soldier interface.
     */
    @Override
    public void stopFollowing(@Nullable final LivingEntity master) {
        this.master = Optional.empty();
        PathHandler.handle().cancelFollower(master, this);
    }

    /**
     * See Soldier interface.
     */
    @Override
    public Optional<LivingEntity> getFollowingEntity() {
        return this.master;
    }

    /**
     * See Soldier interface.
     */
    @Override
    public void setFollowingEntity(@Nullable final LivingEntity master) {
        this.master = Optional.ofNullable(master);
        if (this.master.isPresent()) {
            PathHandler.handle().newFollower(master, this);
        }
    }

    /**
     * See Soldier interface.
     */
    @Override
    public void startAttackEntity(@Nullable final LivingEntity entity) {
        this.finalLocation = Optional.empty();
        this.target = Optional.ofNullable(entity);
        if (this.getCurrentLocation().distance(entity.getLocation()) > PathHandler.MAX_PATH_FINDING_RADIUS) {
            if (this.getEquipment(EnumItemSlot.MAINHAND).getItem().equals(Items.BOW)
                    && Strategy.HOLD_SHOOT_HIGH.equals(Strategy.HOLD_SHOOT_HIGH/*todo entity atribute*/)) {
                System.out.println("ranged atk");
                super.setGoalTarget(Converter.convert(entity), TargetReason.CUSTOM, false);
                //TODO timer that checks the entity if they come to close they switch stratigy and stop high shooting.
            } else {
                PathHandler.handle().handleLongDistanceEnemyFinding(this, entity); //doest work anymore?
            }
        } else {
            this.setGoalTarget(((CraftLivingEntity) entity).getHandle(), TargetReason.OWNER_ATTACKED_TARGET,true);
        }
    }

    /**
     * See Soldier interface.
     */
    @Override
    public void startAttackEntity(Soldier soldier) {
        this.startAttackEntity( soldier.getAsBukkitEntity());
    }

    @Override
    public void stopAttackEntity() {
        this.target = Optional.empty();
        TargetHandler.handle().start(this, null);
        this.setGoalTarget(null, TargetReason.FORGOT_TARGET,false);
    }


    /** Vanilla **/
    @Override
    protected void n() {
        this.goalSelector.a(6, new PathfinderGoalLookAtPlayer(this, EntityHuman.class, 8.0F));
        this.targetSelector.a(1, new PathfinderGoalHurtByTargetNonFriendlyAttackableTarget(this, false, new Class[0]));
        this.targetSelector.a(2, new PathfinderGoalNearestNonFriendlyAttackableTarget(this, EntityHuman.class, true));
        this.targetSelector.a(2, new PathfinderGoalNearestNonFriendlyAttackableTarget(this, EntityMonster.class, true));
        //TODO add humen atk
    }

    /**
     * Runs away from everything
     */
    @Override
    public void moralHit() {
        this.flee = true;
        this.goalSelector.a(3, new PathfinderGoalAvoidTarget(this, EntityLiving.class, 6.0F, 1.0D, 1.2D));
        this.data.delete(true); //TODO batch testing
    }

    /**
     * Runs away from everything
     */
    @Override
    public boolean isAlive() {
        return super.isAlive();
    }

    /**
     * Runs away from everything
     */
    @Override
    public boolean isAttacking() {
        if (target.isPresent() && target.get().isDead()) {
            target = Optional.empty();
        }
        return target.isPresent();
    }








    /* ------------------------------------- Vanilla Implementations Overridden ----------------------------------- */

    /**
     * This method returns in vanilla minecraft a true or false state if the entity needs to be on fire.
     * We now return always false so the entity wil not be set on fire in the movementTick() method of parent EntitySkeletonAbstract.
     * @return always false.
     */
    @Override @Vanilla
    protected boolean dq() {
        return false;
    }

    /**
     * Override some basic vanilla attributes of the entity.
     */
    @Override @Vanilla
    protected void initAttributes() {
        super.initAttributes();
        this.getAttributeInstance(GenericAttributes.MOVEMENT_SPEED).setValue(0.25D);
    }

    /**
     * The dead event when a entity dies.
     * Soldier gets removed from his division if present.
     * If the division is completely wiped out a message to the owner wil be displayed his division is,
     * destroyed when online and the division wil be deleted in the database.
     * Also the dying soldier wil be deleted from the database.
     * Then the super method of the vanilla die event wil process further.
     * @param damageSource we propagate this to the super method.
     */
    @Override @Vanilla
    public void die(DamageSource damageSource) {
        //TODO send message legio got compleetly destoyed and delete division.
        this.getDivision().ifPresent(d -> {if(d.soldierCount() == 1) {
            if (this.getPresentDivision().isAttackingTargetDivision()) {
                this.getPresentDivision().stopAttackingTargetDivision();
            }
            System.out.println("legiodead");
        }});
        this.getData().delete(true); // todo test batching
        super.die(damageSource);
    }

    /**
     * Calls the vanilla super method and propagate the values.
     * @param x represents the longitude with the x-axis on the world, distance east (positive) or west (negative).
     * @param y represents the elevation with the y-axis on the world, high or low (from 0 to 255, with 64 being sea level)
     * @param z represents the latitude with the z-axis on the world, distance south (positive) or north (negative).
     * @param yaw yaw-axis is the angle between the longitudinal axis, left to right.
     * @param pitch pitch-axis is the angle of the elevation axis, up to down.
     */
    @Override @Vanilla
    public void setLocation(final double x, final double y, final double z, final float yaw, final float pitch) {
        super.setLocation(x, y, z, yaw, pitch);
    }

    /**
     * Vanilla method override to attack a entity.
     * Added extra logic so when they are not fleeing they attack non friendly targets.
     * @param target a target that needs to be defined friendly or not.
     * @param reason the reason why the Soldier attacks.
     * @param fireEvent idk vanilla parameter.
     * @return true if the attack can proceed.
     */
    @Override @Vanilla
    public boolean setGoalTarget(EntityLiving target, TargetReason reason, boolean fireEvent) {
        if (!flee) {
            if ((TargetReason.CLOSEST_ENTITY.equals(reason) || TargetReason.CLOSEST_PLAYER.equals(reason))) {
                if (TargetReason.FORGOT_TARGET.equals(this.targetReason) || TargetReason.TARGET_DIED.equals(this.targetReason)) {
                    this.targetReason = reason;
                    return this.setGoalTargetHelper(target, reason, fireEvent);
                } else {
                    return true;
                }
            } else {
                this.targetReason = reason;
                return this.setGoalTargetHelper(target, reason, fireEvent);
            }
        } else {
            return false;
        }
    }

    /**
     * Private helper method to null check data,
     * and then check if the target is not friendly.
     * @param target the target to attack.
     * @param reason why the soldier attacks.
     * @param fireEvent idk vanilla property.
     * @return true if the attack can proceed.
     */
    private boolean setGoalTargetHelper(EntityLiving target, TargetReason reason, boolean fireEvent) {
        return (this.getData() != null
                ? this.getData().getBehavior().atk(this, target, reason) && super.setGoalTarget(target, reason, fireEvent)
                : super.setGoalTarget(target, reason, fireEvent));
    }

    @Override @Vanilla
    public void a(EntityLiving entityliving, float f) {
        EntityArrow entityarrow = this.a(f);
        double d0 = entityliving.locX - this.locX;
        double d1 = entityliving.getBoundingBox().minY + (double)(entityliving.length / 3.0F) - entityarrow.locY;
        double d2 = entityliving.locZ - this.locZ;
        double d3 = (double)MathHelper.sqrt(d0 * d0 + d2 * d2);
        entityarrow.shoot(d0, d1 + d3 * 0.20000000298023224D, d2, 1.6F, (float)(14 - this.world.getDifficulty().a() * 4));
        EntityShootBowEvent event = CraftEventFactory.callEntityShootBowEvent(this, this.getItemInMainHand(), entityarrow, 0.8F);
        if (event.isCancelled()) {
            event.getProjectile().remove();
        } else {
            if (event.getProjectile() == entityarrow.getBukkitEntity()) {
                this.world.addEntity(entityarrow);
            }

            this.a(SoundEffects.ENTITY_SKELETON_SHOOT, 1.0F, 1.0F / (this.getRandom().nextFloat() * 0.4F + 0.8F));
        }
    }

    @Override @Vanilla //Todo configurable
    protected boolean isDropExperience() {
        return false;
    }

}

