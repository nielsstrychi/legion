package com.nielsstrychi.Entities.Soldiers;

import com.nielsstrychi.Data.OwnerData;
import org.bukkit.Location;

import java.util.Optional;
import java.util.UUID;

/**
 * A player owned entity.
 */
public interface PlayerOwnedEntity<DATA extends OwnerData> {

    DATA getData();

    Optional<Location> getFinalLocation();

    void setFinalLocation(final Location location);

    Location getCurrentLocation();

    void setPath(final Location location);

    /**
     * Retrieves only the UUID of a owner if the Player object is not needed.
     * @return UniqueId of the owner like it is stored in the database.
     */
    default UUID getPlayerUniqueId() {
        return this.getData().getPlayerUUID();
    }
}
