package com.nielsstrychi.Entities.Soldiers;

import com.nielsstrychi.Data.PatrolPoint;
import com.nielsstrychi.Data.Division;
import com.nielsstrychi.Handlers.GlowHandler;
import com.nielsstrychi.Util.*;
import com.nielsstrychi.Data.SoldierData;
import net.minecraft.server.v1_13_R2.EntityMonster;
import net.minecraft.server.v1_13_R2.EnumItemSlot;
import net.minecraft.server.v1_13_R2.ItemStack;
import net.minecraft.server.v1_13_R2.NBTTagCompound;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.OfflinePlayer;
import org.bukkit.craftbukkit.v1_13_R2.entity.CraftEntity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;

import java.util.*;

/**
 * A player owned soldier.
 */
public interface Soldier<E extends EntityMonster> extends PlayerOwnedEntity<SoldierData> {

    E getAsOriginalEntity();

    @Override
    SoldierData getData();

    void moveTo(Location loc, float  speed);

    CraftEntity getBukkitEntity();

    void setNewPatrolPoint(PatrolPoint point);
    void removePatrolPoint(int index);
    PatrolPoint getPatrolPoint(int index);
    List getAllPatrolPoints();

    void setFollowingEntity(LivingEntity entity);
    Optional<LivingEntity> getFollowingEntity();
    boolean isFollowing();

    void stopFollowing(@Nullable final LivingEntity master);

    void startAttackEntity(LivingEntity entity);

    void startAttackEntity(Soldier soldier);

    void stopAttackEntity();

    ItemStack getEquipment(EnumItemSlot head);

    void setSlot(final EnumItemSlot head, final ItemStack asNMSCopy);

     void moralHit();

    boolean isAttacking();

    /**
     * Retrieves the entity type static declared by the entity class.
     * @return EntityType enum that always must represent the correct EntityType.
     */
    EntityType getEntityType();

    /**
     * Calls the super method of the implementing entity.
     * @return true if the entity alive, false if the entity is dead.
     */
    @Vanilla
    boolean isAlive();

    /**
     * Calls the super method of the implementing entity.
     * @param x represents the longitude with the x-axis on the world, distance east (positive) or west (negative).
     * @param y represents the elevation with the y-axis on the world, high or low (from 0 to 255, with 64 being sea level)
     * @param z represents the latitude with the z-axis on the world, distance south (positive) or north (negative).
     * @param yaw yaw-axis is the angle between the longitudinal axis, left to right.
     * @param pitch pitch-axis is the angle of the elevation axis, up to down.
     */
    @Vanilla
    void setLocation(final double x, final double y, final double z, final float yaw, final float pitch);


    /**
     * Clones the original entity with this new instance representing the same state.
     * @param original entity with the state wil be cloned.
     */
    default void clone(@NotNull final LivingEntity original) {
        syncMetaData(original);
    }

    /**
     * Synchronises the vanilla NBT data with the new instance.
     * Al properties are taken over even the UUID,
     * so the clone wil be the exact state of its original.
     * @param original entity with the NBT that wil be cloned.
     */
    default void syncMetaData(@NotNull final LivingEntity original) {
        this.getAsOriginalEntity().f(((CraftEntity) original).getHandle().save(new NBTTagCompound()));
    }

    /**
     * default method that wil split up a location.
     * This calls a parent method with the parameters retrieved from location.
     * @param loc location object we wil retrieve values from.
     */
    default void setLocation(@NotNull final Location loc) {
        this.setLocation(loc.getX(), loc.getY(), loc.getZ(), loc.getYaw(), loc.getPitch());
    }

    /**
     * Converts the Instance Soldier to a Bukkit Entity.
     * @return Soldier as a Bukkit Entity.
     */
    default LivingEntity getAsBukkitEntity() {
        return Converter.convert(this.getAsOriginalEntity());
    }

    /**
     * Set the division of a Soldier, propagates trough and calls the method on SoldierData.
     * @param division where the soldier will be set to.
     */
    default void setDivision(@Nullable final Division division) {
        this.getData().setDivision(division);
    }

    /**
     * Checks if the soldier is part of a division.
     * @return true if the soldier is a member of a division if not false.
     */
    default boolean hasDivision() {
        return getDivision().isPresent();
    }

    /**
     * Returns a Optional of division.
     * You may need to cast to a division when retrieving it with .get().
     * @return Empty if no is present else division wrapped around a optional.
     */
    default Optional<Division> getDivision() {
        return this.getData().getDivision();
    }

    /**
     * Gets the division as a object not as an optional. You need to check if a division is present before calling this.
     * @return the Division, never returns null a exception is thrown when you call it without making sure a division is present.
     * @throws NoSuchElementException when you called this method without checking if the value is present first.
     */
    default Division getPresentDivision() throws NoSuchElementException {
        return (Division) this.getData().getDivision().get();
    }

    /**
     * Checks if the Soldier is the commander of this legio.
     * This is visually defined of the banner he is carrying.
     * @return true if the Soldier is the commander.
     */
    default boolean isCommander() {
        return getData().getDivision().isPresent()
                && If.isBanner(this.getEquipment(EnumItemSlot.HEAD))
                && Converter.convert(this.getEquipment(EnumItemSlot.HEAD)).hasItemMeta()
                && Objects.requireNonNull(Converter.convert(this.getEquipment(EnumItemSlot.HEAD)).getItemMeta())
                .getDisplayName().startsWith(ChatColor.GOLD + "Legio");
    }

    /**
     * Sets the DivisionPosition by assigning the Soldier the Division Position Identifier of offset.
     * @param offset where the DPI wil be set to the Soldier.
     */
    default void setOffset(@NotNull final DivisionPosition offset) {
        getData().setDPI((byte) (offset.getId()));
    }

    /**
     * Get the Division Position Offset values to say where the entity needs to start in his formation.
     * @return DivisionPosition that holds the x & y axis, rotation angle and a Division Position Identifier.
     */
    default DivisionPosition getOffset() {
        return this.getPresentDivision().getDivisionPosition(getData().getDPI());
    }

    /**
     * The Division Position Identifier, identifies the position the entity wil take when standing in formation.
     * @return a byte for 1-60 that identifies the position the soldier is assigned when standing in a formation.
     */
    default byte getDPI() {
        return this.getData().getDPI();
    }

    /**
     * Defines if a entity should be glowing or not.
     * @param player that sees the glowing if the soldier glows.
     * @return true if the query has found a glowing state on the soldier else false.
     */
    default boolean isGlowing(@NotNull final Player player) {
        return GlowHandler.handle().query(player, this.getAsBukkitEntity()).isPresent();
    }

    /**
     * Gets the owner of a Soldier.
     * @return a OfflinePlayer because the owner can be offline or online.
     */
    default OfflinePlayer getPlayer() {
        return this.getData().getPlayer();
    }

    /**
     * Sets a new owner to the soldier.
     * @param player that wil become new owner of the Soldier.
     */
    default void setPlayer(@NotNull final Player player) {
        this.getData().setPlayer(player);
    }

    /**
     * Propagate update with the default value true to batch this query.
     */
    default void update() {this.update(true);}

    /**
     * Calls the SoldierData method to update th Soldier.
     * @param batch if true the update statement will be executed in a batch.
     */
    default void update(boolean batch) {
        this.getData().update(batch);
    }
}
