package com.nielsstrychi.Entities.Soldiers;

import net.minecraft.server.v1_13_R2.EntityInsentient;
import net.minecraft.server.v1_13_R2.EntitySkeletonWither;
import net.minecraft.server.v1_13_R2.EntityTypes;


public enum  EntityType {

    WITHER_SKELETON("soldier:wither_skeleton", EntityTypes.WITHER_SKELETON, EntitySkeletonWither.class, SoldierSkeletonWither.class);

    private String name;
    private EntityTypes<? extends EntityInsentient> entityType;
    private Class<? extends EntityInsentient> nmsClass;
    private Class<? extends EntityInsentient> customClass;

    private EntityType(String name, EntityTypes<? extends EntityInsentient> entityType,
                             Class<? extends EntityInsentient> nmsClass,
                             Class<? extends EntityInsentient> customClass) {
        this.name = name;
        this.entityType = entityType;
        this.nmsClass = nmsClass;
        this.customClass = customClass;
    }


    public String getName() {
        return name;
    }

    public EntityTypes getEntityType() {
        return entityType;
    }

    public Class<? extends EntityInsentient> getNMSClass() {
        return nmsClass;
    }

    public Class<? extends EntityInsentient> getCustomClass() {
        return customClass;
    }

}
