package com.nielsstrychi;

import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

import java.util.Optional;
import java.util.UUID;

public class Util2 {

    public static Optional<Player> getPlayerByUUID(UUID uuid) {
        Player player = Bukkit.getServer().getPlayer(uuid);
        return player!=null
                ? Optional.of(player)
                : Optional.empty();
    }
}
