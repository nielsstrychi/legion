package com.nielsstrychi.Data;

import com.nielsstrychi.Util2;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import java.io.Serializable;
import java.util.Optional;
import java.util.UUID;

@MappedSuperclass
public abstract class OwnerData extends Data {

    @Column(name = "OWNER", columnDefinition = "BINARY(16)", nullable = true)
    private UUID player;

    public OfflinePlayer getPlayer() { //TODO
       return Bukkit.getOfflinePlayer(player);
    }

    public UUID getPlayerUUID() {
        return this.player;
    }

    public void setPlayer(UUID player) {
        this.player = player;
    }

    public void setPlayer(Player player) {
        this.player = player.getUniqueId();
    }
}


