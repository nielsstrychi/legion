package com.nielsstrychi.Data;

import com.nielsstrychi.Entities.Soldiers.Soldier;
import com.nielsstrychi.Handlers.GlowHandler;
import com.nielsstrychi.Tiers;
import com.nielsstrychi.Packets.GlowColor;
import com.nielsstrychi.Providers.ContextProvider;
import com.nielsstrychi.Repositories.SQL;
import com.nielsstrychi.Util.DivisionFormation;
import com.nielsstrychi.Util.DivisionPosition;
import com.nielsstrychi.Util2;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import javax.persistence.*;
import java.util.*;
import java.util.stream.Collectors;

@Entity
@Table(name = "DIVISIONS")
@NamedQueries({
        @NamedQuery(name = "Division.FIND_ALL", query = "SELECT division FROM Division division"),
        @NamedQuery(name = "Division.FIND_ALL_FROM_PLAYER_UUID", query = "SELECT division FROM Division division WHERE division.player = :player"),
        @NamedQuery(name = "Division.FIND_ALL_LEGIONAMES_FROM_PLAYER_UUID", query = "SELECT division.tier FROM Division division WHERE division.player = :player")
})
public class Division extends Data{

    public static final String FIND_ALL = "Division.FIND_ALL";
    public static final String FIND_ALL_FROM_PLAYER_UUID = "Division.FIND_ALL_FROM_PLAYER_UUID";
    public static final String FIND_ALL_LEGIONAMES_FROM_PLAYER_UUID = "Division.FIND_ALL_LEGIONAMES_FROM_PLAYER_UUID";

    public static final int MAX = 60;

    @OneToMany(mappedBy = "division", fetch = FetchType.EAGER)
    private List<SoldierData> division = new ArrayList<>();

    @Column(name = "FORMATION")
    @Enumerated(EnumType.STRING)
    private DivisionFormation formation = DivisionFormation.DEFAULT;

    @Column(name = "TIER")
    @Enumerated(EnumType.STRING)
    private Tiers tier;

    private String name;

    @Column(name = "OWNER", columnDefinition = "BINARY(16)", nullable = false)
    private UUID player;

    @Transient private DivisionPosition[] lineOfFormation = formation.getFormationPositions();

    @Transient private Optional<Division> targetDivision = Optional.empty();

    @PostUpdate private void postUpdate() {
         updateLineOfFormation();
        /* updateSoldiers();*/
     }

    private void updateLineOfFormation() {this.lineOfFormation = formation.getFormationPositions();}
   /* private void updateSoldiers() {this.division = }*/

    public Division() {}

    public Division(Soldier leader, Player owner) {

        //TODO check valid banner take name set legio if invlaid throw exception and catch in bmethod
        leader.getDivision().ifPresent(d -> {
            ((Division) d).removeSoldier((leader));
            leader.setDivision(null);
        });
        this.player = owner.getUniqueId();
        addSoldier(leader);
        name = "Nielsstrychi"; //hard coded currently
        save();
    }

    public boolean addSoldier(SoldierData soldier) {
        if (division.size() < MAX) {
            division.add(soldier);
            soldier.setDivision(this);
            reFormatIDs();
          //  division.forEach(ContextProvider.ENTITY_REPOSITORY::update);
            return true;
        }
        return false;
    }

    public boolean addSoldier(Soldier soldier) {
        return addSoldier(soldier.getData());
    }

    public void removeSoldier(Soldier soldier) {
        this.removeSoldier(soldier.getData());
    }

    public void removeSoldier(SoldierData soldier) {
        soldier.setDivision(null);
        soldier.setDPI(null);
        division.remove(soldier);
        //this.reFormatIDs(); //TODO reformat after divison attack
    }

    public int soldierCount() {
        return this.getDivisionList().size();
    }

    //TODO when numbers disapeare they dont start from the bottom to count

    private void reFormatIDs() {
        System.out.println("reformat " + division.size());//TODO always id 60 needs fix
        List<Soldier> soldiers = this.getDivisionList();
        System.out.println("reformat " + soldiers.size());//TODO always id 60 needs fix

        for (int i = 0 ; i < soldiers.size() ; i++) {
            Soldier soldier = soldiers.get(i);
            soldier.setOffset(lineOfFormation[i]);
            soldier.update(true); //TODO update in batch
            //TODO say go to position
        }
        this.getDivisionList().sort(Comparator.comparing((Soldier::getDPI)));
    }

    public void setFormation(DivisionFormation formation) {
        this.formation = formation;
        updateLineOfFormation();
    }

    public List<Soldier> getDivisionList() {
        return division.stream()
                .map(SoldierData::getEntity)
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
    }

    public Player getPlayer() {
        return Util2.getPlayerByUUID(player).orElseThrow(NullPointerException::new); //TODO
    }

    public DivisionPosition getDivisionPosition(byte id) {
        return lineOfFormation[id-1];
    }

    public Tiers getTier() {
        return tier;
    }

    public void setTier(Tiers tier) {
        this.tier = tier;
    }

    public UUID getPlayerUUID() {
        return player;
    }

    public void setPlayer(UUID player) {
        this.player = player;
    }

    public String getName() {
        return name;
    }

    public String getFullName() {
        return ChatColor.GOLD + "Legio " + tier.name() + " " + name;
    }

    public DivisionFormation getFormation() {
        return formation;
    }

    private void update() {
        ContextProvider.DIVISION_REPOSITORY.update(this);
    }

    public void attack(final Division enemyDivision) {
        this.startAttackingTargetDivision(enemyDivision);
        Map<Byte, Soldier> enemySoldierMap = enemyDivision
                .getDivisionList()
                .stream()
                .collect(Collectors.toMap(Soldier::getDPI, soldier -> soldier));
        this.getDivisionList().forEach(attacker -> {
            Soldier enemy = enemySoldierMap.get((byte) (attacker.getDPI() +
                    ((attacker.getDPI() % 2 == 0) ? -1 : 1))); // even attacks uneven and otherwise.
            if (enemy == null) {
                enemySoldierMap.values()
                        .stream()
                        .filter(Objects::nonNull)
                        .filter(Soldier::isAlive)
                        .findAny()//TODO not random
                        .ifPresent(attacker::startAttackEntity);
            } else {
                attacker.startAttackEntity(enemy);
            }
        });
    }

    public Division getTargetDivision() {
        return this.targetDivision.orElseThrow(NullPointerException::new);
    }

    public boolean isAttackingTargetDivision() {
        return this.targetDivision.isPresent();
    }

    public void stopAttackingTargetDivision() {
        reFormatIDs();
        if (isAttackingTargetDivision()) {
            this.getTargetDivision().getDivisionList().forEach(enemy -> GlowHandler.handle().disableGlow(this.getPlayer(), enemy.getAsBukkitEntity()));
        }
        this.targetDivision = Optional.empty();
    }

    private void startAttackingTargetDivision(Division division) {
        division.getDivisionList().forEach(enemy -> GlowHandler.handle().enableGlow(
                this.getPlayer(),
                enemy.getAsBukkitEntity(),
                GlowColor.ENEMY_LEGIO_ATK));
        this.targetDivision = Optional.of(division);
    }

    public void delete(boolean batch) {
        if (batch) {
            ContextProvider.DIVISION_REPOSITORY.batch(this, SQL.DELETE);
        } else {
            ContextProvider.DIVISION_REPOSITORY.delete(this);
        }
    }

    public void update(boolean batch) {
        if (batch) {
            ContextProvider.DIVISION_REPOSITORY.batch(this, SQL.UPDATE);
        } else {
            ContextProvider.DIVISION_REPOSITORY.update(this);
        }
    }

    private void save() {
        ContextProvider.DIVISION_REPOSITORY.save(this);
    }
}
