package com.nielsstrychi.Data;

import com.nielsstrychi.Movement;
import com.nielsstrychi.PatrolRoute;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;

import javax.persistence.*;

@Entity
@Table(name = "POINTS")
public class PatrolPoint extends Data {

    @Transient
    private Location location;

    @Transient
    private World world;

    @Column(name = "X")
    private double x;
    @Column(name = "Y")
    private double y;
    @Column(name = "Z")
    private double z;
    @Column(name = "WORLD")
    private String worldName;
    @Column(name = "MOVEMENT")
    private Movement movement;

    @ManyToOne
    @JoinColumn(name = "ROUTE", nullable = false)
    private PatrolRoute route;

    public PatrolPoint() {
    }

    public PatrolPoint(Location loc, Movement movement, PatrolRoute route) {
        this.movement= movement;
        this.location = loc;
        this.route = route;
        this.x = this.location.getX();
        this.y = this.location.getY();
        this.z = this.location.getZ();
        this.world = this.location.getWorld();
        assert this.world != null;
        this.worldName = this.world.getName();
    }

    @PostLoad
    private void sync() {
        this.location = new Location(
                this.world = Bukkit.getWorld(worldName), x,y,z);
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public Movement getMovement() {
        return movement;
    }

    public void setMovement(Movement movement) {
        this.movement = movement;
    }

    public World getWorld() {
        return world;
    }

    public void setWorld(World world) {
        this.world = world;
    }

    public PatrolRoute getRoute() {
        return route;
    }

    public void setRoute(PatrolRoute route) {
        this.route = route;
    }
}
