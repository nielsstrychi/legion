package com.nielsstrychi.Data;

import com.nielsstrychi.Entities.Soldiers.Behavior;
import com.nielsstrychi.Entities.Soldiers.EntityType;
import com.nielsstrychi.Entities.Soldiers.Soldier;
import com.nielsstrychi.Entities.Soldiers.SoldierSkeletonWither;
import com.nielsstrychi.PatrolRoute;
import com.nielsstrychi.Providers.ContextProvider;
import com.nielsstrychi.Providers.EntityProvider;
import com.nielsstrychi.Repositories.SQL;
import com.nielsstrychi.Util2;
import org.bukkit.entity.Player;

import javax.annotation.Nullable;
import javax.persistence.*;
import java.util.Optional;
import java.util.UUID;

@Entity
@Table(name = "SOLDIERS")
@NamedQueries({
        @NamedQuery(name = "SoldierData.FIND_ALL", query = "SELECT soldier FROM SoldierData soldier")
})
public class SoldierData extends OwnerData {
    public static final String FIND_ALL = "SoldierData.FIND_ALL";

    @Enumerated(EnumType.STRING)
    @Column(name = "ENTITY_TYPE", nullable = false, updatable = false)
    private EntityType type;

    @Enumerated(EnumType.STRING)
    @Column(name = "ENTITY_BEHAVIOR", nullable = false)
    private Behavior behavior = Behavior.ASSERTIVE;

    @ManyToOne
    @JoinColumn(name = "ROUTE")
    private PatrolRoute route;

    @ManyToOne
    @JoinColumn(name = "DIVISION")
    private Division division;

    @Column(name = "DIVISION_POSITION_IDENTIFIER")
    private Byte DPI;

    @Transient
    private Soldier entity;

    /**
     * Constructor used by Hibernate for managed SoldierData object
     */
    public SoldierData() {
    }

    /**
     * Constructor used for new SoldierData object
     */
    public SoldierData(SoldierSkeletonWither entity, Player player) {
        this.entity = entity;
        super.uniqueID = entity.getUniqueID();
        this.type = entity.getEntityType();
        this.setPlayer(player);
        this.save();
    }

    public EntityType getEntityType() {
        return type;
    }

    @Nullable
    public Soldier getEntity() {
        return this.entity = (entity == null)
                ? EntityProvider.provide().getSoldierEntity(getUniqueId())
                : entity;
    }

    public SoldierData setEntity(Soldier entity) {
        this.entity = entity;
        return this;
    }

    public Optional<PatrolRoute> getRoute() {
        return Optional.of(route);
    }

    public SoldierData setRoute(PatrolRoute route) {
        this.route = route;
        return this;
    }

    public Optional<Division> getDivision() {
        return Optional.ofNullable(division);
    }

    public SoldierData setDivision(Division division) {
        this.division = division;
        return this;
    }

    public Byte getDPI() {
        return DPI;
    }

    public SoldierData setDPI(Byte divisionPositionIdentifier) {
        this.DPI = divisionPositionIdentifier;
        return this;
    }

    public SoldierData chainPlayer(UUID player) {
        super.setPlayer(player);
        return this;
    }

    public SoldierData chainPlayer(Player player) {
        super.setPlayer(player);
        return this;
    }

    public Behavior getBehavior() {
        return behavior;
    }

    public void setBehavior(Behavior behavior) {
        this.behavior = behavior;
    }

    public void delete(boolean batch) {
        this.getDivision().ifPresent(d -> {
            d.removeSoldier(this);
            this.setDivision(null);
        });
        if (batch) {
            ContextProvider.ENTITY_REPOSITORY.batch(this, SQL.DELETE);
        } else {
            ContextProvider.ENTITY_REPOSITORY.delete(this);
        }
    }

    public void update(boolean batch) {
        if (batch) {
            ContextProvider.ENTITY_REPOSITORY.batch(this, SQL.UPDATE);
        } else {
            ContextProvider.ENTITY_REPOSITORY.update(this);
        }
    }

    public void save() {
        ContextProvider.ENTITY_REPOSITORY.save(this);
    }
}
