package com.nielsstrychi.Data;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import java.io.Serializable;
import java.util.UUID;

@MappedSuperclass
public abstract class Data implements Serializable {

    @Id
    @Column(name = "UUID", columnDefinition = "BINARY(16)", nullable = false, updatable = false)
    protected UUID uniqueID = UUID.randomUUID();

    public UUID getUniqueId() {
        return this.uniqueID;
    }
}
