package com.nielsstrychi.Data;

import org.bukkit.entity.Player;

import javax.persistence.*;
import java.util.UUID;

/**
 * Database Entity that shows 2 Players are a affiliate of each other.
 * So they have advantages like:
 *  - Marking there troops in a friendly color pattern.
 *  - Disabling friendly targeting.
 *
 * @Author Nielsstrychi
 */
@Entity
@Table(name = "AFFILIATIONS")
@NamedQueries({
        @NamedQuery(name = "Affiliate.FIND_PLAYER_AFFILIATES", query = "SELECT affiliate FROM Affiliate affiliate WHERE (affiliate.associateOne = :player) OR (affiliate.associateTwo = :player)"),
        @NamedQuery(name = "Affiliate.FIND_AFFILIATE", query = "SELECT affiliate FROM Affiliate affiliate WHERE (affiliate.associateOne = :playerTwo AND affiliate.associateTwo = :playerOne) OR (affiliate.associateOne = :playerOne AND affiliate.associateTwo = :playerTwo)")
})
public class Affiliate extends Data {
    public static final String FIND_PLAYER_AFFILIATES = "Affiliate.FIND_PLAYER_AFFILIATES";
    public static final String FIND_AFFILIATE = "Affiliate.FIND_AFFILIATE";

    @Column(name = "ASSOCIATE_ONE", columnDefinition = "BINARY(16)", nullable = false)
    private UUID associateOne;

    @Column(name = "ASSOCIATE_TWO", columnDefinition = "BINARY(16)", nullable = false)
    private UUID associateTwo;

    public Affiliate() {
    }

    public Affiliate(UUID associateOne, UUID associateTwo) {
        this.associateOne = associateOne;
        this.associateTwo = associateTwo;
    }

    public Affiliate(Player associateOne, Player associateTwo) {
        this(associateOne.getUniqueId(), associateTwo.getUniqueId());
    }

    public UUID getAssociateOne() {
        return associateOne;
    }

    public void setAssociateOne(UUID associateOne) {
        this.associateOne = associateOne;
    }

    public UUID getAssociateTwo() {
        return associateTwo;
    }

    public void setAssociateTwo(UUID associateTwo) {
        this.associateTwo = associateTwo;
    }
}
