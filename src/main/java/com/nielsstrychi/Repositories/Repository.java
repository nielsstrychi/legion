package com.nielsstrychi.Repositories;

import com.nielsstrychi.Data.Data;
import com.nielsstrychi.Data.SoldierData;

import java.util.*;

public interface Repository<D extends Data> {

    void update(D data);
    void update(List<D> data);
    void update(Set<D> data);
    void save(D data);
    void save(List<D> data);
    void save(Set<D> data);
    Optional<D> findByUUID(UUID uuid);
    List<D> findAll();


    void batch(D data, SQL action);
    void batch(List<D> data, SQL action);
    Set<D> retrieveBatch(SQL key);
    void clearBatch();
    void executeBatch(SQL... key);

}
