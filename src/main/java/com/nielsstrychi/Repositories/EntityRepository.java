package com.nielsstrychi.Repositories;


import com.nielsstrychi.Data.Data;
import com.nielsstrychi.Entities.Soldiers.Soldier;
import com.nielsstrychi.Exceptions.IllegalDataException;
import com.nielsstrychi.Providers.PersistenceProvider;
import com.nielsstrychi.Data.SoldierData;

import javax.persistence.EntityManager;
import java.util.*;


public class EntityRepository implements Repository<SoldierData>{

    private Map<SQL, Set<SoldierData>> BATCH = new HashMap<>();

    @Override
    public void update(SoldierData data) {
        try(PersistenceProvider provider = PersistenceProvider.provide()) {
            provider.beginSession().merge(data);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void update(Set<SoldierData> data) {
        try(PersistenceProvider provider = PersistenceProvider.provide()) {
            EntityManager session = provider.beginSession();
            data.forEach(session::merge);
            session.flush();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void update(List<SoldierData> data) {
        this.update(new HashSet<>(data));
    }

    @Override
    public void save(SoldierData data) {
        try(PersistenceProvider provider = PersistenceProvider.provide()) {
            provider.beginSession().persist(data);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void save(Set<SoldierData> data) {
        try(PersistenceProvider provider = PersistenceProvider.provide()) {
            EntityManager session = provider.beginSession();
            data.forEach(session::persist);
            session.flush();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void save(List<SoldierData> data) {
        this.save(new HashSet<>(data));
    }

    @Override
    public Optional<SoldierData> findByUUID(UUID uuid) {
        return Optional.empty();
    }

    @Override
    public List<SoldierData> findAll() {
       try(PersistenceProvider provider = PersistenceProvider.provide()) {
           return provider.beginSession()
                   .createNamedQuery(SoldierData.FIND_ALL, SoldierData.class)
                   .getResultList();
       } catch (Exception e) {
           throw new IllegalArgumentException(e);
       }
   }

    @Override
    public void batch(SoldierData data, SQL action) {
        this.retrieveBatch(action).add(data);
    }

    @Override
    public void batch(List<SoldierData> data, SQL action) {
        this.retrieveBatch(action).addAll(data);
    }

    @Override
    public Set<SoldierData> retrieveBatch(SQL key) {
        return this.BATCH.computeIfAbsent(key, set -> new HashSet<>());
    }

    @Override
    public void clearBatch() {
        this.BATCH.clear();
    }

    @Override
    public void executeBatch(SQL... key) {
        List<SQL> keys = Arrays.asList(key);
        if (keys.contains(SQL.SAVE)) this.save(new HashSet<>(this.retrieveBatch(SQL.SAVE)));
        if (keys.contains(SQL.UPDATE)) this.update(new HashSet<>(this.retrieveBatch(SQL.UPDATE)));
        if (keys.contains(SQL.DELETE)) this.delete(new HashSet<>(this.retrieveBatch(SQL.DELETE)));
        clearBatch();
    }

    private SoldierData checkInstanceEqualsSoldierData(final Data instance) {
        if (instance instanceof SoldierData)
            return (SoldierData) instance;
        else
            throw new IllegalDataException(
                    "Method called with a parameter that is not an instance of SoldierData, Instance="
                            +instance.toString());
    }

    public void delete(SoldierData data) {
        try(PersistenceProvider provider = PersistenceProvider.provide()) {
            EntityManager manager = provider.beginSession();
            manager.remove(manager.contains(data)
                    ? data
                    : manager.merge(data));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void delete(Set<SoldierData> data) {
        try(PersistenceProvider provider = PersistenceProvider.provide()) {
            EntityManager manager = provider.beginSession();
            data.forEach(d -> {
            manager.remove(manager.contains(d)
                    ? d
                    : manager.merge(d));
            });
            manager.flush();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void delete(List<SoldierData> data) {
        this.delete(new HashSet<>(data));
    }
}
