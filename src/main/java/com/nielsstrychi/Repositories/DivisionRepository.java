package com.nielsstrychi.Repositories;

import com.nielsstrychi.Data.Division;
import com.nielsstrychi.Data.SoldierData;
import com.nielsstrychi.Tiers;
import com.nielsstrychi.Providers.PersistenceProvider;
import org.bukkit.entity.Player;

import javax.persistence.EntityManager;
import java.util.*;

public class DivisionRepository implements Repository<Division> {

    private Map<SQL, Set<Division>> BATCH = new HashMap<>();

    @Override
    public void update(Division data) {
        try(PersistenceProvider provider = PersistenceProvider.provide()) {
            provider.beginSession().merge(data);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void update(Set<Division> data) {
        try(PersistenceProvider provider = PersistenceProvider.provide()) {
            EntityManager session = provider.beginSession();
            data.forEach(session::merge);
            session.flush();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void update(List<Division> data) {
        this.update(new HashSet<>(data));
    }

    @Override
    public void save(Division data) {
        try(PersistenceProvider provider = PersistenceProvider.provide()) {
            provider.beginSession().persist(data);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void save(Set<Division> data) {
        try(PersistenceProvider provider = PersistenceProvider.provide()) {
            EntityManager session = provider.beginSession();
            data.forEach(session::persist);
            session.flush();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void save(List<Division> data) {
        this.save(new HashSet<>(data));
    }

    @Override
    public Optional<Division> findByUUID(UUID uuid) {
        return Optional.empty();
    }


    public Optional<Division> findByPlayerTierOrName(Player player, String s) {
        return Optional.empty();
    }

    @Override
    public List<Division> findAll() {
        return null;
    }

    @Override
    public void batch(Division data, SQL action) {
        this.retrieveBatch(action).add(data);
    }

    @Override
    public void batch(List<Division> data, SQL action) {
        this.retrieveBatch(action).addAll(data);
    }

    @Override
    public Set<Division> retrieveBatch(SQL key) {
        return this.BATCH.computeIfAbsent(key, set -> new HashSet<>());
    }

    @Override
    public void clearBatch() {
        this.BATCH.clear();
    }

    @Override
    public void executeBatch(SQL... key) {
        List<SQL> keys = Arrays.asList(key);
        if (keys.contains(SQL.SAVE)) this.save(new HashSet<>(this.retrieveBatch(SQL.SAVE)));
        if (keys.contains(SQL.UPDATE)) this.update(new HashSet<>(this.retrieveBatch(SQL.UPDATE)));
        if (keys.contains(SQL.DELETE)) this.delete(new HashSet<>(this.retrieveBatch(SQL.DELETE)));
        clearBatch();
    }

    public List<Division> findAllPlayerDivisions(Player player) {
        return findAllPlayerDivisions(player.getUniqueId());
    }

    public List<Division> findAllPlayerDivisions(UUID player) {
        try (PersistenceProvider provider = PersistenceProvider.provide()) {
            return provider.beginSession()
                    .createNamedQuery(Division.FIND_ALL_FROM_PLAYER_UUID, Division.class)
                    .setParameter("player", player)
                    .getResultList();
        } catch (Exception e) {
            throw new IllegalArgumentException(e);
        }
    }

    public List<Tiers> findPlayerTiers(Player player) {
        return this.findPlayerTiers(player.getUniqueId());
    }

    public List<Tiers> findPlayerTiers(UUID player) {
        try(PersistenceProvider provider = PersistenceProvider.provide()) {
            return provider.beginSession()
                    .createNamedQuery(Division.FIND_ALL_LEGIONAMES_FROM_PLAYER_UUID, Tiers.class)
                    .setParameter("player", player)
                    .getResultList();
        } catch (Exception e) {
            throw new IllegalArgumentException(e);
        }
    }

    public void delete(Division data) {
        try(PersistenceProvider provider = PersistenceProvider.provide()) {
            EntityManager manager = provider.beginSession();
            manager.remove(manager.contains(data)
                    ? data
                    : manager.merge(data));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void delete(Set<Division> data) {
        try(PersistenceProvider provider = PersistenceProvider.provide()) {
            EntityManager manager = provider.beginSession();
            data.forEach(d -> {
                manager.remove(manager.contains(d)
                        ? d
                        : manager.merge(d));
            });
            manager.flush();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void delete(List<Division> data) {
        this.delete(new HashSet<>(data));
    }
}
