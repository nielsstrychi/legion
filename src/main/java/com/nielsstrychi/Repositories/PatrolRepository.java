package com.nielsstrychi.Repositories;

import com.nielsstrychi.Data.Data;

import java.util.*;

public class PatrolRepository implements Repository{
    @Override
    public void update(Data data) {

    }

    @Override
    public void update(List data) {

    }

    @Override
    public void update(Set data) {

    }

    @Override
    public void save(Data data) {

    }

    @Override
    public void save(List data) {

    }

    @Override
    public void save(Set data) {

    }

    @Override
    public Optional findByUUID(UUID uuid) {
        return Optional.empty();
    }

    @Override
    public List findAll() {
        return null;
    }

    @Override
    public void batch(Data data, SQL action) {

    }

    @Override
    public void batch(List data, SQL action) {

    }

    @Override
    public Set retrieveBatch(SQL key) {
        return null;
    }

    @Override
    public void clearBatch() {

    }

    @Override
    public void executeBatch(SQL... key) {

    }
}
