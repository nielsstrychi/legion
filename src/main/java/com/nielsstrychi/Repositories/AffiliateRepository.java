package com.nielsstrychi.Repositories;

import com.nielsstrychi.Data.Affiliate;
import com.nielsstrychi.Data.Data;
import com.nielsstrychi.Providers.PersistenceProvider;
import org.bukkit.entity.Player;

import javax.persistence.EntityManager;
import java.util.*;

public class AffiliateRepository implements Repository<Affiliate>{

    @Override
    public void update(Affiliate data) {
        try(PersistenceProvider provider = PersistenceProvider.provide()) {
            provider.beginSession().merge(data);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void update(Set<Affiliate> data) {
        try(PersistenceProvider provider = PersistenceProvider.provide()) {
            EntityManager session = provider.beginSession();
            data.forEach(session::merge);
            session.flush();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void update(List<Affiliate> data) {
        this.update(new HashSet<>(data));
    }

    @Override
    public void save(Affiliate data) {
        try(PersistenceProvider provider = PersistenceProvider.provide()) {
            provider.beginSession().persist(data);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void save(Set<Affiliate> data) {
        try(PersistenceProvider provider = PersistenceProvider.provide()) {
            EntityManager session = provider.beginSession();
            data.forEach(session::persist);
            session.flush();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void save(List<Affiliate> data) {
        this.save(new HashSet<>(data));
    }

    @Override
    public Optional<Affiliate> findByUUID(UUID uuid) {
        return Optional.empty();
    }

    @Override
    public List<Affiliate> findAll() {
        return null;
    }

    @Override
    public void batch(Affiliate data, SQL action) {

    }

    @Override
    public void batch(List<Affiliate> data, SQL action) {

    }

    @Override
    public Set<Affiliate> retrieveBatch(SQL key) {
        return null;
    }

    @Override
    public void clearBatch() {

    }

    @Override
    public void executeBatch(SQL... key) {
    }

    public List<Affiliate> findPlayerAffiliates(UUID player) {
        try(PersistenceProvider provider = PersistenceProvider.provide()) {
            return provider.beginSession()
                    .createNamedQuery(Affiliate.FIND_PLAYER_AFFILIATES, Affiliate.class)
                    .setParameter("player", player)
                    .getResultList();
        } catch (Exception e) {
            throw new IllegalArgumentException(e);
        }
    }

    public List<Affiliate> findPlayerAffiliates(Player player) {
        return this.findPlayerAffiliates(player.getUniqueId());
    }

    public void delete(Affiliate instance) {
        try(PersistenceProvider provider = PersistenceProvider.provide()) {
            EntityManager manager = provider.beginSession();
            manager.remove(manager.contains(instance)
                    ? instance
                    : manager.merge(instance));
        } catch (Exception e) {
            e.printStackTrace();
        }
}

    public Optional<Affiliate> findByAffiliates(UUID playerOne, UUID playerTwo) {
        try(PersistenceProvider provider = PersistenceProvider.provide()) {
            List<Affiliate> result = provider.beginSession()
                    .createNamedQuery(Affiliate.FIND_AFFILIATE, Affiliate.class)
                    .setParameter("playerOne", playerOne)
                    .setParameter("playerTwo", playerTwo)
                    .getResultList();
            return !result.isEmpty()
                    ? Optional.of(result.get(0))
                    : Optional.empty();
        } catch (Exception e) {
            throw new IllegalArgumentException(e);
        }
    }
    }
