package com.nielsstrychi;

import com.nielsstrychi.Listeners.BlockListener;
import com.nielsstrychi.Listeners.PlayerListener;
import com.nielsstrychi.Providers.*;
import org.bukkit.Bukkit;
import org.bukkit.event.Listener;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.plugin.PluginDescriptionFile;
import org.bukkit.plugin.PluginManager;


public class LegionPlugin extends JavaPlugin implements Listener {

    public static LegionPlugin getInstance() {
        Plugin plugin = Bukkit.getServer().getPluginManager().getPlugin(LegionPlugin.class.getName());
        if ((plugin instanceof LegionPlugin)) {
            return ((LegionPlugin) plugin);
        } else {
            throw new RuntimeException(String.format("Plugin instance %s not found. plugin disabled?",
                    LegionPlugin.class.getSimpleName()));
        }
    }

    @Override
    public void onLoad() {
        System.out.println("onLoad");
        super.onLoad();
        ContextProvider.start();
    }

    @Override
    public void onEnable() throws PluginEnableException {
        System.out.println("onEnable");
        super.onEnable();

        PluginDescriptionFile pdfFile = this.getDescription(); //TODO make nms dynamic?
        getLogger().info( pdfFile.getName() + " version " + pdfFile.getVersion() + " is enabled!" );

        ContextProvider.init(this);
    }

    @Override
    public void onDisable() {
        super.onDisable();
        ContextProvider.close();
    }
}


