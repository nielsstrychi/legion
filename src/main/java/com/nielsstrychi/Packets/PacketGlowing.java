package com.nielsstrychi.Packets;

import com.google.common.collect.Maps;
import com.nielsstrychi.Entities.Soldiers.SoldierSkeletonWither;
import com.nielsstrychi.Providers.TeamsProvider;
import net.minecraft.server.v1_13_R2.*;
import org.apache.commons.lang3.reflect.FieldUtils;
import org.bukkit.Bukkit;
import org.bukkit.craftbukkit.v1_13_R2.entity.CraftLivingEntity;
import org.bukkit.craftbukkit.v1_13_R2.entity.CraftPlayer;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class PacketGlowing {

    private void setEntityGlowColor(Player playerreciever, List<String> entityUUID, String teamname) {
        net.minecraft.server.v1_13_R2.Scoreboard nmsScoreboard = new net.minecraft.server.v1_13_R2.Scoreboard();
        ScoreboardTeam nmsTeam = new ScoreboardTeam(nmsScoreboard, teamname);
        PacketPlayOutScoreboardTeam packet = new PacketPlayOutScoreboardTeam(nmsTeam, entityUUID, 3);
        //playerreciever.setScoreboard(TeamsProvider.scoreboard);
        CraftPlayer craftPlayer = (CraftPlayer) playerreciever;
        craftPlayer.getHandle().playerConnection.sendPacket(packet);
    }

    public void setGlowing(Entity glowingEntity, Player sendPacketPlayer, boolean glow) {
        this.setGlowing(glowingEntity,sendPacketPlayer, glow, null);
    }

    @SuppressWarnings("unchecked")
    public void setGlowing(Entity glowingEntity, Player sendPacketPlayer,final boolean glow, GlowColor color) {
        if (glowingEntity != null) {
            try {
                EntityLiving entity = ((CraftLivingEntity) glowingEntity).getHandle();

                DataWatcher toCloneDataWatcher = entity.getDataWatcher();
                DataWatcher newDataWatcher = new DataWatcher(entity);

                // The map that stores the DataWatcherItems is private within the DataWatcher Object.
                // Use Reflection to access it from Apache Commons and change it.
                Map<Integer, DataWatcher.Item<?>> currentMap = (Map<Integer, DataWatcher.Item<?>>) FieldUtils.readDeclaredField(toCloneDataWatcher, "d", true);
                Map<Integer, DataWatcher.Item<?>> newMap = Maps.newHashMap();

                // Clone the DataWatcher.Items because we don't want to point to those values anymore.
                for (Integer integer : currentMap.keySet()) {
                    newMap.put(integer, currentMap.get(integer).d()); // Puts a copy of the DataWatcher.Item in newMap
                }

                // Get the 0th index for the BitMask value. http://wiki.vg/Entities#Entity
                DataWatcher.Item item = newMap.get(0);
                byte initialBitMask = (Byte) item.b(); // Gets the initial bitmask/byte value so we don't overwrite anything.
                byte bitMaskIndex = (byte) 6; // The index as specified in wiki.vg/Entities
                if (glow) {
                    item.a((byte) (initialBitMask | 1 << bitMaskIndex));
           /*     if (entity instanceof SoldierSkeletonWither &&!((SoldierSkeletonWither) entity).LIST_OF_GLOW_RECIEVERS.contains(sendPacketPlayer)) {
                ((SoldierSkeletonWither) entity).LIST_OF_GLOW_RECIEVERS.add(sendPacketPlayer);}*/

                    if (color != null) {
                        this.setEntityGlowColor(sendPacketPlayer, Arrays.asList(glowingEntity.getUniqueId().toString()), color.name());
                    }

                } else {
                    item.a((byte) (initialBitMask & ~(1 << bitMaskIndex))); // Inverts the specified bit from the index.
                    if (entity instanceof SoldierSkeletonWither) {
                        //      Bukkit.broadcastMessage("" + ((SoldierSkeletonWither) entity).LIST_OF_GLOW_RECIEVERS.remove(sendPacketPlayer));
                    }

                }

                // Set the newDataWatcher's (unlinked) map data
                FieldUtils.writeDeclaredField(newDataWatcher, "d", newMap, true);

                PacketPlayOutEntityMetadata metadataPacket = new PacketPlayOutEntityMetadata(glowingEntity.getEntityId(), newDataWatcher, true);

                ((CraftPlayer) sendPacketPlayer).getHandle().playerConnection.sendPacket(metadataPacket);

            } catch (IllegalAccessException e) { // Catch statement necessary for FieldUtils.readDeclaredField()
                e.printStackTrace();
            }
        }
    }
}
