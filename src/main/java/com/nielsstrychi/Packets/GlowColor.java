package com.nielsstrychi.Packets;

import org.bukkit.ChatColor;

public enum GlowColor {

    PLAYER_LEGIO(ChatColor.DARK_AQUA),
    PLAYER_LEGIO_SLC(ChatColor.AQUA),
    ALLY_LEGIO(ChatColor.BLUE),
    ENEMY_LEGIO(ChatColor.RED),
    ENEMY_LEGIO_ATK(ChatColor.DARK_RED),
    NEUTRAL_LEGIO(ChatColor.BLACK),
    HOLOGRAM_LEGIO(ChatColor.GREEN),
    HIT_LEGIO(ChatColor.WHITE);

    private ChatColor color;

    GlowColor(ChatColor color) {
        this.color = color;
    }

    public boolean equalsName(String otherName) {
        return this.name().equals(otherName);
    }

    public ChatColor getColor() {
        return color;
    }
}
