package com.nielsstrychi.Controllers;

import com.nielsstrychi.Entities.Holograms.Hologram;
import com.nielsstrychi.Entities.Soldiers.Soldier;
import com.nielsstrychi.Entities.Soldiers.SoldierSkeletonWither;
import com.nielsstrychi.Data.Division;
import org.bukkit.Location;

import java.util.ArrayList;
import java.util.List;

public class HologramController {
    List<Hologram> hologramsList = new ArrayList<>();
    Division division;
    Location location;

    public HologramController(Division division, Location location) {
        this.division = division;
        this.location= location;
    }

    private void spawnHolograms() {
        division.getDivisionList().forEach(this::createHologram);
    }

    private void createHologram(Soldier soldier) {
       /* Hologram hologram = (Hologram)((CraftEntity) EntitySpawnController.spawnEntity(Legion.getInstance().ENTITYSPAWNER.HOLOGRAM, locationCalculation())).getHandle();
        hologramsList.add(hologram);
        hologram.setEquipment(
                soldier.getEquipment(EnumItemSlot.HEAD),
                soldier.getEquipment(EnumItemSlot.CHEST),
                soldier.getEquipment(EnumItemSlot.LEGS),
                soldier.getEquipment(EnumItemSlot.FEET),
                soldier.getEquipment(EnumItemSlot.MAINHAND),
                soldier.getEquipment(EnumItemSlot.OFFHAND));*/
    }

    private Location locationCalculation() {
        return location;
    }

    private void removeHolograms() {
        hologramsList.forEach(s -> s.getBukkitEntity().remove());
    }


}
