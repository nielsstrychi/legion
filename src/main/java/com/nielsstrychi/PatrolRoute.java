package com.nielsstrychi;


import com.nielsstrychi.Data.Data;
import com.nielsstrychi.Data.PatrolPoint;
import com.nielsstrychi.Data.SoldierData;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "ROUTES")
public class PatrolRoute extends Data {

    @OneToMany(mappedBy = "route", fetch = FetchType.LAZY)
    private List<SoldierData> soldiers = new ArrayList<>();

    @OneToMany(mappedBy = "route", fetch = FetchType.EAGER)
    private List<PatrolPoint> patrolPoints = new ArrayList<>();

    public List<PatrolPoint> getPatrolPoints() {
        return patrolPoints;
    }

    public void setPatrolPoints(List<PatrolPoint> patrolPoints) {
        this.patrolPoints = patrolPoints;
    }

    public boolean addPatrolPoint(PatrolPoint patrolPoint) {
        if (patrolPoint!=null) {
            patrolPoint.setRoute(this);
            return patrolPoints.add(patrolPoint);
        } else throw new NullPointerException("Patrolpoint can not be null!");
    }

    public boolean deletePatrolPoint(PatrolPoint patrolPoint) {
        return patrolPoints.remove(patrolPoint);
    }
}
