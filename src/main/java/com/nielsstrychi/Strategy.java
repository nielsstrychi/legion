package com.nielsstrychi;

/**
 * Soldier strategy meant for archers.
 */
public enum Strategy {
    HOLD_SHOOT_HIGH, //TODO high and low necessary?
    HOLD_SHOOT_LOW,
    FOLLOW_SHOOT_HIGH,
    FOLLOW_SHOOT_LOW,
}
