package com.nielsstrychi.Util;

import net.minecraft.server.v1_13_R2.ItemArmor;
import net.minecraft.server.v1_13_R2.ItemBanner;

public class If {

    public static boolean isEquipable(org.bukkit.inventory.ItemStack item) {
        return Converter.convert(item).getItem() instanceof ItemArmor;
    }

    public static boolean isEquipable(net.minecraft.server.v1_13_R2.ItemStack item) {
        return item.getItem() instanceof ItemArmor;
    }

    public static boolean isBanner(org.bukkit.inventory.ItemStack item) {
        return Converter.convert(item).getItem() instanceof ItemBanner;
    }

    public static boolean isBanner(net.minecraft.server.v1_13_R2.ItemStack item) {
        return item.getItem() instanceof ItemBanner;
    }
}
