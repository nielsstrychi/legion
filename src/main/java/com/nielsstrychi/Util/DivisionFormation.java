package com.nielsstrychi.Util;

import java.util.Arrays;

public enum DivisionFormation {
    LINE_10x6(DivisionFormationPositions.LINE_10x6),
    DEFAULT(DivisionFormationPositions.LINE_10x6);

    DivisionPosition[] positions;

    DivisionFormation(DivisionPosition[] positions) {
        this.positions = positions;
    }

    public DivisionPosition[] getFormationPositions() {
        return this.positions;
    }
}
