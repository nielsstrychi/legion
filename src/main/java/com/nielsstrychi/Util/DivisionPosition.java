package com.nielsstrychi.Util;

public class DivisionPosition {
    private int id;
    private float x;
    private float z;
    private float angle;

    public DivisionPosition(int id, float x, float z) {
        this(id,x,z,0);
    }

    public DivisionPosition(int id, float x, float z, float angle) {
        this.id = id;
        this.x = x;
        this.z = z;
        this.angle = angle;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public float getX() {
        return x;
    }

    public void setX(float x) {
        this.x = x;
    }

    public float getZ() {
        return z;
    }

    public void setZ(float z) {
        this.z = z;
    }

    public float getAngle() {
        return angle;
    }

    public void setAngle(float angle) {
        this.angle = angle;
    }
}
