package com.nielsstrychi.Util;

/**
 * Bukkit objects to NMS objects converter.
 *
 * @Author Nielsstrychi
 */
public class Converter {

    /**
     * Converts a Bukkit ItemStack object to a NMS ItemStack object.
     *
     * @param item Bukkit ItemStack to be converted
     * @return NMS ItemStack object that got covnerted from a Bukkit ItemStack object.
     *         Returns null if the input is null.
     */
    public static net.minecraft.server.v1_13_R2.ItemStack convert(
            @Nullable final org.bukkit.inventory.ItemStack item) {
        return item != null
                ? org.bukkit.craftbukkit.v1_13_R2.inventory.CraftItemStack.asNMSCopy(item)
                : null;
    }

    /**
     * Converts a NMS ItemStack object to a Bukkit ItemStack object.
     *
     * @param item NMS ItemStack to be converted
     * @return Bukkit ItemStack object that got converted from a NMS ItemStack object.
     *         Returns null if the input is null.
     */
    public static org.bukkit.inventory.ItemStack convert(
            @Nullable final net.minecraft.server.v1_13_R2.ItemStack item) {
        return item != null
                ? org.bukkit.craftbukkit.v1_13_R2.inventory.CraftItemStack.asCraftMirror(item)
                : null;
    }

    /**
     * Converts a Bukkit LivingEntity object to a NMS EntityLiving object.
     *
     * @param nmsEntity NMS EntityLiving to be converted
     * @return converted Bukkit LivingEntity object from a NMS EntityLiving object.
     *         Returns null if the input is null.
     */
    public static net.minecraft.server.v1_13_R2.EntityLiving convert(
            @Nullable final org.bukkit.entity.LivingEntity nmsEntity) {
        return nmsEntity != null
                ? ((org.bukkit.craftbukkit.v1_13_R2.entity.CraftLivingEntity) nmsEntity).getHandle()
                : null;
    }


    /**
     * Converts a NMS LivingEntity object to a Bukkit EntityLiving object.
     *
     * @param bukkitEntity Bukkit EntityLiving to be converted
     * @return converted NMS LivingEntity object from a Bukkit EntityLiving object.
     *         Returns null if the input is null.
     */
    public static org.bukkit.entity.LivingEntity convert(
            @Nullable final net.minecraft.server.v1_13_R2.EntityLiving bukkitEntity) {
        return bukkitEntity != null
                ? (org.bukkit.entity.LivingEntity) bukkitEntity.getBukkitEntity()
                : null;
    }
}
