package com.nielsstrychi.Util;

public class DivisionFormationPositions {
   /* FLYING_WEDGE(new Float[]{
            F,F,F,F,F,F,F,F,F,F,
            F,F,F,F,F,F,F,F,F,F,
            F,F,F,F,F,F,F,F,F,F,
            F,F,F,F,F,F,F,F,F,F,
            F,F,F,F,F,F,F,F,F,F,
            F,F,F,F,F,F,F,F,F,F}),*/
    /*
    1                                *01
    2                             *02   *03
    3                          *04   *06   *05
    4                       *07   *09   *10   *08
    5                    *11   *13   *15   *14   *12
    6                 *16   *18   *20*  *21   *19   *17
    7              *22   *24   *26   *28   *27   *25   *23
    8           *29   *31   *33   *35   *36   *34   *32   *30
    9        *37   *39   *41   *43   *45   *44   *42   *40   *38
    10    *46   *48   *50   *52   *54   *55   *53   *51   *49   *47
    */
  /*  VEE_WEDGE(new Float[]{
            F,F,F,F,F,F,F,F,F,F,
            F,F,F,F,F,F,F,F,F,F,
            F,F,F,F,F,F,F,F,F,F,
            F,F,F,F,F,F,F,F,F,F,
            F,F,F,F,F,F,F,F,F,F,
            F,F,F,F,F,F,F,F,F,F}),*/

    /*
    4       *59   *57                                                                                 *58   *60
    4          *55   *53                                                                           *54   *56
    4             *51   *49                                                                     *50   *52
    4                *47   *45                                                               *46   *48
    4                   *43   *41                                                         *42   *44
    4                      *39   *37                                                   *38   *40
    4                         *35   *33                                             *34   *36
    4                            *31   *29                                       *30   *32
    4                               *27   *25                                 *26   *28
    4                                  *23   *21                           *22   *24
    4                                     *19   *17                     *18   *20
    4                                        *15   *13               *14   *16
    4                                           *11   *09         *10   *12
    4                                              *07   *01   *02   *08
    3                                                 *05  *03*04 *06
    */


 /*   DIAMOND(new Float[]{
            F,F,F,F,F,F,F,F,F,F,
            F,F,F,F,F,F,F,F,F,F,
            F,F,F,F,F,F,F,F,F,F,
            F,F,F,F,F,F,F,F,F,F,
            F,F,F,F,F,F,F,F,F,F,
            F,F,F,F,F,F,F,F,F,F}),*/
    /*

    */
 /*   MARCH_2x30(new Float[]{
            F,F,F,F,F,F,F,F,F,F,
            F,F,F,F,F,F,F,F,F,F,
            F,F,F,F,F,F,F,F,F,F,
            F,F,F,F,F,F,F,F,F,F,
            F,F,F,F,F,F,F,F,F,F,
            F,F,F,F,F,F,F,F,F,F}),
    MARCH_3x20(new Float[]{
            F,F,F,F,F,F,F,F,F,F,
            F,F,F,F,F,F,F,F,F,F,
            F,F,F,F,F,F,F,F,F,F,
            F,F,F,F,F,F,F,F,F,F,
            F,F,F,F,F,F,F,F,F,F,
            F,F,F,F,F,F,F,F,F,F}),
    MARCH_4x15(new Float[]{
            F,F,F,F,F,F,F,F,F,F,
            F,F,F,F,F,F,F,F,F,F,
            F,F,F,F,F,F,F,F,F,F,
            F,F,F,F,F,F,F,F,F,F,
            F,F,F,F,F,F,F,F,F,F,
            F,F,F,F,F,F,F,F,F,F}),
    COLUM_6x10(new Float[]{
            F,F,F,F,F,F,F,F,F,F,
            F,F,F,F,F,F,F,F,F,F,
            F,F,F,F,F,F,F,F,F,F,
            F,F,F,F,F,F,F,F,F,F,
            F,F,F,F,F,F,F,F,F,F,
            F,F,F,F,F,F,F,F,F,F}),
    COLUM_8x8(new Float[]{
            F,F,F,F,F,F,F,F,F,F,
            F,F,F,F,F,F,F,F,F,F,
            F,F,F,F,F,F,F,F,F,F,
            F,F,F,F,F,F,F,F,F,F,
            F,F,F,F,F,F,F,F,F,F,
            F,F,F,F,F,F,F,F,F,F}),

            /*
            10      *46 *37 *21 *08 *03 *01 *02 *07 *22 *36
            10      *47 *39 *23 *10 *06 *04 *05 *09 *24 *38
            10      *48 *41 *25 *15 *13 *11 *12 *14 *26 *40
            10      *49 *43 *27 *19 *17 *16 *18 *20 *28 *42
            10      *50 *45 *35 *33 *31 *29 *30 *32 *34 *44
            10      *59 *57 *55 *53 *51 *52 *54 *56 *58 *60
             */

 /*
                   (+)
                    |
           X    (+)---(-)
                    |
                   (-)

                    Z
  */

    final static DivisionPosition[] LINE_10x6 = {
           new DivisionPosition(1, 0.0F, 0.0F),
           new DivisionPosition(2, -1.0F, 0.0F),
           new DivisionPosition(3, 1.0F, 0.0F),
           new DivisionPosition(4, 0.0F, -1.0F),
           new DivisionPosition(5, -1.0F, -1.0F),
           new DivisionPosition(6, 1.0F, -1.0F),
           new DivisionPosition(7, -2.0F, 0.0F),
           new DivisionPosition(8, 2.0F, 0.0F),
           new DivisionPosition(9, -2.0F, -1.0F),
           new DivisionPosition(10, 2.0F, -1.0F),
           new DivisionPosition(11, 0.0F, -2.0F),
           new DivisionPosition(12, -1.0F, -2.0F),
           new DivisionPosition(13, 1.0F, -2.0F),
           new DivisionPosition(14, -2.0F, -2.0F),
           new DivisionPosition(15, 2.0F, -2.0F),
           new DivisionPosition(16, 0.0F, -3.0F),
           new DivisionPosition(17, 1.0F, -3.0F),
           new DivisionPosition(18, -1.0F, -3.0F),
           new DivisionPosition(19, 2.0F, -3.0F),
           new DivisionPosition(20, -2.0F, -3.0F),
           new DivisionPosition(21, 3.0F, 0.0F),
           new DivisionPosition(22, -3.0F, 0.0F),
           new DivisionPosition(23, 3.0F, -1.0F),
           new DivisionPosition(24, -3.0F, -1.0F),
           new DivisionPosition(25, 3.0F, -2.0F),
           new DivisionPosition(26, -3.0F, -2.0F),
           new DivisionPosition(27, 3.0F, -3.0F),
           new DivisionPosition(28, -3.0F, -3.0F),
           new DivisionPosition(29, 0.0F, -4.0F),
           new DivisionPosition(30, -1.0F, -4.0F),
           new DivisionPosition(31, 1.0F, -4.0F),
           new DivisionPosition(32, -2.0F, -4.0F),
           new DivisionPosition(33, 2.0F, -4.0F),
           new DivisionPosition(34, -3.0F, -4.0F),
           new DivisionPosition(35, 3.0F, -4.0F),
           new DivisionPosition(36, -4.0F, 0.0F),
           new DivisionPosition(37, 4.0F, 0.0F),
           new DivisionPosition(38, -4.0F, -1.0F),
           new DivisionPosition(39, 4.0F, -1.0F),
           new DivisionPosition(40, -4.0F, -2.0F),
           new DivisionPosition(41, 4.0F, -2.0F),
           new DivisionPosition(42, -4.0F, -3.0F),
           new DivisionPosition(43, 4.0F, -3.0F),
           new DivisionPosition(44, -4.0F, -4.0F),
           new DivisionPosition(45, 4.0F, -4.0F),
           new DivisionPosition(46, 5.0F, 0.0F),
           new DivisionPosition(47, 5.0F, -1.0F),
           new DivisionPosition(48, 5.0F, -2.0F),
           new DivisionPosition(49, 5.0F, -3.0F),
           new DivisionPosition(50, 5.0F, -4.0F),
           new DivisionPosition(51, 1.0F, -5.0F),
           new DivisionPosition(52, 0.0F, -5.0F),
           new DivisionPosition(53, 2.0F, -5.0F),
           new DivisionPosition(54, -1.0F, -5.0F),
           new DivisionPosition(55, 3.0F, -5.0F),
           new DivisionPosition(56, -2.0F, -5.0F),
           new DivisionPosition(57, 4.0F, -5.0F),
           new DivisionPosition(58, -3.0F, -5.0F),
           new DivisionPosition(59, 5.0F, -5.0F),
           new DivisionPosition(60, -4.0F, -5.0F)
   };

   /* public static FormationPosition[] LINE_15x4 = {
            new FormationPosition(1, .0F, .0F),
            new FormationPosition(2, .0F, .0F),
            new FormationPosition(3, .0F, .0F),
            new FormationPosition(4, .0F, .0F),
            new FormationPosition(5, .0F, .0F),
            new FormationPosition(6, .0F, .0F),
            new FormationPosition(7, .0F, .0F),
            new FormationPosition(8, .0F, .0F),
            new FormationPosition(9, .0F, .0F),
            new FormationPosition(10, .0F, .0F),
            new FormationPosition(11, .0F, .0F),
            new FormationPosition(12, .0F, .0F),
            new FormationPosition(13, .0F, .0F),
            new FormationPosition(14, .0F, .0F),
            new FormationPosition(15, .0F, .0F),
            new FormationPosition(16, .0F, .0F),
            new FormationPosition(17, .0F, .0F),
            new FormationPosition(18, .0F, .0F),
            new FormationPosition(19, .0F, .0F),
            new FormationPosition(20, .0F, .0F),
            new FormationPosition(21, .0F, .0F),
            new FormationPosition(22, .0F, .0F),
            new FormationPosition(23, .0F, .0F),
            new FormationPosition(24, .0F, .0F),
            new FormationPosition(25, .0F, .0F),
            new FormationPosition(26, .0F, .0F),
            new FormationPosition(27, .0F, .0F),
            new FormationPosition(28, .0F, .0F),
            new FormationPosition(29, .0F, .0F),
            new FormationPosition(30, .0F, .0F),
            new FormationPosition(31, .0F, .0F),
            new FormationPosition(32, .0F, .0F),
            new FormationPosition(33, .0F, .0F),
            new FormationPosition(34, .0F, .0F),
            new FormationPosition(35, .0F, .0F),
            new FormationPosition(36, .0F, .0F),
            new FormationPosition(37, .0F, .0F),
            new FormationPosition(38, .0F, .0F),
            new FormationPosition(39, .0F, .0F),
            new FormationPosition(40, .0F, .0F),
            new FormationPosition(41, .0F, .0F),
            new FormationPosition(42, .0F, .0F),
            new FormationPosition(43, .0F, .0F),
            new FormationPosition(44, .0F, .0F),
            new FormationPosition(45, .0F, .0F),
            new FormationPosition(46, .0F, .0F),
            new FormationPosition(47, .0F, .0F),
            new FormationPosition(48, .0F, .0F),
            new FormationPosition(49, .0F, .0F),
            new FormationPosition(50, .0F, .0F),
            new FormationPosition(51, .0F, .0F),
            new FormationPosition(52, .0F, .0F),
            new FormationPosition(53, .0F, .0F),
            new FormationPosition(54, .0F, .0F),
            new FormationPosition(55, .0F, .0F),
            new FormationPosition(56, .0F, .0F),
            new FormationPosition(57, .0F, .0F),
            new FormationPosition(58, .0F, .0F),
            new FormationPosition(59, .0F, .0F),
            new FormationPosition(60, .0F, .0F),
    };*/
    /*
    LINE_15x4(new Float[]{
            F,F,F,F,F,F,F,F,F,F,
            F,F,F,F,F,F,F,F,F,F,
            F,F,F,F,F,F,F,F,F,F,
            F,F,F,F,F,F,F,F,F,F,
            F,F,F,F,F,F,F,F,F,F,
            F,F,F,F,F,F,F,F,F,F}),
    LINE_20x3(new Float[]{
            F,F,F,F,F,F,F,F,F,F,
            F,F,F,F,F,F,F,F,F,F,
            F,F,F,F,F,F,F,F,F,F,
            F,F,F,F,F,F,F,F,F,F,
            F,F,F,F,F,F,F,F,F,F,
            F,F,F,F,F,F,F,F,F,F}),
    LINE_30x2(new Float[]{
            F,F,F,F,F,F,F,F,F,F,
            F,F,F,F,F,F,F,F,F,F,
            F,F,F,F,F,F,F,F,F,F,
            F,F,F,F,F,F,F,F,F,F,
            F,F,F,F,F,F,F,F,F,F,
            F,F,F,F,F,F,F,F,F,F});

    FromationData(Float[] floats) {
    }*/

    public final static DivisionPosition[] STANDARD = LINE_10x6;

    public static DivisionPosition calculatePositionAngle(DivisionPosition id, float angle) {
        float x = id.getX();
        float y = id.getZ();



        return new DivisionPosition(id.getId(),x,y,angle);
    }
}
