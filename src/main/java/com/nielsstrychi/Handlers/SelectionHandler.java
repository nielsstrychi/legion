package com.nielsstrychi.Handlers;

import com.nielsstrychi.Entities.Soldiers.Soldier;
import com.nielsstrychi.Data.Division;
import com.nielsstrychi.Selectors.DivisionSelector;
import com.nielsstrychi.Selectors.MultiSelector;
import com.nielsstrychi.Selectors.Selector;
import net.minecraft.server.v1_13_R2.EntityLiving;
import net.minecraft.server.v1_13_R2.Items;
import org.bukkit.GameMode;
import org.bukkit.craftbukkit.v1_13_R2.entity.CraftLivingEntity;
import org.bukkit.craftbukkit.v1_13_R2.inventory.CraftItemStack;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerInteractEntityEvent;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

/**
 * The handler is all about handling the flow of a PlayerInteractEntityEvent to see with entities are selected and then performs actions.
 *
 * @author Nielsstrychi
 */
public class SelectionHandler { //TODO return types of selectors cna be £Divison or mulit somethimes

    /**
     * Map that holds the player his current selection.
     */
    private final Map<String, Selector> SELECTOR_MAP = new HashMap<>();

    /**
     * Getter to return a player his selection if not null optional empty.
     * @param player to pull his selection form SELECTOR_MAP.
     * @return player his selection as optional.
     */
    public Optional<Selector> getSelector(final Player player) {
        Selector selector = this.SELECTOR_MAP.get(player.getDisplayName());
        return selector != null
                ? Optional.of(selector)
                : Optional.empty();
    }

    /**
     * Reuses getSelector(Player) but this one is for private use only.
     * @param event pulls the selector with the event that contains the player.
     * @return the event his player selector as optional.
     */
    private Optional<Selector> getSelector(final PlayerInteractEntityEvent event) {
        return this.getSelector(getPlayer(event));
    }

    //TODO remove and add more incapsulation
    @Deprecated
    public Map<String, Selector> getPlayerSelector() {
        return SELECTOR_MAP;
    }

    /* ---------------------------------------------- Selection Flow ------------------------------------------------- */

    /**
     * Starts the handling of a click event on an entity.
     * If a selection is already present (this means they have already a entity selected),
     * it retrieve the previous selection and handle it.
     * Else a new selection wil be created and handled.
     * When the selection is returned the selection actions wil be invoked.
     * @param event retrieved form the PlayerListener.
     *
     *              handle -> current/new
     *              actions <- current/new
     */
    public void handleSelectionFlow(final PlayerInteractEntityEvent event) {
        Optional<Selector> selector = getSelector(event);
        handleSelectionActionFlow(event,(selector.isPresent())
                ? handleCurrentSelectionFlow(event, selector.get())
                : handleNewSelection(event));
    }

    /**
     * Handles the selection again of the previous entity they clicked.
     * Splits the behavior if the player is sneaking or not.
     * @param event the new entity click event.
     * @param selector of previous selection is handled.
     * @return a selector that wil be handled actions on in handleSelectionActionFlow method.
     *
     *              handle -> current -> sneaking/not sneaking
     *              actions <- current <- sneaking/not sneaking
     */
    private Selector handleCurrentSelectionFlow(final PlayerInteractEntityEvent event,
                                                final Selector selector) {
        return (!isSneaking(event))
                ? handleCurrentSelectionAsSneakingIsDisabledFlow(event,selector)
                : handleCurrentSelectionAsSneakingIsEnabledFlow(event,selector);
    }

    /**
     * Player is sneaking and wil check if the new selected entity is a soldier.
     * Splits the behavior when the new selected entity is a soldier or not.
     * @param event that contains the player and the new selected entity.
     * @param selector that handles previous selected entity.
     * @return a selector that wil be propagate trough handleCurrentSelectionFlow to the handleSelectionActionFlow method.
     *
     *              handle -> current -> sneaking -> soldier/not soldier
     *              actions <- current <- sneaking <- soldier/not soldier
     */
    private Selector handleCurrentSelectionAsSneakingIsEnabledFlow(final PlayerInteractEntityEvent event,
                                                                   final Selector selector) {
        return isEntityInstanceOfSoldier(event)
                ? handleCurrentSelectionAsSneakingIsEnabledAndSelectionIsInstanceOfSoldierFlow(event, selector)
                : handleCurrentSelectionAsSneakingIsEnabledAndSelectionIsNotInstanceOfSoldierFlow(event, selector);
    }

    /**
     * Selected a entity that is a soldier check if we selected a new soldier or the same soldier as previous.
     * Splits the behavior when its a new selected soldier or the same as previous selection.
     * @param event that contains the new selected soldier.
     * @param selector that holds the previous selection.
     * @return a selector that wil be propagate trough to all methods back to the handleSelectionActionFlow method.
     *
     *              handle -> current -> sneaking -> soldier -> new/same
     *              actions <- current <- sneaking <- soldier <- new/same
     */
    private Selector handleCurrentSelectionAsSneakingIsEnabledAndSelectionIsInstanceOfSoldierFlow(final PlayerInteractEntityEvent event,
                                                                                                  final Selector selector) {

        if (!isOwner(event)) {
                if (AffiliateHandler.handle().friendlies(getPlayer(event), getClickedEntityAsSoldier(event))) {
                    AffiliateHandler.handle().glowFriendlyTroops(getPlayer(event), getClickedEntityAsSoldier(event));
                } else {
                    TargetHandler.handle().glowTargetTroops(getPlayer(event), getClickedEntityAsSoldier(event));
                }
            return selector; //TODO not owner not allowed to control blocked here
        }

        return isSoldierSameAsClickedSoldier(event,selector)
                ? handleSameSoldierSelectedAsSneakingIsEnabledFlow(event, selector)
                : handleNewSoldierSelectedAsSneakingIsEnabledFlow(event, selector);
    }

    /**
     *
     * @param event
     * @param selector
     * @return a selector that wil be propagate trough to all methods back to the handleSelectionActionFlow method.
     *
     *              handle -> current -> sneaking -> not soldier ->
     *              actions <- current <- sneaking <- not soldier <-
     */
    private Selector handleCurrentSelectionAsSneakingIsEnabledAndSelectionIsNotInstanceOfSoldierFlow(final PlayerInteractEntityEvent event,
                                                                                                     final Selector selector) {
        return selector;  //TODO if needed java doc and impl
    }

    /**
     * A new soldier is selected, Checks if the previous selection is a MultiSelector.
     * Splits the behavior to MultiSelector logic or (Single)Selector logic.
     * @param event that holds the new selected soldier.
     * @param selector that is a Selector or MultiSelector with the previous entity or entities.
     * @return a selector that wil be propagate trough to all methods back to the handleSelectionActionFlow method.
     *
     *              handle -> current -> sneaking -> soldier -> new -> single/multi selector
     *              actions <- current <- sneaking <- soldier <- new <- single/multi selector
     */
    private Selector handleNewSoldierSelectedAsSneakingIsEnabledFlow(final PlayerInteractEntityEvent event,
                                                                     final Selector selector) {
        return (selector instanceof MultiSelector)
                ? handleNewSoldierSelectedAsSneakingIsEnabledAsMultiSelectionFlow(event, (MultiSelector) selector)
                : handleNewSoldierSelectedAsSneakingIsEnabledAsSingleSelectorFlow(event, selector);
    }

    /**
     * Previous selector contains one Soldier.
     * Checks if the new clicked Soldier is part of a Division.
     * Then splits the behavior to add the previous selected soldier to the new soldier his division,
     * or else a new  MultiSelector wil be made that holds the new and previous selected soldiers.
     * @param event holds the new selected soldier.
     * @param selector holds the previous selected soldier.
     * @return a MultiSelector or a DivisionSelector that wil propagate back to the handleSelectionActionFlow method.
     *
     *              handle -> current -> sneaking -> soldier -> new -> single -> new division/multi selector
     *              actions <- current <- sneaking <- soldier <- new <- single <- new division/multi selector
     */
    private Selector handleNewSoldierSelectedAsSneakingIsEnabledAsSingleSelectorFlow(final PlayerInteractEntityEvent event,
                                                                                     final Selector selector) {
        return (getClickedEntityAsSoldier(event).getDivision().isPresent())
                ? handleNewSoldierSelectedAsSneakingIsEnabledNewDivisionSelectionFlow(event, selector)
                : handleNewSoldierSelectedAsSneakingIsEnabledNewMultiSelectionFlow(event, selector);

    }

    /**
     * The previous selection holds more then one Soldier.
     * Splits the behavior if the selector is a MultiSelector or a DivisionSelector.
     * @param event holds the new Soldier that is gone be added to the previous selector.
     * @param selector MultiSelector or DivisionSelector form previous selection.
     * @return current MultiSelector or DivisionSelector that wil propagate back to the handleSelectionActionFlow method.
     *
     *              handle -> current -> sneaking -> soldier -> new -> multi -> current division/multi selector
     *              actions <- current <- sneaking <- soldier <- new <- multi <- current division/multi selector
     */
    private Selector handleNewSoldierSelectedAsSneakingIsEnabledAsMultiSelectionFlow(final PlayerInteractEntityEvent event,
                                                                                     final MultiSelector selector) {
        return (selector instanceof DivisionSelector)
                ? handleNewSoldierSelectedAsSneakingIsEnabledCurrentDivisionSelectionFlow(event, selector)
                : handleNewSoldierSelectedAsSneakingIsEnabledCurrentMultiSelectionFlow(event, selector);
    }

    /**
     * The endpoint of the flow here the new Soldier wil be added to the selector division if the conditions are valid.
     * @param event that contains the new soldier that is gone be added to the selector.
     * @param selector DivisionSelector where the new soldier wil be added to.
     * @return the previous selector (DivisionSelector) gets reused.
     *
     *              handle -> current -> sneaking -> soldier -> new -> multi -> current division selector
     *              actions <- current <- sneaking <- soldier <- new <- multi <- current division selector
     */
    private DivisionSelector handleNewSoldierSelectedAsSneakingIsEnabledCurrentDivisionSelectionFlow(final PlayerInteractEntityEvent event,
                                                                                                     final Selector selector) {
        checkFormation(event, selector); //TODO rewrite beter impl
        return (DivisionSelector) selector;
//        return newFormationSelector(event, newSelector(event), getClickedEntityAsSoldier(event).getDivision()
//                .orElse(selector.getSoldier().getPresentDivision()));
        //TODO trigger if division is serlected and sneak select a single soldier or multiselection
        //TODO apporptiate handleing = adding to formation
    }

    /**
     * A new DivisionSelector wil replace the previous selection.
     * The new DivisionSelector wil be the division of the new soldier retrieved from event.
     * The previous selected soldier wil be added to the Division if conditions are valid.
     * @param event that contains the new soldier that is member of a division.
     * @param selector his selected soldier wil be added to the new DivisionSelector that replaced the previous selector.
     * @return a new DivisionSelector that wil replace the previous selector.
     *
     *              handle -> current -> sneaking -> soldier -> new -> single -> new division selector
     *              actions <- current <- sneaking <- soldier <- new <- single <- new division selector
     */
    private DivisionSelector handleNewSoldierSelectedAsSneakingIsEnabledNewDivisionSelectionFlow(final PlayerInteractEntityEvent event,
                                                                                                 final Selector selector) {
        checkFormation(event, selector); //TODO beter impl
        return newFormationSelector(event, newSelector(event), ((Division) getClickedEntityAsSoldier(event).getDivision()
                .orElse(selector.getSoldier().getPresentDivision())));
        //TODO trigger if division is serlected and sneak select a single soldier or multiselection
        //TODO apporptiate handleing = adding to formation
    }

    /**
     * Check if the new selected soldier not already exist in the current MultiSelector.
     * @param event holds the new selected soldier.
     * @param selector previous MultiSelection that wil be reused.
     * @return previous MultiSelection that gets reused and wil propagate back to the handleSelectionActionFlow method.
     *
     *              handle -> current -> sneaking -> soldier -> new -> multi -> current multi selector -> contains/not contains
     *              actions <- current <- sneaking <- soldier <- new <- multi <- current multi selector <- contains/not contains
     */
    private Selector handleNewSoldierSelectedAsSneakingIsEnabledCurrentMultiSelectionFlow(final PlayerInteractEntityEvent event,
                                                                                          final Selector selector) {
        return !selector.containsSoldier(getClickedEntityAsSoldier(event))
                ? handleNewSoldierSelectedAsSneakingIsEnabledCurrentMultiSelectionDoesNotContainsSoldierFlow(event,selector)
                : handleNewSoldierSelectedAsSneakingIsEnabledCurrentMultiSelectionAlreadyContainsSoldierFlow(event, selector);
    }

    /**
     * Adds the new selected solder retrieved form event to the current MultiSelection.
     * @param event holds the new selected soldier that is gone be added to the current selector.
     * @param selector a MultiSelection that wil retrieve a extra soldier if the conditions are valid.
     * @return the previous MultiSelection that wil be reused, and propagate back to the handleSelectionActionFlow method.
     *
     *              handle -> current -> sneaking -> soldier -> new -> multi -> current multi selector -> not contains
     *              actions <- current <- sneaking <- soldier <- new <- multi <- current multi selector <- not contains
     */
    private Selector handleNewSoldierSelectedAsSneakingIsEnabledCurrentMultiSelectionDoesNotContainsSoldierFlow(final PlayerInteractEntityEvent event,
                                                                                                                final Selector selector) {
        ((MultiSelector) selector).addSoldierMulti(getClickedEntityAsSoldier(event));
        CheckFormation(event, selector);
        return selector;
    }

    /**
     *
     * @param event
     * @param selector
     * @return
     *
     *              handle -> current -> sneaking -> soldier -> new -> multi -> current multi selector -> not contains
     *              actions <- current <- sneaking <- soldier <- new <- multi <- current multi selector <- not contains
     */
    private Selector handleNewSoldierSelectedAsSneakingIsEnabledCurrentMultiSelectionAlreadyContainsSoldierFlow(final PlayerInteractEntityEvent event,
                                                                                                                final Selector selector) {
        //TODO impl if needed
        return selector;
    }

    /**
     * Replaces the previous single selection to a new MultiSelection with the previous and new selected soldiers.
     * @param event contains the new selected soldier that is not part of a division.
     * @param selector contains the previous single selected soldier.
     * @return a new MultiSelector that contains the new and previous selected soldiers.
     *
     *              handle -> current -> sneaking -> soldier -> new -> single -> new multi selector
     *              actions <- current <- sneaking <- soldier <- new <- single <- new multi selector
     */
    private Selector handleNewSoldierSelectedAsSneakingIsEnabledNewMultiSelectionFlow(final PlayerInteractEntityEvent event,
                                                                                      final Selector selector) {
        checkFormation(event, selector); //TODO not needed?
        return addSelector(new MultiSelector(selector, getClickedEntityAsSoldier(event)),
                getPlayerListName(event));
    }

    /**
     * The same selected soldier is clicked twice.
     * Splits the behavior when the selection is a single selection or a MultiSelection.
     * @param event holds the selected soldier same as from previous selection.
     * @param selector the selection that is reselected.
     * @return the same single the same MultiSelector/DivisionSelector or a new single selector.
     *         This wil be defined in the other propagating methods.
     *
     *              handle -> current -> sneaking -> soldier -> same -> single/multi selector
     *              actions <- current <- sneaking <- soldier <- same <- single/multi selector
     */
    private Selector handleSameSoldierSelectedAsSneakingIsEnabledFlow(final PlayerInteractEntityEvent event,
                                                                      final Selector selector) {
        return (selector instanceof MultiSelector)
                ? handleSameSoldierSelectedAsSneakingIsEnabledAsMultiSelectorFlow(event, (MultiSelector) selector)
                : handleSameSoldierSelectedAsSneakingIsEnabledAsSingleSelectorFlow(event, selector);
    }

    /**
     * The same single soldier that is selected is selected again.
     * A field to define it is selected for the second time is set.
     * @param event that holds the soldier that is selected now for the second time.
     * @param selector the selector that the is selected again.
     * @return reuses the same selector that wil have now differed behavior in the handleSelectionActionFlow method.
     *         This because it is selected for the second time.
     *
     *              handle -> current -> sneaking -> soldier -> same -> single selector
     *              actions <- current <- sneaking <- soldier <- same <- single selector
     */
    private Selector handleSameSoldierSelectedAsSneakingIsEnabledAsSingleSelectorFlow(final PlayerInteractEntityEvent event,
                                                                                      final Selector selector) {
        return  selector.setSelectedTwice(true);
    }

    /**
     * The selection holds more then one soldier, a MultiSelection or a DivisionSelection that is selected twice.
     * Splits the behavior when the selection is a MultiSelection or DivisionSelection.
     * @param event that holds the selected soldier that is part of a MultiSelection or DivisionSelection.
     * @param selector MultiSelection or DivisionSelection that is selected for the second time while sneaking.
     * @return alternative of the behavior of propagating methods a single selector or a MultiSelection or higher.
     *
     *              handle -> current -> sneaking -> soldier -> same -> multi -> multi/division selector
     *              actions <- current <- sneaking <- soldier <- same <- multi <- multi/division selector
     */
    private Selector handleSameSoldierSelectedAsSneakingIsEnabledAsMultiSelectorFlow(final PlayerInteractEntityEvent event,
                                                                                     final MultiSelector selector) {
        return (selector instanceof DivisionSelector)
                ? handleSameSoldierSelectedAsSneakingIsEnabledCurrentDivisionSelectorFlow(event, (DivisionSelector) selector)
                : handleSameSoldierSelectedAsSneakingIsEnabledCurrentMultiSelectorFlow(event, selector);
    }

    /**
     * Handle the selection of the soldier in event form the already selected MultiSelection.
     * The selected soldier wil be removed form the MultiSelection.
     * @param event contains the soldier that is selected while the player is sneaking.
     * @param selector the MultiSelection that is selected again.
     * @return the MultiSelection wil be reused if there are at least 2 soldiers remaining.
     *         Else a single selection that wil be selected twice with the last remaining soldier wil be returned.
     *
     *              handle -> current -> sneaking -> soldier -> same -> multi -> multi selector
     *              actions <- current <- sneaking <- soldier <- same <- multi <- multi selector
     */
    private Selector handleSameSoldierSelectedAsSneakingIsEnabledCurrentMultiSelectorFlow(final PlayerInteractEntityEvent event,
                                                                                          final MultiSelector selector) {
        selector.removeSoldierMulti(getClickedEntityAsSoldier(event));
        return (selector.isSingleSoldierList())
                ? addSelector(new Selector(event.getPlayer(),
                selector.getLastSoldier()),
                getPlayerListName(event))
                .setSelectedTwice(true)
                : selector;
    }

    /**
     *
     * @param event
     * @param selector
     * @return
     *
     *              handle -> current -> sneaking -> soldier -> same -> multi -> division selector
     *              actions <- current <- sneaking <- soldier <- same <- multi <- division selector
     */
    private Selector handleSameSoldierSelectedAsSneakingIsEnabledCurrentDivisionSelectorFlow(final PlayerInteractEntityEvent event,
                                                                                             final Selector selector) {
        //TODO what if a division is selected twice while sneaking single select? that is selected twice?
        return selector;
    }


    /**
     * Splits behavior when the new selected entity is a soldier.
     * @param event holds the new selected entity.
     * @param selector previous selection of one or more soldiers.
     * @return a empty selector or reuses the current selector.
     *
     *              handle -> current -> not sneaking -> soldier/not soldier
     *              actions <- current <- not sneaking <- soldier/not soldier
     */
    private Selector handleCurrentSelectionAsSneakingIsDisabledFlow(final PlayerInteractEntityEvent event,
                                                                    final Selector selector) {
        return isEntityInstanceOfSoldier(event)
                ? handleCurrentSelectionAsSneakingIsDisabledInstanceOfSoldierFlow(event, selector)
                : handleCurrentSelectionAsSneakingIsDisabledInstanceNotOfSoldierFlow(event, selector);
    }

    /**
     * Checks if the same soldier is selected as in the previous selection.
     * if the same soldier is selected twice without the player sneaking.
     * Then he wil be deselected else if it is a new soldier more conditions wil be applied.
     * @param event holds the last selected soldier.
     * @param selector holds the previous selected soldier.
     * @return a empty selection if soldier is selected twice without the player sneaking else a selector wil be returned.
     *
     *              handle -> current -> not sneaking -> soldier -> new/same
     *              actions <- current <- not sneaking <- soldier <- new/same
     */
    private Selector handleCurrentSelectionAsSneakingIsDisabledInstanceOfSoldierFlow(final PlayerInteractEntityEvent event,
                                                                                     final Selector selector) {

        if (!isOwner(event)) {
                if (AffiliateHandler.handle().friendlies(getPlayer(event), getClickedEntityAsSoldier(event))) {
                    AffiliateHandler.handle().glowFriendlyTroops(getPlayer(event), getClickedEntityAsSoldier(event));
                } else {
                    selector.toAttackEntity((LivingEntity) event.getRightClicked());
                }
            return selector; //TODO not owner not allowed to control blocked here
        }

        return (selector.getSoldier() != getClickedEntityAsSoldier(event))
                ? handleNewSoldierSelectedAsSneakingIsDisabledFlow(event, selector)
                : handleSameSoldierSelectedAsSneakingIsDisabledFlow(event, selector);
    }

    /**
     * This means the previous selection of soldiers wil target the entity that you selected.
     * @param event holds the selected entity that wil be targeted.
     * @param selector gets reused and wil be send back to the actions method.
     * @return the previous selection of soldiers.
     *
     *              handle -> current -> not sneaking -> not soldier
     *              actions <- current <- not sneaking <- not soldier
     */
    private Selector handleCurrentSelectionAsSneakingIsDisabledInstanceNotOfSoldierFlow(final PlayerInteractEntityEvent event,
                                                                                        final Selector selector) {
        selector.toAttackEntity((LivingEntity) event.getRightClicked());
        //new GlowingState(event.getPlayer(), (LivingEntity) event.getRightClicked(), GlowColor.ENEMY_LEGIO_ATK); //TODo replace logic
        return selector; //TODO test before this returned null;
    }


    /**
     *
     * @param event
     * @param selector
     * @return
     *
     *              handle -> current -> not sneaking -> soldier -> same
     *              actions <- current <- not sneaking <- soldier <- same
     */
    private Selector handleSameSoldierSelectedAsSneakingIsDisabledFlow(final PlayerInteractEntityEvent event,
                                                                       final Selector selector) {
        if (getClickedEntityAsSoldier(event).getDivision().isPresent()){
           // selector.startGlowingSelection(GlowColor.ALLY_LEGIO);
         }
        return removeSelector(selector, getPlayerListName(event));
    }

    /**
     * Checks if the current new selected soldier is part of a division.
     * if not the previous selection wil be deselected and a new selection with the new soldier wil be created.
     * @param event holds the new selected soldier.
     * @param selector previous selection.
     * @return a new selection if the new soldier is not part of a division.
     *         Else a division selection if they are not both part of the same division.
     *         Than the selection wil be deselected.
     *
     *              handle -> current -> not sneaking -> soldier -> new -> division/no division
     *              actions <- current <- not sneaking <- soldier <- new <- division/no division
     */
    private Selector handleNewSoldierSelectedAsSneakingIsDisabledFlow(final PlayerInteractEntityEvent event,
                                                                      final Selector selector) {
        return (getClickedEntityAsSoldier(event).getDivision().isPresent())
                ? handleNewSoldierSelectedAsSneakingIsDisabledAndIsDivisionMemberFlow(event, selector)
                : handleNewSoldierSelectedAsSneakingIsDisabledAndIsNotDivisionMemberFlow(event, selector);
    }

    /**
     *
     * @param event
     * @param selector
     * @return
     *
     *              handle -> current -> not sneaking -> soldier -> new -> no division
     *              actions <- current <- not sneaking <- soldier <- new <- no division
     */
    private Selector handleNewSoldierSelectedAsSneakingIsDisabledAndIsNotDivisionMemberFlow(final PlayerInteractEntityEvent event,
                                                                                            final Selector selector) {
        return newSelector(event);
    }

    /**
     * Check if previous selected soldier is also part of a division.
     * @param event that contains the new selected soldier within a division.
     * @param selector the previous selected soldier that can be part a division.
     * @return TODO
     *
     *              handle -> current -> not sneaking -> soldier -> new -> division -> single/division selector
     *              actions <- current <- not sneaking <- soldier <- new <- division <- single/division selector
     */
    private Selector handleNewSoldierSelectedAsSneakingIsDisabledAndIsDivisionMemberFlow(final PlayerInteractEntityEvent event,
                                                                                         final Selector selector) {
        return selector instanceof DivisionSelector //TODO replaced with getsoldier.getdiv.ispersent needs to be tested
                ? handleNewSoldierSelectedAsSneakingIsDisabledAndIsDivisionMemberCurrentDivisionSelectorFlow(event, (DivisionSelector) selector)
                : handleNewSoldierSelectedAsSneakingIsDisabledAndIsDivisionMemberCurrentSingleSelectorFlow(event, selector);
    }

    /**
     *
     * @param event
     * @param selector
     * @return
     *
     *              handle -> current -> not sneaking -> soldier -> new -> division -> single selector
     *              actions <- current <- not sneaking <- soldier <- new <- division <- single selector
     */
    private Selector handleNewSoldierSelectedAsSneakingIsDisabledAndIsDivisionMemberCurrentSingleSelectorFlow(final PlayerInteractEntityEvent event,
                                                                                                              final Selector selector) {
        return newFormationSelector(event, newSelector(event), ((Division) getClickedEntityAsSoldier(event).getPresentDivision()));
    }

    /**
     *
     * @param event
     * @param selector
     * @return
     *
     *              handle -> current -> not sneaking -> soldier -> new -> division -> division selector
     *              actions <- current <- not sneaking <- soldier <- new <- division <- division selector
     */
    private Selector handleNewSoldierSelectedAsSneakingIsDisabledAndIsDivisionMemberCurrentDivisionSelectorFlow(final PlayerInteractEntityEvent event,
                                                                                                                final DivisionSelector selector) {
        Division division = ((Division) getClickedEntityAsSoldier(event).getPresentDivision());
        return division.equals(selector.getDivision())
                ? removeSelector(selector, getPlayerListName(event))
                : newFormationSelector(event, newSelector(event), division);
    }


    /**
     *
     * @param event
     * @return
     *
     *              handle -> new
     *              actions <- new
     */
    private Selector handleNewSelection(PlayerInteractEntityEvent event) {
        if (isEntityInstanceOfSoldier(event)) {

            if (!isOwner(event)) {
                if (AffiliateHandler.handle().friendlies(getPlayer(event), getClickedEntityAsSoldier(event))) {
                    AffiliateHandler.handle().glowFriendlyTroops(getPlayer(event), getClickedEntityAsSoldier(event));
                } else {
                    TargetHandler.handle().glowTargetTroops(getPlayer(event), getClickedEntityAsSoldier(event));
                }
                return null; //TODO not owner not allowed to control blocked here
            }

            return getClickedEntityAsSoldier(event).getDivision().isPresent()
                    ? newFormationSelector(event, newSelector(event), ((Division) getClickedEntityAsSoldier(event).getPresentDivision()))
                    : newSelector(event);
        } else return null;
    }

    /* ---------------------------------------------- Selection Action Flow ------------------------------------------ */

    //TODO refactor and doc
    private void handleSelectionActionFlow(final PlayerInteractEntityEvent event,
                                           final Selector selector) {
        if (selector != null) {
            if (selector instanceof MultiSelector) {

            } else {
                if (getClickedEntity(event) instanceof Soldier && getClickedEntityAsSoldier(event) == selector.getSoldier()) {
                    if (isSneaking(event)) {
                        if (selector.canEdit(event.getPlayer())&&selector.isSelectedTwice()) {
                            if (event.getPlayer().getInventory().getItemInMainHand().equals(CraftItemStack.asCraftMirror(new net.minecraft.server.v1_13_R2.ItemStack(Items.AIR))) || event.getPlayer().getItemInHand() == null) {
                                event.getPlayer().getInventory().setItemInMainHand(selector.getItem());
                            } else {
                                if (event.getPlayer().getGameMode() == GameMode.CREATIVE) {
                                    org.bukkit.inventory.ItemStack newItem = selector.setItem(event.getPlayer().getInventory().getItemInMainHand());
                                    if (!(newItem.equals(CraftItemStack.asCraftMirror(new net.minecraft.server.v1_13_R2.ItemStack(Items.AIR))))) {
                                        event.getPlayer().getInventory().setItemInMainHand(newItem);
                                    }
                                } else {
                                    event.getPlayer().getInventory().setItemInMainHand(selector.setItem(event.getPlayer().getInventory().getItemInMainHand()));
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    /* ---------------------------------------------- Private Helper Methods ----------------------------------------- */

    private EntityLiving getClickedEntity(PlayerInteractEntityEvent event) {
        return ((CraftLivingEntity) event.getRightClicked()).getHandle();
    }

    private Soldier getClickedEntityAsSoldier(PlayerInteractEntityEvent event) {
        return (Soldier) getClickedEntity(event);
    }

    private String getPlayerListName(PlayerInteractEntityEvent event) {
        return event.getPlayer().getPlayerListName();
    }

    private Player getPlayer(PlayerInteractEntityEvent event) {
        return event.getPlayer();
    }

    private boolean isEntityInstanceOfSoldier(PlayerInteractEntityEvent event) {
        return getClickedEntity(event) instanceof Soldier;//TODO use interface
    }

    //TODO set helper methods and flow methods
    private boolean isSoldierSameAsClickedSoldier(PlayerInteractEntityEvent event, Selector selector) {
        //     System.out.println("isSoldierSameAsClickedSoldier");
        return (selector instanceof MultiSelector)
                ? selector.containsSoldier(getClickedEntityAsSoldier(event))
                : selector.getSoldier() == getClickedEntityAsSoldier(event);
    }

    private void CheckFormation(final PlayerInteractEntityEvent event,
                                final Selector selector) {
        if (((MultiSelector) selector).containsBannerCarrier()) {
            AddToFormation(event, selector);
        }
    }

    private void AddToFormation(final PlayerInteractEntityEvent event,
                                final Selector selector) {
        if (!(getClickedEntityAsSoldier(event).isCommander())) {
            AddToFormationIsNotBannerCarier(event, selector);
        } else {
            AddToFormationDeniedMessage(event);
        }
    }

    private void AddToFormationIsNotBannerCarier(PlayerInteractEntityEvent event, Selector selector) {
        if (((Division) ((MultiSelector) selector).getBannerCarrier().getPresentDivision()).addSoldier(getClickedEntityAsSoldier(event))) {
            AddToFormationSuccesMessage(event, getClickedEntityAsSoldier(event), ((Division) ((MultiSelector) selector).getBannerCarrier().getPresentDivision()));
        } else {
            AddToFormationFailMessage(event, getClickedEntityAsSoldier(event), ((Division) ((MultiSelector) selector).getBannerCarrier().getPresentDivision()));
        }
    }



    private void checkFormation(PlayerInteractEntityEvent event, Selector selector) {
        if (selector.getSoldier().getDivision().isPresent()) {
            addToFormation(event,getClickedEntityAsSoldier(event),selector.getSoldier());
        } else if (getClickedEntityAsSoldier(event).getDivision().isPresent()) {
            addToFormation(event,selector.getSoldier(),getClickedEntityAsSoldier(event));
        }
    }

    private void addToFormation(PlayerInteractEntityEvent event, Soldier recruitSoldier, Soldier leaderSoldier) {
        if (!recruitSoldier.isCommander()) {
            AddToFormationIsNotBannerCarier(event,recruitSoldier,leaderSoldier);
        } else {
            AddToFormationDeniedMessage(event);
        }
    }

    private void AddToFormationIsNotBannerCarier(PlayerInteractEntityEvent event, Soldier recruitSoldier, Soldier leaderSoldier) {
        //  System.out.println("newSoldierSelectedAsSneakingIsEnabledNewMultiSelectionAddToFormationIsNotBannerCarier");
        if (((Division) leaderSoldier.getPresentDivision()).addSoldier(recruitSoldier)) {
            //TODO recruitSoldier.moveTo(); go stand in line
            AddToFormationSuccesMessage(event,recruitSoldier,((Division) leaderSoldier.getPresentDivision()));
        } else {
            AddToFormationFailMessage(event,recruitSoldier,((Division) leaderSoldier.getPresentDivision()));
        }
    }

    private void AddToFormationSuccesMessage(PlayerInteractEntityEvent event, Soldier soldier, Division division) {
        //    System.out.println("newSoldierSelectedAsSneakingIsEnabledCurrentMultiSelectionAddToFormationSuccesMessage");
        event.getPlayer().sendMessage(soldier
                + " added to formation "
                + division);
    }

    private void AddToFormationFailMessage(PlayerInteractEntityEvent event, Soldier soldier, Division division) {
        //   System.out.println("newSoldierSelectedAsSneakingIsEnabledCurrentMultiSelectionAddToFormationFailMessage");
        event.getPlayer().sendMessage("can not added "
                + soldier
                + " to "
                + division
                + " maximum limit reached.");
    }

    private void AddToFormationDeniedMessage(PlayerInteractEntityEvent event) {
        //  System.out.println("newSoldierSelectedAsSneakingIsEnabledCurrentMultiSelectionAddToFormationDeniedMessage");
        event.getPlayer().sendMessage("Cannot assign a banner carrier to a formation without disbanding him from this current formation.");
    }

    private Selector newSelector(PlayerInteractEntityEvent event) {
        return addSelector(new Selector(event.getPlayer(), getClickedEntityAsSoldier(event)), getPlayerListName(event));
    }

    private DivisionSelector newFormationSelector(PlayerInteractEntityEvent event, Selector selector, Division division) {
        return (DivisionSelector) addSelector(new DivisionSelector(selector, division), getPlayerListName(event));
    }

    //TODO -------------------
    private <S extends Selector> S addSelector(S selector, String playerListName) {
        removeSelector(SELECTOR_MAP.get(playerListName),playerListName);//TODO not best practise
        SELECTOR_MAP.put(playerListName,selector);
        return selector;
    }

    private <S extends Selector> S removeSelector(S selector, String playerListName) {
        if (selector!=null) {selector.deleteSelection();} // deselecteer huidige soldier
        SELECTOR_MAP.remove(playerListName);
        return null;
    }
    //TODO -----------------------

    private boolean isSneaking(PlayerInteractEntityEvent event) {
        return event.getPlayer().isSneaking();
    }
    private boolean isOwner(PlayerInteractEntityEvent event) {
        return event.getPlayer().getUniqueId().equals(getClickedEntityAsSoldier(event).getPlayerUniqueId());
    }
}
