package com.nielsstrychi.Handlers;

import com.nielsstrychi.Util.Vanilla;
import net.minecraft.server.v1_13_R2.*;
import org.bukkit.event.entity.EntityTargetEvent;

public class PathfinderGoalHurtByTargetNonFriendlyAttackableTarget extends PathfinderGoalHurtByTarget {

    public PathfinderGoalHurtByTargetNonFriendlyAttackableTarget(EntityCreature entitycreature, boolean flag, Class<?>... aclass) {
        super(entitycreature, flag, aclass);
    }

    /**
     * This method prevents the entity from forgetting his target when far away only when the entity is dead he stops attacking.
     */
    @Override @Vanilla
    public void d() {
        if (this.g == null || !this.g.isAlive()) { //TODO
            this.e.setGoalTarget((EntityLiving)null, EntityTargetEvent.TargetReason.FORGOT_TARGET, true);
            this.g = null;
        }
    }
}
