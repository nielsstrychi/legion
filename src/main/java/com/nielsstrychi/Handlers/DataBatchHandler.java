package com.nielsstrychi.Handlers;

import com.nielsstrychi.LegionPlugin;
import com.nielsstrychi.Providers.ContextProvider;
import com.nielsstrychi.Repositories.SQL;
import org.bukkit.scheduler.BukkitRunnable;

public class DataBatchHandler {

    /**
     * Singleton instance of this class.
     */
    private static DataBatchHandler INSTANCE;

    /**
     * Asynchronously running task for entity path finding.
     * best practice to cancel if not needed. EXAMPLE server shutdown.
     */
    private BukkitRunnable RUNNABLE;

    /**
     * The delay the runnable wil execute a batch.
     * The delay is a long that stands for server ticks 20 tick equals 1 seconds.
     */
    private final long RUNNABLE_DELAY = 600L; // 30 SECONDS

    /**
     * Constructor
     */
    private DataBatchHandler() {
    }

    /**
     * Initializes the singleton if not yet created and returns it.
     * Else the already existing singleton instance is returned.
     * @return the instance of the singleton.
     */
    public static DataBatchHandler handle() {
        return INSTANCE == null ? INSTANCE = new DataBatchHandler() : INSTANCE;
    }

    public final void ticking() {
        RUNNABLE = new BukkitRunnable() {
            @Override
            public void run() {
                long startTime = System.nanoTime();
                executeBatches();
                long endTime = System.nanoTime();
                System.out.println("batch time: " + (endTime - startTime));
            }
        };
        RUNNABLE.runTaskTimerAsynchronously(LegionPlugin.getInstance(), 1200L, RUNNABLE_DELAY);
    }

    public void stop() {
        if (this.RUNNABLE != null)
        this.RUNNABLE.cancel();
        this.executeBatches();
    }

    public void executeBatches() {
        ContextProvider.ENTITY_REPOSITORY.executeBatch(SQL.UPDATE, SQL.DELETE);
        ContextProvider.DIVISION_REPOSITORY.executeBatch(SQL.UPDATE, SQL.DELETE);
        ContextProvider.PATROL_REPOSITORY.executeBatch(SQL.UPDATE, SQL.DELETE);
        ContextProvider.AFFILIATE_REPOSITORY.executeBatch(SQL.UPDATE, SQL.DELETE);
    }
}
