package com.nielsstrychi.Handlers;

import com.nielsstrychi.Packets.GlowColor;
import com.nielsstrychi.Packets.PacketGlowing;

import com.nielsstrychi.Util.Converter;
import net.minecraft.server.v1_13_R2.EntityLiving;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;

import javax.annotation.Nullable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * This handler singleton instance keeps a Set with GlowingState objects.
 * This Set represents all the glowing entities on the server.
 * It wil handle the glowing state of those entities with a helper object GlowingState.
 * It uses a Protocol to send packets to the player and let the entity glow only for them.
 *
 * To let a entity glow for a player use the code snippet below.
 * GlowHandler.handle().enableGlow(Player, LivingEntity, GlowColor);
 *
 * To stop glowing of a entity use the code snippet below.
 * GlowHandler.handle().disableGlow(Player, LivingEntity);
 *
 * If you need the GlowingState object use one of the queries.
 * GlowHandler.handle().query(Player, LivingEntity);
 *
 * @author Nielsstrychi
 */
public class GlowHandler { //TODO add unglow feature, unregistery on shutdown

    /**
     * Singleton instance of this class.
     */
    private static GlowHandler INSTANCE;

    /**
     * Set of GlowingEntities in the server.
     */
    private final Set<GlowingState> GLOWING_SET = new HashSet<>();

    /**
     * Protocol to manipulate packets and send them to the receiver.
     * It gives a glow effect to selected entities that is only visible for the receiver.
     */
    private final PacketGlowing packet = new PacketGlowing();

    /**
     * Constructor
     */
    private GlowHandler() {
    }

    /**
     * Initializes the singleton if not yet created and returns it.
     * Else the already existing singleton instance is returned.
     * @return the instance of the singleton.
     */
    public static GlowHandler handle() {
        return INSTANCE == null ? INSTANCE = new GlowHandler() : INSTANCE;
    }

    /**
     * Query trough the GLOWING_SET.
     * If no params are included "null" inserted the query wil return the whole set.
     * @param receiver filtered if found in the object, else if null criteria gets ignored.
     * @param target filtered if found in the set, else if null criteria gets ignored.
     * @param color filtered if found in the set, else if null criteria gets ignored.
     * @return Set of filtered values that match the input params.
     */
    public Set<GlowingState> query(final @Nullable Player receiver,
                                   final @Nullable LivingEntity target,
                                   final @Nullable GlowColor color) {
        return this.GLOWING_SET.stream()
                .filter(e -> receiver == null || e.getReceiver().equals(receiver))
                .filter(e -> target == null || e.getTarget().equals(target))
                .filter(e -> color == null || e.getColor().equals(color))
                .collect(Collectors.toSet());
    }

    /**
     * Query trough the GLOWING_SET.
     * Null params are not allowed if null is inserted the query wil return a empty optional.
     * @param receiver the receiver who sees the glowing target
     * @param target the glowing target
     * @return Optional GlowingState if the query found at least one else empty optional is returned.
     */
    public Optional<GlowingState> query(final Player receiver,
                                        final LivingEntity target) {
        if (receiver == null || target == null) return Optional.empty();
        return this.GLOWING_SET.stream()
                .filter(e -> e.getReceiver().equals(receiver))
                .filter(e -> e.getTarget().equals(target))
                .findAny();
    }

    public Optional<GlowingState> query(final Player receiver,
                                        final EntityLiving target) {
        return (target != null)
                ? this.query(receiver, Converter.convert(target))
                : Optional.empty();
    }

    /**
     * Must be package private not allowed to be called other then GlowingState.
     * Sends a packet request to the glowing protocol and add the GlowingState object to the GLOWING_SET.
     * @param state passed trough by its own constructor.
     */
    final void enableGlow(final GlowingState state) {
        if (this.GLOWING_SET.contains(state)) {
            overrideGlow(state);
        } else {
            packet.setGlowing(state.getTarget(), state.getReceiver(), true, state.getColor());
            this.GLOWING_SET.add(state);
        }
    }

    public void enableGlow(Player player, LivingEntity entity, GlowColor color) {
        if (player != null && entity != null) {
            this.enableGlow(new GlowingState(player, entity, color));
        }
    }

    /**s
     * If the GlowingState exist override the current with the new one.
     * @param state new one to add to the list.
     */
    private void overrideGlow(final GlowingState state) {
        packet.setGlowing(state.getTarget(), state.getReceiver(),true, state.getColor());
        this.GLOWING_SET.remove(state); // Old state gets removed
        this.GLOWING_SET.add(state); // New state gets inserted
    }

    /**
     * Stops the glowing and removes GlowingState form GLOWING_SET,
     * GlowingState haves no pointer anymore and will be garbage collect.
     * @param state object to be removed.
     */
    private void disableGlow(final GlowingState state) {
        packet.setGlowing(state.getTarget(), state.getReceiver(),false);
        this.GLOWING_SET.remove(state);
    }

    public final void disableGlow(final Player player, final EntityLiving entity) {
        if (player != null && entity != null) {
            this.query(player, entity).ifPresent(this::disableGlow);
        }
    }

    public final void disableGlow(final Player player, final LivingEntity entity) {
        if (player != null && entity != null) //TODO make annotation that throws exception?
        this.disableGlow(player, Converter.convert(entity));
    }
}

/**
 * GlowingState helper object to store @ runtime data of the glowing state of entities in the server.
 * Be aware the object is equal if they share the same receiver and target, Color field is ignored.
 *
 * @author Nielsstrychi
 */
final class GlowingState {

    private final Player receiver;
    private final LivingEntity target;
    private final GlowColor color;

    /**
     * Player & Receiver are not allowed to be null.
     * The GlowHandler wil handle this state object further on the enableGlow method.
     * @param receiver Player that wil see the glowing.
     * @param target Entity that must glow for the Player.
     * @param color glowing color.
     */
    public GlowingState(final Player receiver, //TODO notnull anotainon
                        final LivingEntity target,
                        final GlowColor color) {
        this.receiver = receiver;
        this.target = target;
        this.color = color;
        GlowHandler.handle().enableGlow(this);
    }

    public Player getReceiver() {
        return receiver;
    }

    public LivingEntity getTarget() {
        return target;
    }

    public GlowColor getColor() {
        return color;
    }

    /**
     * Checks for equality of the object only on fields receiver & target
     * @param o to be check if equal.
     * @return true if equal if not false.
     */
    @Override
    public boolean equals(final Object o) { // TODO null checks?
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GlowingState state = (GlowingState) o;
        return receiver.equals(state.receiver) &&
                target.equals(state.target);
    }

    /**
     * Checks for hash equality only on fields receiver & target.
     * @return hashCode of receiver & target concatenated.
     */
    @Override
    public int hashCode() {
        return Objects.hash(receiver, target);
    } //TODO is it aways accurate? bukkint nms entity same hash?

    @Override
    public String toString() {
        return "GlowingState{" +
                "receiver=" + receiver +
                ", target=" + target +
                ", color=" + color +
                '}';
    }
}

