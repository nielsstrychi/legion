package com.nielsstrychi.Handlers;


import com.nielsstrychi.Providers.ContextProvider;
import com.nielsstrychi.Selectors.Selector;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;

import org.bukkit.Chunk;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

public class ClickHandler {

    /*TESTING STILL*/

    @SuppressWarnings("deprecation")
    public static void handleRightFarClick(PlayerInteractEvent event) {
        Player player = event.getPlayer();
        int range = 50;

        HashMap<Chunk, Set<Block>> chunks = new HashMap<>();

        for (Block block : player.getLineOfSight((Set<Material>) null, range)) {
            Chunk chunk = block.getChunk();

            Set<Block> blocks;
            if (chunks.containsKey(chunk)) { // The chunk was already registered
                blocks = chunks.get(chunk);
            } else { // The chunk was not yet registered
                blocks = new HashSet<Block>();
            }
            blocks.add(block);
            chunks.put(chunk, blocks);
        }

        Entity targetedEntity = null;
        double distance = Double.MAX_VALUE;

        for (Chunk chunk : chunks.keySet()) {
            Entity[] entites = chunk.getEntities();

            for (Block block : chunks.get(chunk)) {
                for (Entity entity : entites) {
                    if (entity instanceof LivingEntity &&entity!=player)
                        if (isInBlock(entity, block)) {
                            double tmpDistance = entity.getLocation().distance(player.getLocation());

                            if (targetedEntity == null || tmpDistance < distance) {
                                targetedEntity = entity;
                                distance = tmpDistance;
                            }
                        }
                }
            }
        }

        if (targetedEntity == null) {
            Optional<Selector> selector = ContextProvider.SELECTION_HANDLER.getSelector(player);
            if(selector.isPresent() && selector.get().getSoldier() != null) {
                selector.get().moveSoldier(player.getTargetBlock(null, 50).getLocation());
            }
        } else {
            ContextProvider.SELECTION_HANDLER.handleSelectionFlow(new PlayerInteractEntityEvent(event.getPlayer(), targetedEntity));
        }
    }


    private static boolean isInBlock(Entity entity, Block block) {
        return (entity.getWorld() == block.getWorld()) && (entity.getLocation().distance(getCenterOfBlock(block)) < 1);
    }

    private static Location getCenterOfBlock(Block block) {
        return block.getLocation().add(0.5, 0.5, 0.5);
    }
}
