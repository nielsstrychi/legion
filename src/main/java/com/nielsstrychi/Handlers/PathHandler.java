package com.nielsstrychi.Handlers;

import com.nielsstrychi.Entities.Soldiers.PlayerOwnedEntity;
import com.nielsstrychi.Entities.Soldiers.Soldier;
import com.nielsstrychi.LegionPlugin;
import com.nielsstrychi.Packets.GlowColor;
import com.nielsstrychi.Util.Converter;
import com.nielsstrychi.Util.NotNull;
import net.minecraft.server.v1_13_R2.EntityInsentient;
import org.bukkit.Location;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityTargetEvent;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.*;

/**
 * A singleton instance handler that holds a Map item with a key LivingEntity as master and a Set of Soldiers as followers.
 * The handler drives the followers to the master, With the usage of a asynchronously running BukkitRunnable that trigger them to move.
 *
 * @author Nielsstrychi
 */
public class PathHandler {

    /**
     * Singleton instance of this class.
     */
    private static PathHandler INSTANCE;

    /**
     * Asynchronously running task for entity path finding.
     * best practice to cancel if not needed. EXAMPLE server shutdown.
     */
    private BukkitRunnable RUNNABLE;

    /**
     * Followers map that contains a LivingEntity(key) that represent the master,
     * followed by a Set<Soldier>(value) that represent the active followers of master.
     */
    private final Map<LivingEntity, Set<Soldier>> FOLLOWERS_MAP = new HashMap<>();

    /**
     * Maximum path finding radius is the
     */
    public static final int MAX_PATH_FINDING_RADIUS = 15;

    /**
     * The delay each runnable wil check his conditions.
     * The delay is a long that stands for server ticks 20 tick equals 1 seconds.
     */
    private final long RUNNABLE_DELAY = 30L; // 1.5 SECONDS

    /**
     * Map that caches the destination difference for path finding vector calculations.
     * This so it does not recalculate when the final destination did not change.
     */
    private final Map<PathVector, PathVector> VECTOR_CACHE = new HashMap<>();

    /**
     * Constructor initializes the runnable and starts a path finding task that runs until canceled.
     */
    private PathHandler() {
        handleFollowingPath();
    }

    /**
     * Initializes the singleton if not yet created and returns it.
     * Else the already existing singleton instance is returned.
     * @return the instance of the singleton.
     */
    public static PathHandler handle() {
        return INSTANCE == null ? INSTANCE = new PathHandler() : INSTANCE;
    }

    /**
     *  Called once by the constructor.
     *  Starts a asynchronously task to update followers target positions every second.
     *  If the master is valid followers are updated else master its followers are canceled.
     */
    private void handleFollowingPath() { //TODO what with exceptions cancel and start over?
        RUNNABLE = new BukkitRunnable() {
            @Override
            public void run() {
                copyFollowerMap().forEach((master, followers) -> {
                    if (!isMasterValid(master)) return;
                    updateFollowingPath(master.getLocation(), followers);
                });
            }
        };
        RUNNABLE.runTaskTimerAsynchronously(LegionPlugin.getInstance(), 0L, RUNNABLE_DELAY);
    }

    /**
     * Checks if the master is a valid LivingEntity that can be followed.
     * If master is not valid all his followers gets canceled and master gets removed form the FOLLOWERS_MAP.
     * @param master the LivingEntity that is being followed.
     * @return true if master is valid to follow else false and master followers gets canceled.
     */
    private boolean isMasterValid(final LivingEntity master) {
        if (master == null) return false;
        if (!(master instanceof Player && ((Player) master).isOnline())) {
            this.cancelFollowers(master);
            return false;
        }
        if (master.isDead()) {
            this.cancelFollowers(master);
            return false;
        }
        return true;
    }

    /**
     * Update the location of masters followers and checks if followers are valid.
     * Valid followers that are performing other duties are paused of following there master,
     * and are ignored so no new path request gets invoked for them.
     * @param location of master where the followers need move.
     * @param followers Set of Soldiers that needs to be move to the new master location.
     */
    private void updateFollowingPath(final Location location, final Set<Soldier> followers) {//TODO does this work with a copy list?
        followers.stream()
                .filter(this::isFollowerValid)
                .filter(this::isPaused)
                .map(soldier -> ((EntityInsentient) (soldier.getBukkitEntity())
                        .getHandle())
                        .getNavigation())
                .forEach(f ->  f.a(     //TODO what with null values //TODO add pause when entity have other task //TODO follow only when insight
                        location.getX(),
                        location.getY(),
                        location.getZ(),
                        1.75f));
    }

    /**
     * Checks if the followers are valid and can follow.
     * @param soldier to be checked if they are valid.
     * @return true if they are valid else false.
     */
    private boolean isFollowerValid(final Soldier soldier) {
        return soldier.isAlive();
    }

    /**
     * When a soldier is performing an other task at the moment and can not follow.
     * @param soldier that may be paused to follow.
     * @return true if the soldier is doing other task that intercept them from following.
     */
    private boolean isPaused(final Soldier soldier) {
        return soldier.isAttacking();
    }

    /**
     * Copy the FOLLOWERS_MAP so we can not get a ConcurrentModificationException.
     * @return a new HashMap with cloned values of FOLLOWERS_MAP
     */
    private Map<LivingEntity, Set<Soldier>> copyFollowerMap() {
        return new HashMap<>(FOLLOWERS_MAP);
    }

    /**
     * Cancels all followers of the master.
     * @param master the target who all entities follow.
     */
    public void cancelFollowers(final LivingEntity master) {
        this.getFollowers(master).forEach(soldier -> soldier.stopFollowing(master));
        this.FOLLOWERS_MAP.remove(master);
    }

    /**
     * Cancels the individual follower following master.
     * @param master the target that the follower follows.
     * @param follower a follower of master.
     */
    public void cancelFollower(final LivingEntity master, final Soldier follower) {
        this.getFollowers(master).remove(follower);
    }

    /**
     * Adds the follower to the FOLLOWER_MAP,
     * follower path finding get handled dynamically.
     * @param master the followed entity.
     * @param follower the following soldier.
     */
    public void newFollower(@NotNull final LivingEntity master, @NotNull final Soldier follower) {
        if (master == null) throw new NullPointerException("Master = null");
        if (follower == null) throw new NullPointerException("Follower = null");
        this.getFollowers(master).add(follower);
    }

    /**
     * Returns all the followers if the master already exist in the FOLLOWERS_MAP,
     * else a empty Set of Soldiers is inserted and returned.
     * @param master is the key to get all his followers.
     * @return Set of Soldiers that represent followers of the master.
     */
    private Set<Soldier> getFollowers(final LivingEntity master) {
        if (this.FOLLOWERS_MAP.containsKey(master)) {
            return this.FOLLOWERS_MAP.get(master);
        } else {
            this.FOLLOWERS_MAP.put(master, new HashSet<>());
            return this.FOLLOWERS_MAP.get(master);
        }
    }

    public int countFollowers(final LivingEntity master) {
        return this.getFollowers(master).size();
    }

    /**
     * Handles the Entity vanilla path finding occurrences.
     * When the entity his final location is not out of the MAX_PATH_FINDING_RADIUS radius then the vanilla path finding wil be applied.
     * Else handleLongDistancePathFinding wil handle long distance path finding.
     * @param entity the entity that is on the move his final location.
     */
    public void handlePathFinding(final PlayerOwnedEntity entity) {
            if (entity.getCurrentLocation().distance((Location) entity.getFinalLocation().get()) > MAX_PATH_FINDING_RADIUS) {
                handleLongDistancePathFinding(entity);
            } else {
                entity.setPath((Location) entity.getFinalLocation().get());
                entity.setFinalLocation(null); //TODO reset final location?
            }
        }

    /**
     * Synchronous task that checks when a entity has arrived at his position.
     * If the final location is in reach less than 1 block from his position,
     * the task is canceled else the path wil be calculated again.
     * @param entity the entity that wants to reach his far destination.
     */
    private void handleLongDistancePathFinding(final PlayerOwnedEntity entity) {
            new BukkitRunnable() {
                @Override
                public void run() {
                    if (!entity.getFinalLocation().isPresent() || entity.getCurrentLocation().distance((Location) entity.getFinalLocation().get()) < 1) {
                        this.cancel();
                    } else {
                        entity.setPath(pathFinding((Location) entity.getFinalLocation().get(), entity.getCurrentLocation()));
                    }
                }
            }.runTaskTimer(LegionPlugin.getInstance(), 0L, RUNNABLE_DELAY);
        }

    /**
     * Synchronous task that wil dynamically walk to the target with the fastest route.
     * When the target is in the reach radius the task is canceled and the vanilla enemy finding system wil take over.
     * Glow is applied so the attacked entity wil glow even when there is not yet a entity target event trigger.
     * @param attacker the evil attacking entity! that needs to walk a far distance to find his target.
     * @param target the victim that is far away and not aware a entity a far away target is targeting him.
     */
    public void handleLongDistanceEnemyFinding(final Soldier attacker, final LivingEntity target) {
        new BukkitRunnable() {
            @Override
            public void run() {
                if ((attacker.getFinalLocation() == null & attacker.isAttacking()) && target != null && !target.isDead()) {
                    if(attacker.getCurrentLocation().distance(target.getLocation()) < MAX_PATH_FINDING_RADIUS) {
                        ((EntityInsentient) attacker).setGoalTarget(Converter.convert(target),
                                EntityTargetEvent.TargetReason.OWNER_ATTACKED_TARGET, true);
                        this.cancel();
                    } else {
                        GlowHandler.handle().enableGlow(attacker.getPlayer().getPlayer(), target, GlowColor.ENEMY_LEGIO_ATK);
                        attacker.setPath(pathFinding(target.getLocation(), attacker.getCurrentLocation()));
                    }
                } else {
                    GlowHandler.handle().disableGlow(attacker.getPlayer().getPlayer(), target);
                    this.cancel();
                }
            }
        }.runTaskTimer(LegionPlugin.getInstance(), 0L, RUNNABLE_DELAY);
    }

    /**
     * Starts the path finding.
     * When a final location is to far for the vanilla path finding system,
     * we wil calculate the fastest rout to get from point A to B.
     * Else when the final location is in reach (closer than the MAX_PATH_FINDING_RADIUS),
     * then we wil return the original final location back so the vanilla system can handle the small distance.
     * @param finalLocation the final location we want to reach.
     * @param currentLocation the current location we are present a the moment.
     * @return a calculated closer location to walk to or the final location if close enough.
     * @throws IllegalArgumentException thrown when the inputted locations are not in the same world.
     */
    private Location pathFinding(final Location finalLocation, final Location currentLocation) {
        if (currentLocation.getWorld().equals(finalLocation.getWorld())) {
            if (currentLocation.distance(finalLocation) > MAX_PATH_FINDING_RADIUS) {
                return calculateTravelPoint(finalLocation, currentLocation);
            } else {
                return finalLocation;
            }
        } else {
            throw new IllegalArgumentException("Path finding to locations that doest share the same world is not possible!");
        }
    }

    /**
     * A beautiful piece of art.
     * Calculates the closest position on a straight line to the final location the vanilla path finding system can reach.
     * This wil repeat is self dynamically when the final location changes by synchronous task runners that call this method.
     * Uses caching to minimalism the repeated calculations when the final location did not change and the vector gets reused.
     * @param finalLocation the location we want to reach and is to far out of range for the vanilla path finding.
     * @param currentLocation the location where we are currently.
     * @return the closest location a we can reach with the vanilla path finding system.
     */
    private Location calculateTravelPoint(final Location finalLocation, final Location currentLocation) {
        PathVector source = new PathVector(currentLocation.getX(), currentLocation.getZ());
        PathVector destination = new PathVector(finalLocation.getX(), finalLocation.getZ());
        PathVector sourceToDestination = VECTOR_CACHE.containsKey(destination)
                ? VECTOR_CACHE.get(destination)
                : PathVector.fromTo(source, destination);
        PathVector intermediate = sourceToDestination.scale(MAX_PATH_FINDING_RADIUS / sourceToDestination.getNorm()).add(source);
        return new Location(currentLocation.getWorld(), intermediate.getX(), currentLocation.getY(), intermediate.getZ()); //TODO calc Y
    }

    /**
     * Clears the cached vectors.
     */
    public void clearVectorCache() {
        VECTOR_CACHE.clear();
    }

}

/**
 * Helper class to define the next vector position, with immutable class design.
 *
 * Big thanks to
 * @author Jasper
 * for helping me with the path finding calculations.
 */
final class PathVector {

    private final double x;
    private final double z;

    PathVector(double x, double z) {
        this.x = x;
        this.z = z;
    }

    private PathVector(PathVector other) {
        this.x = other.getX();
        this.z = other.getZ();
    }

    double getX() {
        return x;
    }

    double getZ() {
        return z;
    }

    private PathVector subtract(PathVector other) {
        return new PathVector(other.getX() - this.x, other.getZ() - this.z);
    }

    PathVector add(PathVector other) {
        return new PathVector(other.getX() - this.x, other.getZ() - this.z);
    }

    PathVector scale(double factor) {
        return new PathVector(this.x * factor, this.z * factor);
    }

    double getNorm() {
        return Math.sqrt((this.x * this.x) + (this.z * this.z));
    }

    static PathVector fromTo(PathVector from, PathVector to) {
        return new PathVector(to.subtract(from));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PathVector vector = (PathVector) o;
        return Double.compare(vector.x, x) == 0 &&
                Double.compare(vector.z, z) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(x, z);
    }

    @Override
    public String toString() {
        return "Vector{" +
                "x=" + x +
                ", z=" + z +
                '}';
    }
}
