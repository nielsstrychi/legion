package com.nielsstrychi.Handlers;

import com.nielsstrychi.Entities.Soldiers.Soldier;
import com.nielsstrychi.Util.Vanilla;
import net.minecraft.server.v1_13_R2.*;
import org.bukkit.event.entity.EntityTargetEvent;

import javax.annotation.Nullable;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;

/**
 * Extends PathfinderGoalNearestAttackableTarget to override the method that is responisble to find nearby attackible entitiees.
 * Here whe then filter friendly units out so they dont get attacked.
 * @param <T> class of attack able entity i think.
 *
 * @author Nielsstrychi
 * @author Vanilla minecraft code
 */
@Vanilla
public class PathfinderGoalNearestNonFriendlyAttackableTarget<T extends EntityLiving> extends PathfinderGoalNearestAttackableTarget {

    /**
     * PathfinderGoalNearestAttackableTarget line 18 (in constructor default is 10)
     */
    private final int i = 10;
    protected final Class<T> a;

    /**
     * Override vanilla constructor
     * @param entitycreature the creature that is gone attack.
     * @param oclass the class of entities he wish to attack.
     * @param flag i dont know.
     */
    public PathfinderGoalNearestNonFriendlyAttackableTarget(EntityCreature entitycreature, Class oclass, boolean flag) {
        super(entitycreature, oclass, flag);
        this.a = oclass;
    }

    /**
     * Loops to search which entities are close.
     * Added extra logic to filter the friendly entities out the list so that they can target faster entities that are farther away.
     * Else they keep looking at the closest one and if this one is friendly they keep ignoring the target after it and dont find an other non friendly entity near by.
     * @return i dont know. not important. a boolean i think if they found one successfully.
     */
    @Vanilla
    @Override
    public boolean a() {
        if (this.i > 0 && this.e.getRandom().nextInt(this.i) != 0) {
            return false;
        } else if (this.a != EntityHuman.class && this.a != EntityPlayer.class) {
            List<T> list = this.e.world.a(this.a, this.a(this.i()), this.c);
            if (list.isEmpty()) {
                return false;
            } else {
                Collections.sort(list, this.b);

                Optional<T> optional = list.stream().filter(entity -> !AffiliateHandler.handle().friendlies((Soldier) this.e, entity)).findFirst();
                if (optional.isPresent()) { // Above whe just filtered all friendly entities out the list.
                    this.d = optional.get();
                    return true;
                } else return false;
            }
        } else {
            this.d = this.e.world.a(this.e.locX, this.e.locY + (double)this.e.getHeadHeight(), this.e.locZ, this.i(), this.i(), new Function<EntityHuman, Double>() {
                @Nullable
                public Double apply(@Nullable EntityHuman entityhuman) {
                    ItemStack itemstack = entityhuman.getEquipment(EnumItemSlot.HEAD);
                    return PathfinderGoalNearestNonFriendlyAttackableTarget.super.e instanceof EntitySkeleton && itemstack.getItem() == Items.SKELETON_SKULL || PathfinderGoalNearestNonFriendlyAttackableTarget.super.e instanceof EntityZombie && itemstack.getItem() == Items.ZOMBIE_HEAD || PathfinderGoalNearestNonFriendlyAttackableTarget.super.e instanceof EntityCreeper && itemstack.getItem() == Items.CREEPER_HEAD ? 0.5D : 1.0D;
                }
            }, this.c);
            return this.d != null;
        }
    }

    /**
     * This overrides the stop method then normaly gets triggerd when far away TODO
     */
    @Override
    public void d() {
        if (this.g == null || !this.g.isAlive()) {
            this.e.setGoalTarget((EntityLiving)null, EntityTargetEvent.TargetReason.FORGOT_TARGET, true);
            this.g = null;
        }
    }
}
