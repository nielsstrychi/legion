package com.nielsstrychi.Handlers;

import com.nielsstrychi.Data.Affiliate;
import com.nielsstrychi.Entities.Soldiers.PlayerOwnedEntity;
import com.nielsstrychi.Entities.Soldiers.Soldier;
import com.nielsstrychi.LegionPlugin;
import com.nielsstrychi.Packets.GlowColor;
import com.nielsstrychi.Providers.ContextProvider;
import net.minecraft.server.v1_13_R2.EntityLiving;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Handles the Affiliates requests of Players and handles the behavior of there soldiers.
 * Affiliates get persisted in the database, but the work objects are stored in memory with a map that represents the state of the database.
 * This for better performance when checking entity behaviors and te minimize unnecessary query calls to the database.
 * The caching happens on startup of the server in the cache() method.
 *
 * @author Nielsstrychi
 */
public class AffiliateHandler {

    /**
     * Singleton instance of this class.
     */
    private static AffiliateHandler INSTANCE;

    /**
     * Cache map, it represents the same state as in the database. Values get cached for performance reasons.
     * When soldiers need to decide fast of an entity is friendly or not we dont want database operations slowing us down.
     */
    private final Map<UUID, Set<UUID>> AFFILIATE_PERSISTENCE_MAP = new HashMap<>();

    /**
     * Request map, when affiliate request are pending for the state gets hold here for 45 seconds.
     * So that the same request can not be spammed twice or more.
     * And the receiver haves time to accept/denied.
     */
    private final Map<Player, Player> AFFILIATE_REQUEST_MAP = new HashMap<>();

    /**
     * Constructor
     */
    private AffiliateHandler() {
    }

    /**
     * Initializes the singleton if not yet created and returns it.
     * Else the already existing singleton instance is returned.
     * @return the instance of the singleton.
     */
    public static AffiliateHandler handle() {
        return INSTANCE == null ? INSTANCE = new AffiliateHandler() : INSTANCE;
    }

    /**
     * This needs to happen on start up.
     * All player UUIDs who ever joined the sever are checked for relations.
     * If there are relations they are stored in the AFFILIATE_PERSISTENCE_MAP to perform operation on.
     */
    public void cache() {
        for (OfflinePlayer player : Bukkit.getServer().getOfflinePlayers()) {
            List<Affiliate> affiliates = ContextProvider.AFFILIATE_REPOSITORY.findPlayerAffiliates(player.getUniqueId());
            AFFILIATE_PERSISTENCE_MAP.put(player.getUniqueId(),
                    affiliates.stream().map(a -> player.getUniqueId().equals(a.getAssociateOne())
                            ? a.getAssociateTwo()
                            : a.getAssociateOne()).collect(Collectors.toSet()));
        }
    }

    /**
     *
     * @param soldier
     * @param target
     * @return
     */
    public boolean friendlies(final Soldier soldier, final EntityLiving target) {
        if (target instanceof PlayerOwnedEntity) {
           return ((PlayerOwnedEntity) target).getPlayerUniqueId()
                   .equals(soldier.getPlayerUniqueId())
                   || this.AFFILIATE_PERSISTENCE_MAP.get(soldier.getPlayerUniqueId())
                   .contains(((PlayerOwnedEntity) target).getPlayerUniqueId());
        } else
        return false;
    }
    
    public boolean friendlies(final Player player, final Soldier soldier) {
        return this.AFFILIATE_PERSISTENCE_MAP.get(player.getUniqueId())
                .contains(soldier.getPlayerUniqueId());
    }

    /**
     * Handles a affiliate request propagated from the AffiliateExecutor.
     * Checks if the parameter input is valid and send a response to the requester and receiver if the request is valid.
     * @param requester the Player who sends a request/invite to become affiliates with receiver.
     * @param receiver the Player who wil receive a request if input is valid, and need to accept, denied or ignore.
     *                 They have 45 seconds to response a timer is started.
     */
    public void handleNewAffiliateRequest(final Player requester, final Player receiver) {
        if (requester.getUniqueId().equals(receiver.getUniqueId())) {requester.sendRawMessage("You just requested to be a affiliate with yourself, you are already your own best friend :-)."); return;}
        if (!ContextProvider.AFFILIATE_REPOSITORY.findByAffiliates(requester.getUniqueId(), receiver.getUniqueId()).isPresent()) {
            if (AFFILIATE_REQUEST_MAP.containsKey(receiver)) {
                requester.sendRawMessage(String.format("%s still has open affiliate request pending. Wait a few seconds before applying a new request.",
                        receiver.getDisplayName()));
            } else {
                AFFILIATE_REQUEST_MAP.put(receiver, requester);
                affiliatePendingRequest(requester, receiver);
                requester.sendRawMessage(String.format("Successful send a affiliate request to %s.",
                        receiver.getDisplayName()));
                receiver.sendRawMessage(String.format("%s send you an affiliate request. Execute '/lg af accept' or '/lg af denied'.",
                        requester.getDisplayName()));
            }
        } else {
            requester.sendRawMessage(String.format("You are already a affiliate with %s.",
                    receiver.getDisplayName()));
        }
    }

    /**
     * Starts a timer of 45 seconds.
     * When the time is up and there has been no response of the receiver the pending request is deleted.
     * @param requester the Player who requests to be affiliates.
     * @param receiver the Player that need to respond in 45 seconds.
     */
    public void affiliatePendingRequest(final Player requester, final Player receiver) {
        new BukkitRunnable() {
            @Override
            public void run() {
                if (AFFILIATE_REQUEST_MAP.containsKey(receiver)) {
                    requester.sendRawMessage(String.format("%s did not respond to your affiliate request.",
                            receiver.getDisplayName()));
                    AFFILIATE_REQUEST_MAP.remove(receiver);
                } this.cancel();
            }
        }.runTaskLater(LegionPlugin.getInstance(), 20*45); // 45 seconds
    }

    /**
     * Handles the response of the receiver.
     * @param receiver the Player who issued the response on a request targeted to him.
     * @param accepted if they have accepted or not.
     */
    public void handleAffiliateResponse(final Player receiver, boolean accepted) {
        if (AFFILIATE_REQUEST_MAP.containsKey(receiver)) {
            if (accepted) {
                AFFILIATE_REQUEST_MAP.get(receiver).sendRawMessage(String.format("%s accepted your invitation to be affiliates.",
                        receiver.getDisplayName()));
                receiver.sendRawMessage(String.format("You accepted the invitation you are now affiliates with %s.",
                        AFFILIATE_REQUEST_MAP.get(receiver).getDisplayName()));
                addAffiliate(AFFILIATE_REQUEST_MAP.get(receiver), receiver);
            } else {
                AFFILIATE_REQUEST_MAP.get(receiver).sendRawMessage(String.format("%s denied your invitation to be affiliates.",
                        receiver.getDisplayName()));
            }
            AFFILIATE_REQUEST_MAP.remove(receiver);
        } else {
            receiver.sendRawMessage("No pending affiliate requests at the moment.");
        }
    }

    /**
     * Handles a breakup of affiliated Players.
     * @param breaker the Player who issued a breakup.
     * @param loser the Player who was affiliated with the breaker.
     */
    public void handleAffiliationBreak(final Player breaker, final Player loser) {
        if (AFFILIATE_PERSISTENCE_MAP.containsKey(breaker.getUniqueId())) {
            if (AFFILIATE_PERSISTENCE_MAP.get(breaker.getUniqueId()).contains(loser.getUniqueId())) {
                removeAffiliate(breaker, loser);
                breaker.sendRawMessage(String.format("You successful canceled the affiliation with %s.", loser.getDisplayName()));
                //TODO null checks player online checks
                loser.sendRawMessage(String.format("Your affiliation with %s has been canceled.", breaker.getDisplayName()));
            } else {
                breaker.sendRawMessage(String.format("You dont have an affiliation with %s.", loser.getDisplayName()));
            }
        } else {
            breaker.sendRawMessage("You dont have any affiliations to break up.");
        }
    }

    /**
     * Adds the new affiliation between the to player to the database and cached in memory persistence map
     * @param player the UUID of the requester.
     * @param affiliate the responder on the invite.
     */
    private void addAffiliate(final UUID player, final UUID affiliate) {
        ContextProvider.AFFILIATE_REPOSITORY.save(new Affiliate(player, affiliate));
        AFFILIATE_PERSISTENCE_MAP.get(player).add(affiliate); // cache
        AFFILIATE_PERSISTENCE_MAP.get(affiliate).add(player); // cache
    }

    /**
     * Propagate to addAffiliate(final UUID player, final UUID affiliate)
     * @param player the UUID of the requester.
     * @param affiliate the responder on the invite.
     */
    private void addAffiliate(final Player player, final Player affiliate) {
        this.addAffiliate(player.getUniqueId(), affiliate.getUniqueId());
    }

    /**
     * Removes a affiliate relation :-(.
     * Clears the affiliated relation in the n memory persistence cached map and in the database,
     * If the affiliation is present in the database.
     * @param player the player who stopped the affiliation.
     * @param affiliate the poor guy who lost a friend.
     */
    private void removeAffiliate(final UUID player, final UUID affiliate) {
        ContextProvider.AFFILIATE_REPOSITORY.findByAffiliates(player, affiliate).ifPresent(af -> {
            ContextProvider.AFFILIATE_REPOSITORY.delete(af);
            AFFILIATE_PERSISTENCE_MAP.get(player).remove(affiliate);
            AFFILIATE_PERSISTENCE_MAP.get(affiliate).remove(player);
        });
    }

    /**
     * Propagate to removeAffiliate(final UUID player, final UUID affiliate)
     * @param player the Player who stopped the affiliation.
     * @param affiliate the Player who was affiliated to player.
     */
    private void removeAffiliate(final Player player, final Player affiliate) {
        this.removeAffiliate(player.getUniqueId(), affiliate.getUniqueId());
    }

    /**
     * Maps the names of the affiliated from player.
     * @param player the Player who wish to get the names of his affiliated.
     * @return a Mapped Set of Strings representing player his affiliates names.
     */
    public Set<String> getPlayerAffiliatedNames(final UUID player) {
        return AFFILIATE_PERSISTENCE_MAP.get(player)
                .stream()
                .map(p -> Bukkit.getServer()
                        .getOfflinePlayer(p)
                        .getName())
                .collect(Collectors.toSet());
    }

    /**
     * Propagate to getPlayerAffiliatedNames(final UUID player)
     * @param player the Player who wish to get the names of his affiliated.
     * @return a Mapped Set of Strings representing player his affiliates names.
     */
    public Set<String> getPlayerAffiliatedNames(final Player player) {
        return this.getPlayerAffiliatedNames(player.getUniqueId());
    }

    public void glowFriendlyTroops(Player player, Soldier soldier) {
        if (soldier.hasDivision()) {
                soldier.getPresentDivision().getDivisionList().forEach(s ->
                        GlowHandler.handle().enableGlow(player, s.getAsBukkitEntity(), GlowColor.ALLY_LEGIO));
        } else {
            GlowHandler.handle().enableGlow(player, soldier.getAsBukkitEntity(), GlowColor.ALLY_LEGIO);
        }
    }
}
