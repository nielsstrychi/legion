package com.nielsstrychi.Handlers;

import com.nielsstrychi.Data.Division;
import com.nielsstrychi.Entities.Soldiers.Soldier;
import com.nielsstrychi.Packets.GlowColor;
import com.nielsstrychi.Util.Converter;
import net.minecraft.server.v1_13_R2.EntityLiving;
import org.bukkit.craftbukkit.v1_13_R2.entity.CraftLivingEntity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;

import java.util.*;
import java.util.stream.Collectors;

public class TargetHandler {

    private final Map<Soldier, LivingEntity> TARGET_MAP = new HashMap<>();
    private final Map<Division, List<?>> DIVISION_TARGET_MAP = new HashMap<>();

    /**
     * Singleton instance of this class.
     */
    private static TargetHandler INSTANCE;

    /**
     * Constructor
     */
    private TargetHandler() {
    }

    /**
     * Initializes the singleton if not yet created and returns it.
     * Else the already existing singleton instance is returned.
     * @return the instance of the singleton.
     */
    public static TargetHandler handle() {
        return INSTANCE == null ? INSTANCE = new TargetHandler() : INSTANCE;
    }


    public void start(final Soldier entity, final LivingEntity target) { //TODO test probably unstable
        Optional<Division> division = entity.getDivision();
        if (division.isPresent()) {
            if (target == null) {
                if (entity.isCommander()) {
                    List<?> removeValues = DIVISION_TARGET_MAP.get(division.get());
                    if (removeValues != null) removeValues.forEach(e -> GlowHandler.handle().disableGlow( entity.getPlayer().getPlayer(), (LivingEntity) e));
                    DIVISION_TARGET_MAP.remove(division.get());
                }
            } else {
                if ((((CraftLivingEntity) target).getHandle() instanceof Soldier)) {
                    Soldier targetedSoldier = (Soldier) ((((CraftLivingEntity) target).getHandle()));
                    Optional<Division> targetedDivision = targetedSoldier.getDivision();
                    if (targetedDivision.isPresent()) {
                        List<?> oldValues = DIVISION_TARGET_MAP.put(division.get(), targetedDivision.get().getDivisionList());
                        if (oldValues != null) {
                            oldValues.stream()
                                    .filter(Objects::nonNull)
                                    .forEach(e -> GlowHandler.handle().enableGlow(
                                            entity.getPlayer().getPlayer(),
                                            Converter.convert((EntityLiving) e),
                                            GlowColor.ENEMY_LEGIO_ATK));
                        }
                        targetedDivision.get()
                                .getDivisionList()
                                .stream()
                                .filter(Objects::nonNull)
                                .forEach(e -> GlowHandler.handle().enableGlow(
                                entity.getPlayer().getPlayer(),
                                Converter.convert((EntityLiving) e),
                                GlowColor.ENEMY_LEGIO_ATK));
                    }
                }
                List<LivingEntity> addList = DIVISION_TARGET_MAP
                        .computeIfAbsent(division.get(), k -> new ArrayList<>())
                        .stream()
                        .filter(Objects::nonNull)
                        .map(en -> Converter.convert((EntityLiving) en))
                        .collect(Collectors.toList());
                addList.add(target);
                addList.forEach(t -> GlowHandler.handle().enableGlow(entity.getPlayer().getPlayer(), t, GlowColor.ENEMY_LEGIO_ATK));
            }
        } else {
            if (entity.getPlayer().isOnline()) {
                if (target != null) {
                    GlowHandler.handle().enableGlow(entity.getPlayer().getPlayer(), target, GlowColor.ENEMY_LEGIO_ATK);
                }
                GlowHandler.handle().disableGlow(entity.getPlayer().getPlayer(), TARGET_MAP.put(entity, target)); // Put returns the previous value. So the old target stops glowing.
            }
        }
       }

    public void glowTargetTroops(Player player, Soldier soldier) {
        if (soldier.hasDivision()) {
            soldier.getPresentDivision().getDivisionList().forEach(s ->
                    GlowHandler.handle().enableGlow(player, s.getAsBukkitEntity(), GlowColor.ENEMY_LEGIO));
        } else {
            GlowHandler.handle().enableGlow(player, soldier.getAsBukkitEntity(), GlowColor.ENEMY_LEGIO);
        }
    }
}
