package com.nielsstrychi.Selectors;

import com.nielsstrychi.Entities.Soldiers.Soldier;
import com.nielsstrychi.Handlers.GlowHandler;
import com.nielsstrychi.Packets.GlowColor;
import org.bukkit.Location;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;

import java.util.*;


public class MultiSelector extends Selector{
    private Set<Soldier> mulitList;

    protected MultiSelector(){}

    public MultiSelector(Selector selector, Soldier soldier) {
        this.mulitList = new HashSet<>();
        this.player = selector.getPlayer();
        this.soldier = selector.getSoldier();
        mulitList.add(this.soldier);
        mulitList.add(soldier);
        startGlowingSelection(GlowColor.PLAYER_LEGIO_SLC);
    }

    public Soldier getBannerCarrier() {
        return mulitList.stream().filter(e -> e.isCommander()).findFirst().get();
    }

    public boolean containsBannerCarrier() {
        return mulitList.stream().anyMatch(e -> e.isCommander());
    }

    public boolean isListEmpty() {return mulitList.isEmpty();}

    public int count() {
        return this.mulitList.size();
    }

    @Override
    public boolean containsSoldier(Soldier soldier) {
        return mulitList.contains(soldier);
    }

    @Override
    public void startGlowingSelection(GlowColor glowColor) {
        mulitList.forEach(
                s -> GlowHandler.handle().enableGlow(player, s.getAsBukkitEntity(), glowColor));
    }

    @Override
    public void moveSoldier(Location location) { //TODO check if dead en list clearen van deads and nulls
        mulitList.forEach(
                s -> s.moveTo(location, 1.75F));
    }

    @Override
    public void toAttackEntity(LivingEntity entity) {
        mulitList.forEach(
                s -> s.startAttackEntity(entity));
    }

    public void deleteSelection() {
        this.stopGlowingSelection();
    }

    public void stopGlowingSelection() {
        mulitList.forEach(
                s -> GlowHandler.handle().disableGlow(player, s.getAsBukkitEntity()));
    }

    @Override
    public void teleportSoldier(Location location) {
        soldier.getBukkitEntity().teleport(location);
    }

    public void addSoldierMulti(Soldier soldier) {
        mulitList.add(soldier);
    }

    public boolean removeSoldierMulti(Soldier soldier) {
        return mulitList.remove(soldier);
    }

    public boolean isSingleSoldierList() {
        return mulitList.size()==1;
    }

    public Soldier getLastSoldier() {
        return (Soldier) mulitList.toArray()[0];
    }

    public void follow(Player player) {
        this.mulitList.forEach(s -> s.setFollowingEntity(player));
    }

    public void unFollow(Player player) {
        this.mulitList.forEach(soldier -> soldier.stopFollowing(player));
    }

}
