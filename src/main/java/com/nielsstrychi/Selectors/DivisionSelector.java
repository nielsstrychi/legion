package com.nielsstrychi.Selectors;

import com.nielsstrychi.Entities.Soldiers.Soldier;
import com.nielsstrychi.Data.Division;
import com.nielsstrychi.Handlers.GlowHandler;
import com.nielsstrychi.Packets.GlowColor;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_13_R2.entity.CraftLivingEntity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;

public class DivisionSelector extends MultiSelector {

    private final Division division;

    public DivisionSelector(Selector selector, Division division) {
        this.player = selector.getPlayer();
        this.soldier = selector.getSoldier();
        this.division = division;
        selector.deleteSelection();
        startGlowingSelection(GlowColor.PLAYER_LEGIO_SLC);
    }

    @Override
    public int count() {
        return this.division.soldierCount();
    }

    @Override
    public boolean isListEmpty() {return division.getDivisionList().isEmpty();}

    @Override
    public boolean containsSoldier(Soldier soldier) {
        return division.getDivisionList().contains(soldier);
    }

    @Override
    public void moveSoldier(Location targetLocation) {
        if (division.isAttackingTargetDivision()) {
            division.stopAttackingTargetDivision();
        }
        division.getDivisionList().forEach(
                s -> s.moveTo(calcOffsetLocation(targetLocation,s),1.75F));
    }

    @Override
    public void toAttackEntity(LivingEntity entity) {
        if (((CraftLivingEntity) entity).getHandle() instanceof Soldier && ((Soldier) ((CraftLivingEntity) entity).getHandle()).hasDivision()) {
            Division enemyDivision = ((Soldier) ((CraftLivingEntity) entity).getHandle()).getPresentDivision();
            division.attack(enemyDivision);
        } else {
            division.getDivisionList().forEach(
                    s -> s.startAttackEntity(entity));
        }
    }

    public void deleteSelection() {
        this.stopGlowingSelection();
    }

    @Override
    public void startGlowingSelection(GlowColor glowColor) {
        division.getDivisionList().forEach(
                s -> GlowHandler.handle().enableGlow(player, s.getAsBukkitEntity(), glowColor));
    }

    @Override
    public void stopGlowingSelection() {
        division.getDivisionList().forEach(
                s -> GlowHandler.handle().disableGlow(player, s.getAsBukkitEntity()));
    }

    private Location calcOffsetLocation(Location targetLocation, Soldier soldier) {
        return new Location(targetLocation.getWorld(),
                targetLocation.getX()+soldier.getOffset().getX(),
                targetLocation.getY(),
                targetLocation.getZ()+soldier.getOffset().getZ());
    }

    public Division getDivision() {
        return division;
    }

    public void follow(Player player) {
        this.division.getDivisionList().forEach(s -> s.setFollowingEntity(player));
    }

    public void unFollow(Player player) {
        this.division.getDivisionList().forEach(soldier -> soldier.stopFollowing(player));
    }
}
