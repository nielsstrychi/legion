package com.nielsstrychi.Selectors;

import com.nielsstrychi.Entities.Soldiers.Soldier;
import com.nielsstrychi.Data.Division;
import com.nielsstrychi.Handlers.GlowHandler;
import com.nielsstrychi.Tiers;
import com.nielsstrychi.Packets.GlowColor;
import com.nielsstrychi.Providers.ContextProvider;
import com.nielsstrychi.Util.If;
import com.nielsstrychi.Util.NotNull;
import net.minecraft.server.v1_13_R2.EnumItemSlot;
import net.minecraft.server.v1_13_R2.ItemArmor;
import net.minecraft.server.v1_13_R2.Items;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_13_R2.inventory.CraftItemStack;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.List;
import java.util.Objects;

public class Selector {
    protected Player player;
    protected Soldier soldier;
    private boolean selectedTwice;

    protected Selector(){}

    public Selector(Player player, Soldier soldier) {
        this.player = player;
        this.soldier = soldier;
        startGlowingSelection(GlowColor.PLAYER_LEGIO_SLC);
    }

    @Override
    protected void finalize() throws Throwable {
        deleteSelection();
        super.finalize();
    }

    protected void startGlowingSelection(GlowColor glowColor) {
        GlowHandler.handle().enableGlow(player, soldier.getAsBukkitEntity(), glowColor);
    }

    public void deleteSelection() {
        stopGlowingSelection();
    }

    protected void stopGlowingSelection() {
        GlowHandler.handle().disableGlow(player, soldier.getAsBukkitEntity());
    }

    public Player getPlayer() {
        return player;
    }

    public Soldier getSoldier() {
        return soldier;
    }

    public boolean isSelectedTwice() {
        return selectedTwice;
    }

    public Selector setSelectedTwice(boolean selectedTwice) {
        this.selectedTwice = selectedTwice;
        return this;
    }

    public boolean canEdit(Player checkplayer) {
        return checkplayer==player;
    }

    public boolean canCommand(Player checkplayer) {
        return checkplayer==player;
    }

    public void moveSoldier(Location location) {
        soldier.moveTo(location, 1.75F);
    }


    public void toAttackEntity(LivingEntity entity) {
        soldier.startAttackEntity(entity);
    }

    public boolean containsSoldier(Soldier soldier) {
        return soldier.equals(this.soldier);
    }

    public void teleportSoldier(Location location) {
        soldier.getBukkitEntity().teleport(location);
    }

    public ItemStack setItem(ItemStack item) {
        ItemStack returnItem = CraftItemStack.asCraftMirror(new net.minecraft.server.v1_13_R2.ItemStack(Items.AIR));
        if (If.isEquipable(item)) {
            switch((((ItemArmor) CraftItemStack.asNMSCopy(item).getItem()).b()).getSlotName()) {
                case "head": if (!soldier.getEquipment(EnumItemSlot.HEAD).isEmpty()) {
                    returnItem = CraftItemStack.asCraftMirror(soldier.getEquipment(EnumItemSlot.HEAD));
                }   soldier.setSlot(EnumItemSlot.HEAD, CraftItemStack.asNMSCopy(item));
                 break;
                case "chest": if (!soldier.getEquipment(EnumItemSlot.CHEST).isEmpty()) {
                    returnItem = CraftItemStack.asCraftMirror(soldier.getEquipment(EnumItemSlot.CHEST));
                }   soldier.setSlot(EnumItemSlot.CHEST, CraftItemStack.asNMSCopy(item));
                break;
                case "legs": if (!soldier.getEquipment(EnumItemSlot.LEGS).isEmpty()) {
                    returnItem = CraftItemStack.asCraftMirror(soldier.getEquipment(EnumItemSlot.LEGS));
                }   soldier.setSlot(EnumItemSlot.LEGS, CraftItemStack.asNMSCopy(item));
                break;
                case "feet": if (!soldier.getEquipment(EnumItemSlot.FEET).isEmpty()) {
                    returnItem = CraftItemStack.asCraftMirror(soldier.getEquipment(EnumItemSlot.FEET));
                }   soldier.setSlot(EnumItemSlot.FEET, CraftItemStack.asNMSCopy(item));
                break;
            }
        }
        else if(this.isValidBanner(item)){
            if (!this.isValidBanner(CraftItemStack.asCraftMirror(soldier.getEquipment(EnumItemSlot.HEAD)))) {
                soldier.setPlayer(player); //TODO testing

                if(player.equals(soldier.getPlayer())) {
                   List<Tiers> legios = ContextProvider.DIVISION_REPOSITORY
                            .findPlayerTiers(player);
                   if (legios.size() < 10) { //speler formaties niet meer is dan 9 of MAX properties

                       Tiers legio = getTier(item);
                       if (!legios.contains(legio)) {
                           if (!soldier.getEquipment(EnumItemSlot.HEAD).isEmpty()) {
                               returnItem = CraftItemStack.asCraftMirror(soldier.getEquipment(EnumItemSlot.HEAD));
                           }   soldier.setSlot(EnumItemSlot.HEAD, CraftItemStack.asNMSCopy(item));
                           Division division = createFormation(player, soldier);
                           division.setTier(legio);
                           ContextProvider.DIVISION_REPOSITORY.save(division);
                           soldier.update();
                           System.out.println("formatie gemaakt");
                       } else {
                           System.out.println("divsion number " + legio + " already exist in your legion");
                       }

                    } else {
                        System.out.println("u heeft al max aantal formaties");
                    }
                } else {
                    System.out.println("only the owner of this soldier can create a formation");
                }
            }
        }
    else if (soldier.getEquipment(EnumItemSlot.MAINHAND).isEmpty()) {soldier.setSlot(EnumItemSlot.MAINHAND, CraftItemStack.asNMSCopy(item));}
    else if (soldier.getEquipment(EnumItemSlot.OFFHAND).isEmpty()) {soldier.setSlot(EnumItemSlot.OFFHAND, CraftItemStack.asNMSCopy(item));}
    else {
            returnItem = CraftItemStack.asCraftMirror(soldier.getEquipment(EnumItemSlot.OFFHAND));
            soldier.setSlot(EnumItemSlot.OFFHAND, CraftItemStack.asNMSCopy(item));
        }
    return returnItem;
    }

    public ItemStack getItem() {
        ItemStack stack = CraftItemStack.asCraftMirror(new net.minecraft.server.v1_13_R2.ItemStack(Items.AIR));
             if (!soldier.getEquipment(EnumItemSlot.OFFHAND).isEmpty()) {stack = CraftItemStack.asCraftMirror(soldier.getEquipment(EnumItemSlot.OFFHAND)); deleteItem(EnumItemSlot.OFFHAND);}
        else if (!soldier.getEquipment(EnumItemSlot.MAINHAND).isEmpty()) {stack = CraftItemStack.asCraftMirror(soldier.getEquipment(EnumItemSlot.MAINHAND)); deleteItem(EnumItemSlot.MAINHAND);}
        else if (!soldier.getEquipment(EnumItemSlot.HEAD).isEmpty()) {stack = CraftItemStack.asCraftMirror(soldier.getEquipment(EnumItemSlot.HEAD)); deleteItem(EnumItemSlot.HEAD);}
        else if (!soldier.getEquipment(EnumItemSlot.CHEST).isEmpty()) {stack = CraftItemStack.asCraftMirror(soldier.getEquipment(EnumItemSlot.CHEST)); deleteItem(EnumItemSlot.CHEST);}
        else if (!soldier.getEquipment(EnumItemSlot.LEGS).isEmpty()) {stack = CraftItemStack.asCraftMirror(soldier.getEquipment(EnumItemSlot.LEGS)); deleteItem(EnumItemSlot.LEGS);}
        else if (!soldier.getEquipment(EnumItemSlot.FEET).isEmpty()) {stack = CraftItemStack.asCraftMirror(soldier.getEquipment(EnumItemSlot.FEET)); deleteItem(EnumItemSlot.FEET);}
        return stack;
    }

    private void deleteItem(EnumItemSlot enumItemSlot) {
        soldier.setSlot(enumItemSlot, new net.minecraft.server.v1_13_R2.ItemStack(Items.AIR));
    }

    private boolean isValidBanner(ItemStack item) {
       return If.isBanner(item)
                && item.getItemMeta().hasDisplayName()
                && item.getItemMeta().getDisplayName().length() > 7
                && item.getItemMeta().getDisplayName().substring(0,7).equals(ChatColor.GOLD + "Legio");
    }

    private Tiers getTier(@NotNull final ItemStack item) {
        String name = Objects.requireNonNull(item.getItemMeta()).getDisplayName();
        switch (name.substring(8, name.indexOf(" ", 9))) {
            case "I": return Tiers.I;
            case "II": return Tiers.II;
            case "III": return Tiers.III;
            case "IV": return Tiers.IV;
            case "V": return Tiers.V;
            case "VI": return Tiers.VI;
            case "VII": return Tiers.VII;
            case "VIII": return Tiers.VIII;
            case "IX": return Tiers.IX;
        } throw new IllegalArgumentException("Tier not found.");
    }

    private Division createFormation(Player player, Soldier soldier) {
        return new Division(soldier,player);
    }

    public void follow(Player player) {
        this.getSoldier().setFollowingEntity(player);
    }

    public void unFollow(Player player) {
        this.getSoldier().stopFollowing(player);
    }
}
