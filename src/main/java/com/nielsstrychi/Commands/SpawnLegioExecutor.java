package com.nielsstrychi.Commands;

import com.nielsstrychi.Data.Division;
import com.nielsstrychi.Entities.Soldiers.EntityType;
import com.nielsstrychi.Entities.Soldiers.Soldier;
import com.nielsstrychi.Entities.Soldiers.SoldierSkeletonWither;
import com.nielsstrychi.Tiers;
import com.nielsstrychi.Providers.EntityProvider;
import net.minecraft.server.v1_13_R2.EnumItemSlot;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.craftbukkit.v1_13_R2.inventory.CraftItemStack;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class SpawnLegioExecutor implements Executor {

    public boolean onCommand(final CommandSender sender,
                             final Command command,
                             final String label,
                             final String[] split) {
        if (sender instanceof Player) {
            Player player = (Player) sender;


            SoldierSkeletonWither commander = EntityProvider.provide().spawnNewSoldier(EntityType.WITHER_SKELETON, new Location(player.getWorld(),player.getLocation().getX(),player.getLocation().getY(),player.getLocation().getZ()), player);
            commander.setSlot(EnumItemSlot.HEAD, CraftItemStack.asNMSCopy(new ItemStack(Material.BLACK_BANNER)));
            commander.setSlot(EnumItemSlot.MAINHAND, CraftItemStack.asNMSCopy(new ItemStack(Material.IRON_SWORD)));
            commander.setSlot(EnumItemSlot.OFFHAND, CraftItemStack.asNMSCopy(new ItemStack(Material.SHIELD)));
            commander.setSlot(EnumItemSlot.CHEST, CraftItemStack.asNMSCopy(new ItemStack(Material.IRON_CHESTPLATE)));
            commander.setSlot(EnumItemSlot.LEGS, CraftItemStack.asNMSCopy(new ItemStack(Material.IRON_LEGGINGS)));
            commander.setSlot(EnumItemSlot.FEET, CraftItemStack.asNMSCopy(new ItemStack(Material.IRON_BOOTS)));
            Division division = new Division(commander,player);
            division.setTier(Tiers.I);
            for (int i = 0, j = 0; i < 59 ; i++) {
                if (i%10==0) j++;
                SoldierSkeletonWither member = EntityProvider.provide().spawnNewSoldier(EntityType.WITHER_SKELETON, new Location(player.getWorld(),player.getLocation().getX()+i%10,player.getLocation().getY(),player.getLocation().getZ() + j), player);
                member.setSlot(EnumItemSlot.MAINHAND, CraftItemStack.asNMSCopy(new ItemStack(Material.IRON_SWORD)));
                member.setSlot(EnumItemSlot.OFFHAND, CraftItemStack.asNMSCopy(new ItemStack(Material.SHIELD)));
                member.setSlot(EnumItemSlot.HEAD, CraftItemStack.asNMSCopy(new ItemStack(Material.IRON_HELMET)));
                member.setSlot(EnumItemSlot.CHEST, CraftItemStack.asNMSCopy(new ItemStack(Material.IRON_CHESTPLATE)));
                member.setSlot(EnumItemSlot.LEGS, CraftItemStack.asNMSCopy(new ItemStack(Material.IRON_LEGGINGS)));
                member.setSlot(EnumItemSlot.FEET, CraftItemStack.asNMSCopy(new ItemStack(Material.IRON_BOOTS)));
                division.addSoldier(member);
            }
            division.update(true);
            division.getDivisionList().forEach(Soldier::update);

            return true;
        } else {
            return false;
        }
    }
}
