package com.nielsstrychi.Commands;

import com.nielsstrychi.Entities.Soldiers.EntityType;
import com.nielsstrychi.Entities.Soldiers.SoldierSkeletonWither;
import com.nielsstrychi.LegionPlugin;
import com.nielsstrychi.Packets.GlowColor;
import com.nielsstrychi.Packets.PacketGlowing;
import com.nielsstrychi.Packets.PacketParticles;
import com.nielsstrychi.Providers.EntityProvider;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class DebugExecutor implements Executor {

    public boolean onCommand(final CommandSender sender,
                             final Command command,
                             final String label,
                             final String[] split) {
        if (sender instanceof Player) {
            Player player = (Player) sender;

            PacketGlowing glowProtocol = new PacketGlowing();
            new PacketParticles().setParticles(player);
            SoldierSkeletonWither spawnedEntity2 = EntityProvider.provide().spawnNewSoldier(EntityType.WITHER_SKELETON, new Location(player.getWorld(),player.getLocation().getX(),player.getLocation().getY(),player.getLocation().getZ()), player);
            return true;
        } else {
            return false;
        }
    }
}
