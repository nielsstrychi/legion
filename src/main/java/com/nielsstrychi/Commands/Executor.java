package com.nielsstrychi.Commands;

import com.nielsstrychi.Util.NotNull;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

/**
 * Interface to abstract commonly used methods and easy register commands with there executors.
 *
 * @author Nielsstrychi
 */
public interface Executor extends CommandExecutor {

    /**
     * Checks if the executor of the command is a ingame player.
     *
     * @param sender that must be a player to return true.
     * @return true if the executor is a player else false.
     */
    default boolean isSenderPlayer(@NotNull final CommandSender sender) {
        return (sender instanceof Player);
    }

    /**
     * Retrieves the item that the player is holding at this moment in his main hand.
     *
     * @param player to get the item in his hand of.
     * @return the Item he is holding or an ItemStack with the property AIR if they dont hold anything.
     */
    default ItemStack getCurrentItemInHand(@NotNull final Player player) {
        return player.getInventory().getItemInMainHand();
    }
}
