package com.nielsstrychi.Commands;

import com.nielsstrychi.Data.Division;
import com.nielsstrychi.Providers.ContextProvider;
import com.nielsstrychi.Selectors.DivisionSelector;
import com.nielsstrychi.Selectors.Selector;
import com.nielsstrychi.Util.DivisionFormation;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.Optional;
import java.util.stream.Stream;

/**
 * View player selection
 */
public class FormationExecutor implements Executor {

    public boolean onCommand(final CommandSender sender,
                             final Command command,
                             final String label,
                             final String[] split) {
        if (sender instanceof Player) {
            Player player = (Player) sender;
            if (split.length > 0) {
                Optional<DivisionFormation> formation = Stream.of(DivisionFormation.values())
                        .filter(f -> (f.name().contains("_")
                                ? f.name().substring(f.name().indexOf('_') + 1)
                                : f.name())
                                .toLowerCase()
                                .equals(split[0].toLowerCase()))
                        .findAny();
                if (formation.isPresent()) {
                    Selector selector = ContextProvider.SELECTION_HANDLER.getPlayerSelector().get(player.getName());
                    if (selector == null) {
                        player.sendRawMessage("No legio is selected.");
                    } else if (selector instanceof DivisionSelector) {
                        Division division = ((DivisionSelector) selector).getDivision();
                        division.setFormation(formation.get());
                        player.sendRawMessage(String.format("Formation of %s set to %s.", division.getName(), split[0]));
                    }
                    return true;
                } else {
                    player.sendRawMessage(String.format("The formation %s was not recognized.", split[0].toLowerCase()));
                    return true;
                }
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
}
