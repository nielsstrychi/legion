package com.nielsstrychi.Commands.Executors;

import com.nielsstrychi.Commands.Commands;
import com.nielsstrychi.Commands.Executor;
import com.nielsstrychi.LegionPlugin;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.PluginCommand;

import java.util.Optional;
import java.util.stream.Stream;

public class ExecutorHandler implements Executor {

    public static void registerCommands(LegionPlugin plugin) {
        ExecutorHandler handler = new ExecutorHandler();
        plugin.getCommand(Commands.PLUGIN_PREFIX).setExecutor(handler);
        plugin.getCommand(Commands.PLUGIN_PREFIX_SHORT).setExecutor(handler);
        Stream.of(Commands.values()).forEach(s -> {
                PluginCommand command = plugin.getCommand(s.getCommand());
                if (command != null) {
                    command.setExecutor(s.getExecutor());
                } else {
                    System.out.println((String.format(
                            "There is no command found in the 'plugin.yml' for command: '%s', please add it to the 'plugin.yml'.",
                            s.getCommand())));
                }
        });
    }

    @Override
    public boolean onCommand(final CommandSender sender,
                             final Command command,
                             final String label,
                             final String[] args) {
        if (args.length > 0) {
            Optional<Commands> subCommand = Commands.getEnumValueByCommand(args[0]);
            if (subCommand.isPresent()) {
                String[] filteredArgs = new String[args.length - 1];
                for (int i = 0; i < args.length; i++) {
                    if (i == 0) continue;
                    filteredArgs[i - 1] = args[i];
                }
                return subCommand.get().getExecutor().onCommand(sender, command, label, filteredArgs);
            }
        }
        return false;
    }
}
