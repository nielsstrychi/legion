package com.nielsstrychi.Commands.Executors;


import com.nielsstrychi.Commands.Executor;
import com.nielsstrychi.Tiers;
import com.nielsstrychi.Providers.ContextProvider;
import com.nielsstrychi.Util.If;
import com.nielsstrychi.Util.NotNull;
import com.nielsstrychi.Util.Nullable;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.List;
import java.util.Objects;

/**
 * Casts a banner to a metadata manipulated object.
 * That is specified to apply on a commander and so create division.
 *
 * @author Nielstrychi
 */
public class CreateDivisionBannerCommandExecutor implements Executor {

    /**
     * Main method of this executor that is responsible of creating a metadata manipulated object is issued correct.
     *
     * @param sender the executor of the command.
     * @param command the issued command.
     * @param label the alias of the command used.
     * @param split the words that are provided after the command.
     * @return true if the execution has done logic or false if they issued the command wrong.
     *         The help screen for this command is presented to the player then.
     */
    @Override
    public boolean onCommand(@NotNull final CommandSender sender,
                             @NotNull final Command command,
                             @NotNull final String label,
                             @NotNull final String[] split) {
        if (!isSenderPlayer(sender)) { return false; }
        Player player = (Player) sender;
        ItemStack item = getCurrentItemInHand(player);
        if (If.isBanner(item)) {
            List<Tiers> tiers = getPlayerTiers(player);
            if (isNewDivisionAllowed(tiers)) {
                Tiers tier = getCurrentTier(tiers);
                if (tier == null) {
                    player.sendRawMessage("There are no legio tiers more available for you.");
                    return true;
                }
                String name = getLegioName(split, tier);
                editBannerItemMeta(item, tier, name);
                player.sendRawMessage(String.format("Banner for legio %s created, Apply on your commander soldier to create you division.", name));
                return true;
            } else {
                player.sendRawMessage("You have the maximum amount of divisions already.");
                return true;
            }
        } else {
            player.sendRawMessage("You must hold a banner that represents the division.");
            return true;
        }
    }

    /**
     * Generates a name if no name has been chosen else it concatenates the chosen it to a full name.
     *
     * @param split hold the chosen words to combine a name if non are given a name is generates
     * @param tier of the legio that will define a name if it is generated.
     * @return a generated name or a chosen name depended of the input of the player.
     */
    private String getLegioName(@NotNull final String[] split, @NotNull final Tiers tier) {
        return split.length >= 1
                ? getOwnLegioName(split)
                : generateLegioName(tier);
    }

    /**
     * Cascades the given words and validates to combine a legio name chosen by the player.
     *
     * @param split a array of words that combined will craft the legio his name.
     * @return a string that is the combination of the given words.
     */
    private String getOwnLegioName(@NotNull final String[] split) {
        StringBuilder builder = new StringBuilder();
            for (int i = 0; i < split.length; i++) {
                builder.append(split[i]);
                if (split.length != i - 1) {
                    builder.append(" ");
                }
            }
            return builder.toString();
    }

    /**
     * Generates a legio name is none is specified by the user.
     *
     * @param legio tier that should generate an historical legio name with there original romain number.
     * @return a random historical legio name associated with there requested tier, only the name returns.
     */
    private String generateLegioName(@NotNull final Tiers legio) {
        return legio.getRandomName();
    }

    /**
     * Edit the metadata of the given banner object.
     * Sets the color, tier and name of the legio on the banner item.
     *
     * @param banner that the metadata will be modified.
     * @param tier that will be used for the creation of this new division if applied on a commander.
     * @param name the name that wil be given to the division.
     */
    private void editBannerItemMeta(@NotNull final ItemStack banner,
                                    @NotNull final Tiers tier,
                                    @NotNull final String name) {
        ItemMeta meta = banner.getItemMeta();
        Objects.requireNonNull(meta).setDisplayName(
                String.format("%sLegio %s %s%s",
                        ChatColor.GOLD,
                        tier.name(),
                        name,
                        ChatColor.RESET));
        banner.setItemMeta(meta);
    }

    /**
     * Retrieves a available legio tier that can be used to create a new legio.
     * May return null if all tier are already inuse for this player.
     *
     * @param tiers a list of the current legio tiers the player already have in use.
     * @return a legio tier that is not yet inuse by this player or null if there all tiers are inuse.
     */
    @Nullable
    private Tiers getCurrentTier(@NotNull final List<Tiers> tiers) {
        for (int i = 0; Tiers.values().length > i ; i++) {
            if (!tiers.contains(Tiers.values()[i])) {
                return Tiers.values()[i];
            }
        } return null;
    }

    /**
     * Checks if the player has not yet reach the maximum capacity of legios.
     *
     * @param tiers a list of legio tiers the player already owns.
     * @return true if they may create an other or false if they already have max capacity.
     */
    private boolean isNewDivisionAllowed(List<Tiers> tiers) {
        return tiers.size() < 10;
    }


    /**
     * Retrieves the player his legios tiers from the database.
     *
     * @param player the player who's legios get retrieved.
     * @return a list of legio tiers in use by the player.
     */
    private List<Tiers> getPlayerTiers(Player player) {
        return ContextProvider.DIVISION_REPOSITORY.findPlayerTiers(player);
    }
}
