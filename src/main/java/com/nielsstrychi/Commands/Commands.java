package com.nielsstrychi.Commands;

import com.nielsstrychi.Commands.Executors.CreateDivisionBannerCommandExecutor;

import java.util.Optional;
import java.util.stream.Stream;

public enum Commands {

    INFO(new InfoExecutor(), "info", "i"),
    POSITION(new TestExecutor(), "position", "pos"),
    SELECTED(new SelectedExecutor(), "selected", "sl"),
    FORMATION(new FormationExecutor(), "formation", "fm"),
    FOLLOW(new FollowExecutor(), "follow", "fw"),
    CREATE(new CreateDivisionBannerCommandExecutor(), "create", "c"),
    AFFILIATE(new AffiliateExecutor(), "affiliate", "af"),

    // ADMIN COMMANDS
    TEST(new TestExecutor(), "test", "t"),
    CLEAR(new TestExecutor(), "clear", "cl"),
    LOAD(new TestExecutor(), "load", "l"),
    SAVE(new DebugExecutor(), "save", "s"),
    DEBUG(new DebugExecutor(), "debug", "db"),
    SPAWN(new SpawnLegioExecutor(), "spawn", "sp");

    public static final String PLUGIN_PREFIX = "legion";
    public static final String PLUGIN_PREFIX_SHORT = "lg";

    private final Executor executor;
    private final String command;
    private final String commandShort;

    Commands(final Executor executor, final String command, final String commandShort) {
        this.executor = executor;
        this.command = command;
        this.commandShort = commandShort;
    }

    public Executor getExecutor() {
        return this.executor;
    }

    public String getCommand() {
        return PLUGIN_PREFIX + " " + this.command;
    }

    public String getCommandShort() {
        return PLUGIN_PREFIX_SHORT + " " + this.commandShort;
    }

    public String getCommandInitialValue() {
        return this.command;
    }

    public String getCommandInitialValueShort() {
        return this.commandShort;
    }

    public static Optional<Commands> getEnumValueByCommand(String command) {
        return Stream.of(Commands.values()).filter(c -> (c.getCommandInitialValue().equals(command)) || (c.getCommandInitialValueShort().equals(command))).findFirst();
    }
}
