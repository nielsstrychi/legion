package com.nielsstrychi.Commands;

import com.nielsstrychi.Data.Division;
import com.nielsstrychi.Providers.ContextProvider;
import com.nielsstrychi.Util.Config;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

public class InfoExecutor implements Executor {

    public boolean onCommand(final CommandSender sender,
                             final Command command,
                             final String label,
                             final String[] split) {
        if (sender instanceof Player) {
            Player player = (Player) sender;

            if (0 == split.length) {
                List<Division> divisions = ContextProvider.DIVISION_REPOSITORY.findAllPlayerDivisions(player);
                divisions.stream()
                        .sorted(Comparator.comparing(Division::getTier)).filter(Objects::nonNull)
                        .forEach(d -> player.sendRawMessage(d.getFullName() + ": "
                                    + d.soldierCount() + "/" + Config.MAX
                                    + " Formation: " + d.getFormation().name()));
                return true;
            } else {
                Optional<Division> division = ContextProvider.DIVISION_REPOSITORY.findByPlayerTierOrName(player, split[0]);
                if (division.isPresent()) {
                    return true;
                }
                return false;
            }
        } else {
            return false;
        }
    }
}
