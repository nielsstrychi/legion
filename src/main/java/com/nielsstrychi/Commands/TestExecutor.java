package com.nielsstrychi.Commands;

import com.nielsstrychi.LegionPlugin;
import com.nielsstrychi.Providers.ContextProvider;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class TestExecutor implements Executor {

    public boolean onCommand(final CommandSender sender,
                             final Command command,
                             final String label,
                             final String[] split) {
        if (sender instanceof Player) {
            Player player = (Player) sender;
            System.out.println("test");
            ContextProvider.SELECTION_HANDLER.getPlayerSelector().get(player.getName()).getSoldier().moralHit();
            return true;
        } else {
            return false;
        }
    }
}
