package com.nielsstrychi.Commands;

import com.nielsstrychi.Data.Division;
import com.nielsstrychi.Providers.ContextProvider;
import com.nielsstrychi.Selectors.DivisionSelector;
import com.nielsstrychi.Selectors.MultiSelector;
import com.nielsstrychi.Selectors.Selector;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * View player selection
 */
public class SelectedExecutor implements Executor {

    public boolean onCommand(final CommandSender sender,
                             final Command command,
                             final String label,
                             final String[] split) {
        if (sender instanceof Player) {
            Player player = (Player) sender;
            Selector selector = ContextProvider.SELECTION_HANDLER.getPlayerSelector().get(player.getName());
            if (selector == null) {
                player.sendRawMessage("No entity is selected.");
            } else if (!(selector instanceof MultiSelector)) {
                player.sendRawMessage(String.format("Selected one %s soldier.", selector.getSoldier().getAsBukkitEntity().getType().name()));
            } else if (selector instanceof DivisionSelector) {
                Division division = ((DivisionSelector) selector).getDivision();
                player.sendRawMessage(String.format("Selected %s troops from division %s.", division.soldierCount(), division.getName()));
            } else {
                player.sendRawMessage(String.format("Selected %s soldiers.", ((MultiSelector) selector).count()));
            }
            return true;
        } else {
            return false;
        }
    }
}
