package com.nielsstrychi.Commands;

import com.nielsstrychi.Handlers.AffiliateHandler;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class AffiliateExecutor implements Executor {

    public boolean onCommand(final CommandSender sender,
                             final Command command,
                             final String label,
                             final String[] split) {
        if (sender instanceof Player && split.length > 0) {
            Player player = (Player) sender;
            switch (split[0]) {
                case "accept":
                case "a": return affiliateResponseAccept(player);
                case "denied":
                case "d": return affiliateResponseDenied(player);
                case "request":
                case "req": return (split.length > 1) && affiliateRequest(player, split);
                case "break":
                case "b": return (split.length > 1) && breakAffiliation(player, split);
                case "view":
                case "v": return affiliateView(player);
                default: player.sendRawMessage(String.format("/legion affiliate %s is not a recognised command.", split[0])); return true;
            }
        } else {
            return false;
        }
    }

    private boolean affiliateRequest(final Player requester, final String[] split) {

        Player receiver = Bukkit.getServer().getPlayer(split[1]);
        if (receiver != null) {
            AffiliateHandler.handle().handleNewAffiliateRequest(requester, receiver);
             } else {
            requester.sendRawMessage(String.format("Can not find a player with name %s.", split[1]));
        }
        return true;
    }

    private boolean affiliateResponseAccept(final Player receiver) {
        AffiliateHandler.handle().handleAffiliateResponse(receiver, true);
        return true;
    }

    private boolean affiliateResponseDenied(final Player receiver) {
        AffiliateHandler.handle().handleAffiliateResponse(receiver, false);
        return true;
    }

    private boolean affiliateView(final Player playerOne) {
        StringBuilder builder = new StringBuilder();
        AffiliateHandler.handle()
                .getPlayerAffiliatedNames(playerOne)
                .forEach(name -> {
                    if (builder.length() != 0)
                        builder.append(", ");
                        builder.append(name);
                });
        playerOne.sendRawMessage(builder.toString());
        return true;
    }

    private boolean breakAffiliation(final Player player, final String[] split) {
        Player receiver = Bukkit.getServer().getPlayer(split[1]);
        if (receiver != null) {
            AffiliateHandler.handle().handleAffiliationBreak(player, receiver);
        } else {
            player.sendRawMessage(String.format("Can not find a player with name %s.", split[1]));
        }
        return true;
    }
}
