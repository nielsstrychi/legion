package com.nielsstrychi.Commands;

import com.nielsstrychi.Data.Division;
import com.nielsstrychi.Providers.ContextProvider;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.List;

public class PositionExecutor implements Executor {

    public boolean onCommand(final CommandSender sender,
                             final Command command,
                             final String label,
                             final String[] split) {
        if (sender instanceof Player) {
            Player player = (Player) sender;

            List<Division> divisions = ContextProvider.DIVISION_REPOSITORY.findAllPlayerDivisions(player.getUniqueId());
            Division d = divisions.stream().findAny().get();

            player.sendRawMessage(d.getName());
            player.sendRawMessage(d.getFullName());
            player.sendRawMessage(d.soldierCount()+" count");
            player.sendRawMessage(d.getDivisionList().size()+"size");
            player.sendRawMessage(d.getDivisionList().get(0).getData().getDPI()+"id");
            player.sendRawMessage(d.getDivisionList().get(0).getAsBukkitEntity().getName()+"name");


            return true;
        } else {
            return false;
        }
    }
}
