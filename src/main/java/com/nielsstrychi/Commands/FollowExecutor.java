package com.nielsstrychi.Commands;

import com.nielsstrychi.Data.Division;
import com.nielsstrychi.Handlers.PathHandler;
import com.nielsstrychi.Providers.ContextProvider;
import com.nielsstrychi.Selectors.DivisionSelector;
import com.nielsstrychi.Selectors.MultiSelector;
import com.nielsstrychi.Selectors.Selector;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.Optional;

/**
 * View player selection
 */
public class FollowExecutor implements Executor {

    public boolean onCommand(final CommandSender sender,
                             final Command command,
                             final String label,
                             final String[] split) {
        if (sender instanceof Player) {
            Player player = (Player) sender;
            if (split.length > 0 && ("stop".equals(split[0]) || "s".equals(split[0]))) {
                PathHandler.handle().cancelFollowers(player);
            } else if (split.length > 0 && ("view".equals(split[0]) || "v".equals(split[0]))) {
                player.sendRawMessage(String.format("%s troops are following you.",
                        PathHandler.handle().countFollowers(player)));
            }else {
                Optional<Selector> selector = ContextProvider.SELECTION_HANDLER.getSelector(player);
                if (!selector.isPresent()) {
                    player.sendRawMessage("No entity is selected.");
                } else if (!(selector.get() instanceof MultiSelector)) {
                    selector.get().follow(player);
                    player.sendRawMessage(String.format("One %s soldier follows you now.", selector.get().getSoldier().getAsBukkitEntity().getType().name()));
                } else if (selector.get() instanceof DivisionSelector) {
                    Division division = ((DivisionSelector) selector.get()).getDivision();
                    selector.get().follow(player);
                    player.sendRawMessage(String.format("Division %s is following you now.", division.getName()));
                } else {
                    selector.get().follow(player);
                    player.sendRawMessage(String.format("%s soldiers are following you now.", ((MultiSelector) selector.get()).count()));
                }
            }
            return true;
        } else {
            return false;
        }
    }
}
