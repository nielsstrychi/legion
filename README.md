# Nielsstrychi's Legion plugin

//TODO description here :)

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

* Maven
* Java Development Kit 8
* CraftBukkit or Spigot minecraft server

Download & Install Maven.

```
    https://maven.apache.org/install.html
```

Download & Install JDK 8.

```
    https://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html
```

Download a bukkit/spigot minecraft server from the same version of the project.

```
    https://getbukkit.org/
```

### Installing

Setup a bukkit/spigot minecraft server in /server/${minecraft.version} directory.

```
    Place craftbukkit.jar in /server/${minecraft.version} directory.
```

Add configuration in the IntelliJ IDEA (Right above).
```
    1. Click (+) to add configuration (Top left corner)
    2. Select JAR Application (In the middle of the selection)
    3. Specify the path to the craftbukkit.jar located in /server/${minecraft.version} directory. 
    4. Use the same path for the working directory. So the server files are extracted in the /server/${minecraft.version} folder.
    5. Specify your JRE, Java 8 is recommended.
    6. Click (+) by before launch and add a Run Maven Goal (Left below).
    7. Input in the text field command line "CLEAN" and "PACKAGE" then apply.
    8. Apply changes and exit run/debug configuration.
    9. Run the server once with the green play button (Top right) and accept the eula agreement.
    10. Its now up and running in debug mode you can apply live changes to the code.
```

Optional create a empty .bat file and paste following command inside, you can also start up the server this way.

```
    java -Xms1024M -Xmx1024M -jar craftbukkit.jar -o true
    PAUSE
```

## Deployment

To deploy for production run in a terminal following command.

```
    mvn clean install -Dprod
```

The JAR in the target folder is ready to be deployed in a production server.

## Built With

* [BuildTools](https://www.spigotmc.org/wiki/buildtools/) - The minecraft server library's.
* [Maven](https://maven.apache.org/) - Dependency Management.
* [Hibernate](https://hibernate.org/) - Object-relational mapping.

## Authors

* **Niels Strychi** - *Initial work* - [nielsstrychi](https://github.com/Nielsstrychi).

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details.

